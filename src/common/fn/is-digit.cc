bool is_digit(const char x)
{
 static thread_local const mem m="0123456789"; 

 return contain(m,x);
}

bool is_digit(const mem& x)
{
 const int l=get_length(x);

 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  
  if(!is_digit(c))
   return false;
 }
 
 return true;
}
