mem wrap(const void* x,const int y)
{
 check(x!=0);
 check(y>0);
 
 mem r;
 
 allocate(r,y);
 
 char* p=get_address(r);
 
 memcpy(p,x,y);
 
 return r;
}

mem wrap(const bool y)
mem wrap(const char y)
mem wrap(const int y)
mem wrap(const unsigned y)
mem wrap(const long y)
mem wrap(const unsigned long y)
mem wrap(const double y)
mem wrap(const void* y)
{
 return wrap(&y,sizeof(y));
}

mem wrap(const txt& x)
mem wrap(const map& x)
{
 mem m;
 
 serialize(m,x);
 
 return m;
}

