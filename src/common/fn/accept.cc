bool accept(fd& x,fd& y)
{
 clear(y);

 y.handle=accept(x.handle,0,0);
 
 if(y.handle==-1)
 {
  check(errno==EAGAIN||errno==EWOULDBLOCK);
  
  return false;
 }

 set_non_block(y);
 
 return true;
}
