void echo(const bool x)
void echo(const char x)
void echo(const int x)
void echo(const unsigned x)
void echo(const long x)
void echo(const unsigned long x)
void echo(const double x)
void echo(const void* x)
{
 const mem m=to_mem(x);
 
 echo(m);
}

void echo(const char* x)
{
 check(x!=0);
 
 const mem m=to_mem(x);
 
 echo(m);
}

void echo(const mem& x)
{
 if(is_empty(x))
  return;
 
 const int l=get_length(x);
 const char* p=get_address(x);
 const int n=write(STDOUT_FILENO,p,l);
 
 check(n==l);
}

void echo(const buf& x)
{
 const mem m=to_mem(x);
 
 echo(m);
}
