long disk_size()
{
 struct statfs s;
 const int n=statfs("/",&s);
 
 check(n==0);
 
 return s.f_bsize*s.f_blocks;
}
