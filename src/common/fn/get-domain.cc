mem get_domain(const mem& x)
{
 const mem command=concat("nslookup ",x);
 pfile f;
 
 open(f,command);
 
 const mem m1=read(f);
 
 try
 {  
  clear(f);
 }
 catch(...)
 {
  return x;
 }
 
 const txt t1=to_txt(m1);
 const mem m2=front(t1);
 const txt t2=split(m2,'\t');
 const mem m3=back(t2);
 const txt t3=split(m3,' ');
 const mem m4=back(t3);
 
 return strip_r(m4,'.');
}
