mem encode(const char x)
{
 mem r;
 
 if(x=='\\')
  append(r,"\\\\");
 else if(x=='\n')
  append(r,"\\n");
 else
  push(r,x);
 
 return r;
}

mem encode(const mem& x)
{
 const int l=get_length(x);

 buf b;
 
 reserve(b,l);
 
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  const mem m=encode(c);
  
  append(b,m);
 }
 
 return detach(b);
}
