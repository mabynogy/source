char back(const mem& x)
{
 check(is_full(x));
 
 return back(x,0);
}

char back(const mem& x,const int y)
{
 const int l=get_length(x);
 
 check(y>=0);
 check(y<l);
 
 const int n=l-y-1;
 
 return at(x,n);
}

void back(mem& x,const int y,const char z)
{
 const int l=get_length(x);

 check(y>=0);
 check(y<l);
 
 const int n=l-y-1;
 
 set(x,n,z);
}

char back(const buf& x)
{
 check(is_full(x));
 
 return back(x,0);
}

char back(const buf& x,const int y)
{
 const int l=get_length(x);

 check(y>=0);
 check(y<l);
 
 const int n=l-y-1;
 
 return at(x,n);
}

void back(buf& x,const int y,const char z)
{
 const int l=get_length(x);

 check(y>=0);
 check(y<l);
 
 const int n=l-y-1;
 
 set(x,n,z);
}

mem back(const txt& x)
{
 check(is_full(x));
 
 buf b;
 
 const int l=get_length(x.buffer);
 const int lx=l-1;
 
 for(int i=0;i<lx;i++)
 {
  const char n=i+1;
  const char c=back(x.buffer,n);
  
  if(c=='\n')
   break;
   
  unshift(b,c);
 }
 
 return detach(b);
}

mem back(const seq& x)
{
 check(is_full(x));
 
 const mem m=back(x.text);
 
 return decode(m);
}
