mem to_hexa(const int x)
{
 mem r;
 
 allocate(r,256);
 
 const int l=get_length(r); 
 char* p=get_address(r);
 const int n=sprintf(p,"%X",x);
 
 check(n>=0);
 check(n<l);
 
 allocate(r,n);
 
 return r;
}
