mem translit(const mem& x)
{
 mem r;
 const mem tmp=path_tmp("translit.txt");
 
 save(tmp,x);
 
 try
 { 
  r=os_execute(concat("iconv -f UTF-8 -t ASCII//TRANSLIT ",tmp));
 }
 catch(...)
 {
  fs_remove(tmp);
  
  throw;
 }

 fs_remove(tmp);
 
 return r;
}
