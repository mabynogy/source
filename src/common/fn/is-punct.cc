bool is_punct(const char x)
{
 static thread_local const mem m="!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"; 

 return contain(m,x);
}
