void open(fd& x,const mem& y)
{
 open(x,y,O_RDONLY);
}

void open(fd& x,const mem& y,const int z)
{
 clear(x);
 
 const mem m=to_c(y);
 const char* p=get_address(m);
 
 x.handle=open(p,z,0644);
 
 check(is_full(x));
}

void open(pfile& x,const mem& y)
{
 clear(x);
 
 const mem m=to_c(y);
 const char* p=get_address(m);
 
 x.pointer=popen(p,"r");
 
 check(is_full(x));
}
