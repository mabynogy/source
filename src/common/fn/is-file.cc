bool is_file(const mem& x)
{
 const mem m=to_c(x);
 struct stat s;
 
 const char* p=get_address(m);
 const int n=stat(p,&s);
 
 if(n!=0)
  return false;
  
 return S_ISREG(s.st_mode);
}
