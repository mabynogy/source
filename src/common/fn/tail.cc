mem tail(const mem& x,const int y)
mem tail(const buf& x,const int y)
{
 check(y>=0);
 
 const int l=get_length(x);
 
 if(l<=y)
  return x;
  
 return slice_r(x,y);
}
