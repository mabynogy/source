void dir_make(const mem& x)
{
 check(!is_fs(x));

 const mem qx=quote(x);
 const mem command=concat("mkdir -p ",qx);
 
 os_execute(command);
 
 check(is_dir(x));
}
