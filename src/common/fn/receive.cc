mem receive(const fd& x)
{
 mem r;
 
 while(true)
 {
  mem m;
  
  allocate(m,1024);
  
  const int l=get_length(m);
  char* p=get_address(m);
  const int n=read(x.handle,p,l);
  
  if(n==0)
   break;
   
  if(n==-1)   
  {
   check(errno==EAGAIN||errno==EWOULDBLOCK);
   
   break;
  }
 
  check(n>0);
  check(n<=l);
  
  allocate(m,n);  
  append(r,m);
 } 
 
 return r;
}
