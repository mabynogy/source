void title_l(const mem& x)
{
 title_l(x,"*");
}

void title_l(const mem& x,const mem& y)
{
 check(is_full(x));
 check(is_full(y));
 
 const int width=term_width(); 
 const mem m1=repeat(y,2);
 const mem m2=repeat(y,width);
 const mem m3=concat(m1," ",x," ",m2);
 const mem m4=head(m3,width);
 
 print(m4);
}
