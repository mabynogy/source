int os_system(const mem& x)
{
 const mem m=to_c(x);
 const char* p=get_address(m);
 
 return system(p);
}

int os_system(const mem& x,const mem& y)
{
 int r=0;
 const mem previous=dir_current();

 dir_change(x);
 
 try
 {
  r=os_system(y);
 }
 catch(...)
 {
  dir_change(previous);

  throw;
 }
 
 dir_change(previous);
 
 return r;
}
