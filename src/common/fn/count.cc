int count(const mem& x,const char y)
int count(const buf& x,const char y)
{
 int r=0;
 
 const int l=get_length(x);
 
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  
  if(c==y)
   r++;
 }
 
 return r;
}

int count(const txt& x)
{
 return count(x.buffer,'\n');
}

int count(const seq& x)
{
 return count(x.text);
}

int count(const map& x)
{
 return count(x.keys);
}
