struct stat fs_stat(const mem& x)
{
 check(is_fs(x));
 
 const mem m=to_c(x);
 struct stat r;
 
 const char* p=get_address(m);
 const int n=stat(p,&r);
 
 check(n==0);
 
 return r;
}
