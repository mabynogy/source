mem strip_r(const mem& x,const char y)
mem strip_r(const buf& x,const char y)
{
 if(match_r(x,y))
 { 
  const int l=get_length(x);
  const int lx=l-1;

  return slice_l(x,lx);
 }
  
 return x;
}

mem strip_r(const mem& x,const mem& y)
mem strip_r(const buf& x,const mem& y)
{
 if(match_r(x,y))
 { 
  const int lx=get_length(x);
  const int ly=get_length(x);
  const int l=lx-ly;

  return slice_l(x,l);
 }
  
 return x;
}
