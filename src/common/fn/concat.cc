mem concat(const mem& x,const mem& y)
{
 mem r;
 
 append(r,x);
 append(r,y);
 
 return r;
}

mem concat(const mem& x,const mem& y,const mem& z)
{
 mem r;
 
 append(r,x);
 append(r,y);
 append(r,z);
 
 return r;
}

mem concat(const mem& x,const mem& y,const mem& z,const mem& a)
{
 mem r;
 
 append(r,x);
 append(r,y);
 append(r,z);
 append(r,a);
 
 return r;
}

mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b)
{
 mem r;
 
 append(r,x);
 append(r,y);
 append(r,z);
 append(r,a);
 append(r,b);
 
 return r;
}

mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b,const mem& c)
{
 mem r;
 
 append(r,x);
 append(r,y);
 append(r,z);
 append(r,a);
 append(r,b);
 append(r,c);
 
 return r;
}

mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b,const mem& c,const mem& d)
{
 mem r;
 
 append(r,x);
 append(r,y);
 append(r,z);
 append(r,a);
 append(r,b);
 append(r,c);
 append(r,d);
 
 return r;
}

mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b,const mem& c,const mem& d,const mem& e)
{
 mem r;
 
 append(r,x);
 append(r,y);
 append(r,z);
 append(r,a);
 append(r,b);
 append(r,c);
 append(r,d);
 append(r,e);
 
 return r;
}

mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b,const mem& c,const mem& d,const mem& e,const mem& f)
{
 mem r;
 
 append(r,x);
 append(r,y);
 append(r,z);
 append(r,a);
 append(r,b);
 append(r,c);
 append(r,d);
 append(r,e);
 append(r,f);
 
 return r;
}

mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b,const mem& c,const mem& d,const mem& e,const mem& f,const mem& g)
{
 mem r;
 
 append(r,x);
 append(r,y);
 append(r,z);
 append(r,a);
 append(r,b);
 append(r,c);
 append(r,d);
 append(r,e);
 append(r,f);
 append(r,g);
 
 return r;
}

mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b,const mem& c,const mem& d,const mem& e,const mem& f,const mem& g,const mem& h)
{
 mem r;
 
 append(r,x);
 append(r,y);
 append(r,z);
 append(r,a);
 append(r,b);
 append(r,c);
 append(r,d);
 append(r,e);
 append(r,f);
 append(r,g);
 append(r,h);
 
 return r;
}
