mem trim_r(const mem& x)
{
 buf b=to_buf(x);
 
 while(is_full(b))
 {
  const char c=back(b);
  
  if(!is_blank(c))
   break;
   
  drop_r(b);
 }
 
 return detach(b);
}

txt trim_r(const txt& x)
{
 txt r;
 txt t=x;
 
 while(is_full(t))
 {
  const mem m1=shift(t);
  const mem m2=trim_r(m1);
  
  push(r,m2);
 }
 
 while(is_full(r))
 {
  const mem m=back(r);
  
  if(is_empty(m))
   drop_r(r);
  else
   break;
 }
 
 return r;
}
