void dir_change(const mem& x)
{
 const mem m=to_c(x);
 const char* p=get_address(m);
 const int n=chdir(p);
 
 check(n==0); 
}
