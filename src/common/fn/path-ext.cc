mem path_ext(const mem& x)
{
 const mem base=path_base(x); 
 
 txt t=split(base,'.'); 
 
 const int l=count(t);
 
 if(l<2)
  return "";
  
 return pop(t);
}
