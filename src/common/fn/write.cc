void write(const fd& x,const mem& y)
{
 check(is_full(x));

 if(is_empty(y))
  return;
  
 const char* p=get_address(y);
 const int l=get_length(y);
 const int n=write(x.handle,p,l);
 
 check(n==l);
}
