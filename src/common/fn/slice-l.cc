mem slice_l(const mem& x,const int y)
mem slice_l(const buf& x,const int y)
{
 const int l=get_length(x);
 
 check(y>=0);
 check(y<=l);
 
 return slice(x,0,y);
}

txt slice_l(const txt& x,const int y)
{
 const int l=count(x);

 check(y>=0);
 check(y<=l);

 return slice(x,0,y);
}

seq slice_l(const seq& x,const int y)
{
 const txt t=slice_l(x.text,y);

 return to_seq(t); 
}
