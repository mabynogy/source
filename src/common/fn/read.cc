mem read(const fd& x)
{
 buf b;
 
 while(true)
 {
  const mem m=read(x,1024);
  
  if(is_empty(m))
   break;
   
  append(b,m);
 }
 
 return detach(b);
}

mem read(const pfile& x)
{
 buf b;
 
 while(true)
 {
  const mem m=read(x,1024);
  
  if(is_empty(m))
   break;
   
  append(b,m);
 }
 
 return detach(b);
}

mem read(const fd& x,const int y)
{
 check(y>=0);
 
 mem r;

 if(y==0)
  return r;
  
 allocate(r,y);
 
 char* p=get_address(r);
 const int n=read(x.handle,p,y); 
 
 check(n>=0);
 check(n<=y);
 
 allocate(r,n);
 
 return r;
}

mem read(const pfile& x,const int y)
{
 check(y>=0);
 
 mem r;

 if(y==0)
  return r;
  
 allocate(r,y);
 
 char* p=get_address(r);
 const int n=fread(p,1,y,x.pointer);
 
 check(n>=0);
 check(n<=y);
 
 allocate(r,n);
 
 return r;
}
