long disk_usage()
{
 struct statfs s;
 const int n=statfs("/",&s);

 check(n==0);

 const long l=disk_size();
 
 return l-s.f_bsize*s.f_bfree;
}
