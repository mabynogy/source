mem dir_current()
{
 mem m;
 
 allocate(m,PATH_MAX);
 
 const int l=get_length(m);
 char* p=get_address(m);
 char* r=getcwd(p,l);
 
 check(r!=0);
 
 return r;
}
