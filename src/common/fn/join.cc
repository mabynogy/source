mem join(const txt& x)
{
 return to_mem(x);
}

mem join(const seq& x)
{
 return join(x,'\n');
}

mem join(const txt& x,const char y)
{
 mem r=to_mem(x);
 
 const int l=get_length(r);
 
 for(int i=0;i<l;i++)
 {
  const char c=at(r,i);
  
  if(c=='\n')
   set(r,i,y);
 }
 
 return r;
}

mem join(const seq& x,const char y)
{ 
 mem r;
 seq t=x;
 
 while(is_full(t))
 {
  const mem m=shift(t);
  
  append(r,m);
  push(r,y);
 }
 
 return r;
}

mem join(const txt& x,const mem& y)
{ 
 mem r;
 txt t=x;
 
 while(is_full(t))
 {
  const mem m=shift(t);
  
  append(r,m);
  append(r,y);
 }
 
 return r;
}

mem join(const seq& x,const mem& y)
{ 
 mem r;
 seq t=x;
 
 while(is_full(t))
 {
  const mem m=shift(t);
  
  append(r,m);
  append(r,y);
 }
 
 return r;
}
