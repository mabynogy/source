void create(pthread& x,void (*y)(const map&),const map& z)
{
 check(y!=0);
 
 clear(x);
 
 map m;
 
 set(m,"routine",wrap((void*)y));
 set(m,"arguments",wrap(z));
 
 void* p=lift(m);
 const int n=pthread_create(&x.pointer,0,on_create,p);
 
 check(n==0);
}

void* on_create(void* x)
{
 check(x!=0);
 
 map m1;
 
 unlift(m1,x);
 
 const mem m2=get(m1,"routine"); 
 const mem m3=get(m1,"arguments"); 
 void* p=0;
 
 unwrap(p,m2);

 void (*routine)(const map&)=(void (*)(const map&))p;
 
 map arguments;
 
 unwrap(arguments,m3); 
 routine(arguments); 
 
 return 0;
}
