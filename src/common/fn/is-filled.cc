bool is_filled(const buf& x)
{
 const int l=get_capacity(x);
 
 return x.length==l;
}
