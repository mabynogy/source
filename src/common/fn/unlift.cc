void unlift(mem& x,void* y)
{
 check(y!=0);

 buf b;
 
 while(true)
 {
  const int l=get_length(b);
  
  char* p=(char*)y;
  const char c=p[l];
  
  if(c=='\n')
   break;
  
  push(b,c);  
 }
 
 const mem m=detach(b);
 
 free(y); 
 x=decode(m);
}

void unlift(txt& x,void* y)
void unlift(map& x,void* y)
{
 check(y!=0);
 
 mem m;
 
 unlift(m,y);
 deserialize(x,m);
}
