buf to_buf(const mem& x)
{
 const int l=get_length(x);
 
 buf r;
 
 dim(r,l);
 copy(r,0,x,0,l);
 
 return r;
}
