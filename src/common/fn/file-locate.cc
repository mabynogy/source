mem file_locate(const mem& x)
{
 check(is_full(x));
 
 const mem current=dir_current();
 
 txt dirs=split(current,'/');
 
 while(is_full(dirs))
 {
  const mem dir=join(dirs,'/');
  const mem path=path_concat(dir,x);
  
  if(is_file(path))
   return path_real(path);

  drop_r(dirs);
 }
 
 stop();
 
 return {};
}
