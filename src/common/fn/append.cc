void append(mem& x,const mem& y)
{
 const int lx=get_length(x);
 const int ly=get_length(y);
 const int l=lx+ly;
 
 allocate(x,l);
 copy(x,lx,y,0,ly);
}

void append(buf& x,const char* y)
{
 check(y!=0);

 const mem m=y;
 
 append(x,m);
}

void append(buf& x,const mem& y)
void append(buf& x,const buf& y)
{
 const int lx=get_length(x);
 const int ly=get_length(y);
 const int l=lx+ly;
 
 reserve(x,l);
 
 for(int i=0;i<ly;i++)
 {
  const char c=at(y,i);
  
  push(x,c);
 }
}

void append(txt& x,const txt& y)
{
 append(x.buffer,y.buffer);
}

void append(seq& x,const seq& y)
{
 append(x.text,y.text);
}
