void listen(fd& x,const int y)
{
 clear(x);
 
 x.handle=socket(AF_INET,SOCK_STREAM,0);
 
 check(x.handle>=0);

 const int on=1;
 const int n1=setsockopt(x.handle,SOL_SOCKET,SO_REUSEADDR,&on,sizeof(on));
 
 check(n1==0);
 
 struct sockaddr_in a;

 a.sin_family=AF_INET; 
 a.sin_addr.s_addr=htonl(INADDR_ANY); 
 a.sin_port=htons(y);
 
 const int n2=bind(x.handle,(sockaddr*)&a,sizeof(a));

 check(n2==0);
 
 const int n3=listen(x.handle,8);
 
 check(n3==0);
 
 set_non_block(x);
}
