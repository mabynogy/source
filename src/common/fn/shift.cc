char shift(mem& x)
{
 check(is_full(x));
 
 const char r=front(x);
 
 drop_l(x);
 
 return r;
}

mem shift(mem& x,const int y)
{
 const int l=get_length(x);

 check(y>=0);
 check(y<=l);
 
 const mem r=slice_l(x,y);
 
 drop_l(x,y);
 
 return r;
}

char shift(buf& x)
{
 check(is_full(x));
 
 const char r=front(x);
 
 drop_l(x);
 
 return r;
}

mem shift(buf& x,const int y)
{
 const int l=get_length(x);

 check(y>=0);
 check(y<=l);
 
 const mem r=slice_l(x,y);
 
 drop_l(x,y);
 
 return r;
}

mem shift(txt& x)
mem shift(seq& x)
{
 check(is_full(x));

 const mem r=front(x);
 
 drop_l(x);
 
 return r; 
}
