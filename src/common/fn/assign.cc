void assign(mem& x,const char* y)
{
 check(y!=0);
 
 const int l=strlen(y);
 
 if(l==0)
 {
  clear(x);
  
  return;
 }
 
 allocate(x,l);
 
 char* p=get_address(x);
 
 memcpy(p,y,l);
}

void assign(mem& x,const mem& y)
void assign(mem& x,const buf& y)
{
 const int l=get_length(y);

 allocate(x,l);
 copy(x,0,y,0,l);
}

void assign(buf& x,const char* y)
{ 
 check(y!=0);

 const int l=strlen(y);

 reset(x); 
 reserve(x,l); 
 
 for(int i=0;i<l;i++)
 {
  const char c=y[i];
  
  push(x,c);
 }
}

void assign(buf& x,const buf& y)
{
 reset(x);
 append(x,y);
}

void assign(txt& x,const txt& y)
{
 assign(x.buffer,y.buffer);
}

void assign(seq& x,const seq& y)
{
 assign(x.text,y.text);
}

void assign(map& x,const map& y)
{
 assign(x.keys,y.keys);
 assign(x.values,y.values);
}
