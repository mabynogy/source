void* lift(const mem& x)
{
 mem m=encode(x);
 
 push(m,'\n');
 
 const ptr p=detach(m);
 
 return p.pointer;
}

void* lift(const txt& x)
void* lift(const map& x)
{
 mem m;
 
 serialize(m,x);
 
 return lift(m);
}
