int get_random(const int x)
{
 check(x>=0);
 
 int v=0;
 int n=0;
 
 while(n<x)
 {
  const int i=rand();
  
  v+=i;
  n+=RAND_MAX;  
 }
 
 return v%x;
}
