void fs_remove(const mem& x)
{
 check(is_fs(x));

 const mem m=to_c(x); 
 const char* p=get_address(m);
 const int n=remove(p);
 
 check(n==0);
}
