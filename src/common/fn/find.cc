int find(const mem& x,const char y)
{
 const int l=get_length(x);
 
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  
  if(c==y)
   return i;
 }
 
 return -1;
}
