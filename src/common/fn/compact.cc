mem compact(const mem& x)
{
 txt t1=to_txt(x);
 txt t2;
 
 while(is_full(t1))
 {
  const mem m1=shift(t1);
  const mem m2=trim_r(m1);
  
  if(is_empty(m2))
   continue;
  
  push(t2,m2);
 }
 
 const mem m3=glue(t2);
 
 return trim_r(m3);
}
