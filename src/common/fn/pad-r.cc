mem pad_r(const int x,const int y)
{
 check(y>=0);
 
 const mem m=to_mem(x);
 
 return pad_r(m,y,'0'); 
}

mem pad_r(const mem& x,const int y,const char z)
{
 check(y>=0);
 
 mem r=x;
 
 while(true)
 {
  const int l=get_length(r);
  
  if(l>=y)
   break;
   
  push(r,z);
 }
 
 return r;
}
