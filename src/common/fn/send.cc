void send(const fd& x,const mem& y)
{
 const timer t=timeout(0.1);
 mem m=y;
 
 while(is_full(m))
 {  
  check(!hit(t));
  
  const int l=get_length(m);  
  const char* p=get_address(m);  
  const int n=write(x.handle,p,l);
     
  if(n==-1)   
  {
   check(errno==EAGAIN||errno==EWOULDBLOCK);
   
   wait(0.01);
  }
 
  check(n>=0);
  check(n<=l);
  
  drop_l(m,n);  
 } 
}
