mem get_date()
{
 const int n=time(0);
 
 check(n!=-1);
 
 return get_date(n);
}

mem get_date(const int x)
{
 const time_t t=x;
 const tm* p=localtime(&t);
 
 check(p!=0);
 
 const tm s=*p;
 const int m=s.tm_mon+1;
 const int y=s.tm_year+1900; 
 const mem minute=pad_l(s.tm_min,2);
 const mem hour=pad_l(s.tm_hour,2);
 const mem day=pad_l(s.tm_mday,2);
 const mem month=pad_l(m,2);
 const mem year=to_mem(y);
 const mem date=concat(year,"/",month,"/",day);
 const mem time=concat(hour,"h",minute);
 
 return concat(date," ",time);
}
