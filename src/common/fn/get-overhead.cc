int get_overhead(const buf& x)
{
 const int l=get_capacity(x);
 
 return l-x.length; 
}
