int get_length(const ptr& x)
{
 return x.length;
}

int get_length(const mem& x)
{
 return get_length(x.pointer);
}

int get_length(const buf& x)
{
 return x.length;
}
