bool match_r(const mem& x,const char y)
bool match_r(const buf& x,const char y)
{
 if(is_empty(x))
  return false;

 const char c=back(x);
 
 return c==y;
}

bool match_r(const mem& x,const mem& y)
bool match_r(const buf& x,const mem& y)
{
 if(is_empty(x))
  return false;

 if(is_empty(y))
  return false;
 
 const int lx=get_length(x);
 const int ly=get_length(y);
  
 if(ly>lx)
  return false;
  
 for(int i=0;i<ly;i++)
 {
  const char cx=back(x,i);
  const char cy=back(y,i);
  
  if(cx!=cy)
   return false;
 }
 
 return true;
}
