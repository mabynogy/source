mem head(const mem& x,const int y)
mem head(const buf& x,const int y)
{
 check(y>=0);
 
 const int l=get_length(x);
 
 if(l<=y)
  return x;
  
 return slice_l(x,y);
}
