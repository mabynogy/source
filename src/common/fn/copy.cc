void copy(ptr& x,const int y,const int z,const int a)
{
 const int l=get_length(x);
 
 check(y>=0);
 check(y<=l);
 check(z>=0);
 check(z<=l);
 check(a>=0);
 check(a<=l);
 check(y+a<=l);
 check(z+a<=l);
 
 if(a==0)
  return;
 
 const int yz=y+z;
 const bool overlap=a>y&&a<yz;

 char* py=get_address(x,y);
 char* pz=get_address(x,z);
  
 if(overlap)
  memmove(py,pz,a);
 else
  memcpy(py,pz,a);
}

void copy(mem& x,const int y,const int z,const int a)
{
 copy(x.pointer,y,z,a);
}

void copy(ptr& x,const int y,const ptr& z,const int a,const int b)
{
 const int lx=get_length(x);
 const int lz=get_length(z);

 check(y>=0);
 check(y<=lx);
 check(a>=0);
 check(a<=lz);
 check(b>=0);
 check(b<=lz);
 check(y+b<=lx);
 check(a+b<=lz);
 
 if(b==0)
  return;

 char* py=get_address(x,y);
 const char* pa=get_address(z,a);
  
 memcpy(py,pa,b);
}

void copy(mem& x,const int y,const mem& z,const int a,const int b)
{
 copy(x.pointer,y,z.pointer,a,b);
}

void copy(buf& x,const int y,const mem& z,const int a,const int b)
{
 const int lx=get_length(x);
 const int lz=get_length(z);

 check(y>=0);
 check(y<=lx);
 check(a>=0);
 check(a<=lz);
 check(b>=0);
 check(b<=lz);
 check(y+b<=lx);
 check(a+b<=lz);
 
 if(b==0)
  return;
  
 if(is_circular(x))
 {
  for(int i=0;i<b;i++)
  {
   const int ny=y+i;
   const int na=a+i;
   const char c=at(z,na);
   
   set(x,ny,c);
  }
  
  return;
 }
 
 const int ny=x.index+y;
 
 copy(x.memory,ny,z,a,b);
}

void copy(mem& x,const int y,const buf& z,const int a,const int b)
{
 const int lx=get_length(x);
 const int lz=get_length(z);

 check(y>=0);
 check(y<=lx);
 check(a>=0);
 check(a<=lz);
 check(b>=0);
 check(b<=lz);
 check(y+b<=lx);
 check(a+b<=lz);
 
 if(b==0)
  return;
  
 if(is_circular(z))
 {
  for(int i=0;i<b;i++)
  {
   const int ny=y+i;
   const int na=a+i;
   const char c=at(z,na);
   
   set(x,ny,c);
  }
  
  return;
 }
 
 const int na=z.index+a;

 copy(x,y,z.memory,na,b); 
}

void copy(buf& x,const int y,const buf& z,const int a,const int b)
{
 const int lx=get_length(x);
 const int lz=get_length(z);

 check(y>=0);
 check(y<=lx);
 check(a>=0);
 check(a<=lz);
 check(b>=0);
 check(b<=lz);
 check(y+b<=lx);
 check(a+b<=lz);
 
 if(b==0)
  return;
  
 if(is_circular(x)||is_circular(z))
 {
  for(int i=0;i<b;i++)
  {
   const int ny=y+i;
   const int na=a+i;
   const char c=at(z,na);
   
   set(x,ny,c);
  }
  
  return;
 }

 const int ny=x.index+y;
 const int na=z.index+a;
 
 copy(x.memory,ny,z.memory,na,b);
}
