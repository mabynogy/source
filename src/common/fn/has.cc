bool has(map& x,const mem& y)
{
 check(is_full(y));

 return contain(x.keys,y); 
}
