char pop(mem& x)
{
 check(is_full(x));
 
 const char r=back(x);
 
 drop_r(x);
 
 return r;
}

mem pop(mem& x,const int y)
{
 const int l=get_length(x);

 check(y>=0);
 check(y<=l);
 
 const mem r=slice_r(x,y);
 
 drop_r(x,y);
 
 return r;
}

char pop(buf& x)
{
 check(is_full(x));
 
 const char r=back(x);
 
 drop_r(x);
 
 return r;
}

mem pop(buf& x,const int y)
{
 const int l=get_length(x);

 check(y>=0);
 check(y<=l);
 
 const mem r=slice_r(x,y);
 
 drop_r(x,y);
 
 return r;
}

mem pop(txt& x)
mem pop(seq& x)
{
 check(is_full(x));
 
 const mem r=back(x);

 drop_r(x);
 
 return r;
}
