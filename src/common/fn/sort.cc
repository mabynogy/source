void sort(txt& x)
{
 if(is_empty(x))
  return;
 
 const mem path=path_tmp("sort.txt");
 const mem content=to_mem(x);
 
 save(path,content);
 
 const mem command=concat("sort ",path); 
 const mem result=os_execute(command);
 
 fs_remove(path);
 
 x=to_txt(result);
}
