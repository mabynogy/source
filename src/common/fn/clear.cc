void clear(ptr& x)
{
 x.pointer=0;
 x.length=0;
}

void clear(mem& x)
{
 if(is_empty(x))
  return;
  
 free(x.pointer.pointer);
 clear(x.pointer);
}

void clear(buf& x)
{
 x.index=0;
 x.length=0;
 
 clear(x.memory);
}

void clear(txt& x)
{
 clear(x.buffer);
}

void clear(seq& x)
{
 clear(x.text);
}

void clear(fd& x)
{
 if(is_empty(x))
  return;

 const int n=close(x.handle);
 
 x.handle=-1;
 
 check(n==0);
}

void clear(pfile& x)
{
 if(is_empty(x))
  return;
  
 const int n=pclose(x.pointer);
 
 x.pointer=0;

 check(n==0);
}

void clear(pthread& x)
{
 if(is_empty(x))
  return;
 
 void* p=0;
 const int n=pthread_join(x.pointer,&p);
 
 x.pointer=0;

 check(n==0);
}

void clear(map& x)
{
 clear(x.keys);
 clear(x.values);
}

void clear(timer& x)
{
 x.start=get_now();
 x.duration=1;
}

void clear(grid& x)
{
 x.width=0;
 x.height=0;
 clear(x.cells);
}

void clear(tbl& x)
{
 clear(x.columns);
 clear(x.cells);
}
