void unwrap(void* x,const int y,const mem& z)
{
 const int l=get_length(z);
 
 check(x!=0);
 check(y>0);
 check(y==l);

 const char* p=get_address(z);
 
 memcpy(x,p,y);
} 

void unwrap(bool& x,const mem& y)
void unwrap(char& x,const mem& y)
void unwrap(int& x,const mem& y)
void unwrap(unsigned& x,const mem& y)
void unwrap(long& x,const mem& y)
void unwrap(unsigned long& x,const mem& y)
void unwrap(double& x,const mem& y)
void unwrap(void*& x,const mem& y)
{
 unwrap(&x,sizeof(x),y);
}

void unwrap(txt& x,const mem& y)
void unwrap(map& x,const mem& y)
{
 deserialize(x,y);
}
