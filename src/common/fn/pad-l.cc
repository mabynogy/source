mem pad_l(const int x,const int y)
{
 check(y>=0);
 
 const mem m=to_mem(x);
 
 return pad_l(m,y,'0');
}

mem pad_l(const mem& x,const int y,const char z)
{
 check(y>=0);
 
 mem r=x;
 
 while(true)
 {
  const int l=get_length(r);
  
  if(l>=y)
   break;
   
  unshift(r,z);
 }
 
 return r;
}
