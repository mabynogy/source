void deserialize(txt& x,const mem& y)
{
 txt t=to_txt(y);
 
 move(x,t);
}

void deserialize(map& x,const mem& y)
{
 clear(x);
 
 txt t;
 
 deserialize(t,y);
 
 while(is_full(t))
 {
  const mem m1=shift(t);
  const mem m2=shift(t);
  const mem m3=decode(m1);
  const mem key=trim(m3);
  const mem value=decode(m2);
  
  put(x,key,value);
 }
}
