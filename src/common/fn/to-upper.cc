char to_upper(const char x)
{
 return toupper(x);
}

mem to_upper(const mem& x)
{
 mem r;
 
 const int l=get_length(x);
 
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  const char n=to_upper(c);
  
  push(r,n);
 }
 
 return r;
}
