char* get_address(ptr& x)
char* get_address(mem& x)
const char* get_address(const mem& x)
{
 return get_address(x,0);
}

char* get_address(ptr& x,const int y)
const char* get_address(const ptr& x,const int y)
{
 const int l=get_length(x);
 
 check(y>=0);
 check(y<l);
 
 return x.pointer+y;
}

char* get_address(mem& x,const int y)
const char* get_address(const mem& x,const int y)
{
 return get_address(x.pointer,y);
}
