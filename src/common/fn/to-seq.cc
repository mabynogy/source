seq to_seq(const txt& x)
{
 seq r;
 txt t=x;
 
 while(is_full(t))
 {
  const mem m=shift(t);
  
  push(r,m);
 }

 return r; 
}
