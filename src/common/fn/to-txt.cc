txt to_txt(const mem& x)
{
 txt r;
 buf b1=to_buf(x);
 buf b2;
 
 while(is_full(b1))
 {
  if(match_l(b1,"\r\n"))
  {
   shift(b1,2);
   push(b2,'\n');
  }
  else if(match_l(b1,'\r'))
  {
   shift(b1);
   push(b2,'\n');
  }
  else
  {
   const char c=shift(b1);
   
   push(b2,c);
  }
 }

 if(!match_r(b2,'\n'))
  push(b2,'\n');
 
 swap(r.buffer,b2);
 
 return r;
}
