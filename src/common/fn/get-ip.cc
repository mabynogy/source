mem get_ip(const fd& x)
{
 sockaddr_in a;
 socklen_t l=sizeof(a);
 const int n=getpeername(x.handle,(sockaddr*)&a,&l);
 
 check(n==0);
 
 const char* p=inet_ntoa(a.sin_addr);

 check(p!=0);
 
 return p;
}
