void flower()
{
 flower("*");
}

void flower(const mem& x)
{
 check(is_full(x));
 
 const int width=term_width(); 
 const mem m1=repeat(x,width);
 const mem m2=head(m1,width);
 
 print(m2);
}
