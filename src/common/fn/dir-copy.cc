void dir_copy(const mem& x,const mem& y)
{
 check(is_dir(x));
 check(is_dir(y));
 
 const mem qx=quote(x);
 const mem qy=quote(y);
 const mem m=concat("cp -rf ",qx," ",qy);
 
 os_execute(m);
}
