bool is_alpha(const char x)
{
 static thread_local const mem low="abcdefghijklmnopqrstuvwxyz";
 static thread_local const mem up="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
 
 if(contain(low,x))
  return true;

 if(contain(up,x))
  return true;
  
 return false;
}

bool is_alpha(const mem& x)
{
 if(is_empty(x))
  return false;
  
 const int l=get_length(x);
 
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  
  if(!is_alpha(c))
   return false;
 }
 
 return true;
}
