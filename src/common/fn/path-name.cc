mem path_name(const mem& x)
{
 const mem base=path_base(x); 
 
 txt t=split(base,'.'); 
 
 const int l=count(t);
 
 if(l>1)
  drop_r(t);
  
 return glue(t,'.');
}
