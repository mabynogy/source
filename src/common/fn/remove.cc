void remove(txt& x,const int y)
{
 remove(x,y,1);
}

void remove(txt& x,const int y,const int z)
{
 const int l=count(x);
 
 check(y>=0);
 check(z>=0);
 check(y<l);
 check(y+z<=l);
 
 txt t;
 const int n=y+z;
 
 move(t,x);
  
 for(int i=0;i<l;i++)
 {
  const mem m=shift(t);
  
  if(i<y||i>n)
   push(x,m);
 }
}

void remove(seq& x,const int y)
{
 remove(x.text,y);
}

void remove(seq& x,const int y,const int z)
{
 remove(x.text,y,z);
}
