void put(map& x,const mem& y,const mem& z)
{
 check(is_full(y));
 
 map m;
 
 swap(m,x);
 
 bool found=false;
 
 while(is_full(m))
 {
  const mem k=shift(m.keys);
  const mem v=shift(m.values);

  push(x.keys,k);
  
  if(eq(k,y))
  {
   push(x.values,z);
   
   found=true;
  }
  else
   push(x.values,v);  
 }
 
 if(!found)
 {
  push(x.keys,y);
  push(x.values,z);
 }
}
