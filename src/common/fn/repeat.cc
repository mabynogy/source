mem repeat(const mem& x,const int y)
{
 check(y>=0);
 
 buf b;
 
 const int l=get_length(x);
 const int ly=l*y;
 
 reserve(b,ly);
 
 for(int i=0;i<y;i++)
 {
  append(b,x);
 }
  
 return detach(b);
}
