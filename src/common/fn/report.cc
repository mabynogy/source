void report()
{
 const int n=errno;
 const mem m=strerror(n);
 
 print(concat("error ",to_lit(m)));
 print(concat("number ",to_mem(n)));
}
