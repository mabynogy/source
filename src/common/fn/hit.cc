bool hit(const timer& x)
{
 const double end=x.start+x.duration;
 const double now=get_now();
 
 return now>end;
}
