void drop_l(mem& x)
{
 check(is_full(x));
 
 drop_l(x,1);
}

void drop_l(mem& x,const int y)
{
 const int l=get_length(x);
 
 check(y>=0);
 check(y<=l);
 
 const int ly=l-y;
 
 copy(x,0,y,ly);
 allocate(x,ly);
}

void drop_l(buf& x)
{
 check(is_full(x));
 
 drop_l(x,1);
}

void drop_l(buf& x,const int y)
{
 check(y>=0);
 check(y<=x.length);

 const int l=get_capacity(x);
 
 x.index+=y;
 x.index%=l;
 x.length-=y; 
}

void drop_l(txt& x)
{
 check(is_full(x));
 
 while(is_full(x.buffer))
 {
  const char c=shift(x.buffer);
  
  if(c=='\n')
   break;
 }
}

void drop_l(seq& x)
{
 check(is_full(x));
 
 drop_l(x.text);
}

void drop_l(txt& x,const int y)
void drop_l(seq& x,const int y)
{
 check(y>=0);
 
 for(int i=0;i<y;i++)
 {
  drop_l(x);
 }
}
