mem path_tmp(const mem& x)
{
 const mem dir1=dir_locate("tmp");
 const mem dir2=path_dir(x);
 const mem dir3=path_concat(dir1,dir2);
 const mem base=path_base(x); 
 const mem path=path_concat(dir3,base); 
 
 return path_unique(path);
}
