bool is_full(const ptr& x)
bool is_full(const mem& x)
bool is_full(const buf& x)
bool is_full(const fd& x)
bool is_full(const pfile& x)
bool is_full(const pthread& x)
bool is_full(const seq& x)
bool is_full(const txt& x)
bool is_full(const map& x)
bool is_full(const grid& x)
bool is_full(const tbl& x)
{
 return !is_empty(x);
}
