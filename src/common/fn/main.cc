#include "ctype.h"
#include "errno.h"
#include "fcntl.h"
#include "libgen.h"
#include "limits.h"
#include "pthread.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "termios.h"
#include "time.h"
#include "unistd.h"

#include "arpa/inet.h"

#include "sys/socket.h"
#include "sys/stat.h"
#include "sys/vfs.h"

int main(const int x,const char** y)
{
 const int n=time(0);
 
 srand(n);
 
 txt t;
 
 for(int i=0;i<x;i++)
 {
  const char* p=y[i];
  
  push(t,p);
 }
 
 entry(t);
 
 return 0;
}
