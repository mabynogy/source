void dump(const bool x)
void dump(const char x)
void dump(const int x)
void dump(const unsigned x)
void dump(const long x)
void dump(const unsigned long x)
void dump(const double x)
void dump(const void* x)
{
 print(x);
}

void dump(const char* x)
{
 check(x!=0);
 
 const mem m=to_mem(x);
 
 dump(m);
}

void dump(const mem& x)
void dump(const buf& x)
{
 const mem m=to_lit(x);
 
 print(m);
}

void dump(const txt& x)
{
 txt t=x;
 int n=0;
 
 while(is_full(t))
 {
  const mem m=shift(t);
  
  echo("#");
  echo(n);
  echo(" ");
  dump(m);
  
  n++;
 }
}

void dump(const seq& x)
{
 seq s=x;
 int n=0;
 
 while(is_full(s))
 {
  const mem m=shift(s);
  
  echo("#");
  echo(n);
  echo(" ");
  dump(m);
  
  n++;
 }
}

void dump(const map& x)
{
 map m=x;
 
 while(is_full(m))
 {
  const mem k=shift(m.keys);
  const mem v=shift(m.values);
  
  echo(to_lit(k));
  echo(" ");
  echo(to_lit(v));
  print();
 }
}
