int file_mtime(const mem& x)
{
 check(is_file(x));
 
 const struct stat s=fs_stat(x);
 
 return s.st_mtime;
}
