char front(const mem& x)
{
 check(is_full(x));
 
 return at(x,0);
}

void front(mem& x,const char y)
{
 check(is_full(x));
 
 set(x,0,y);
}

char front(const buf& x)
{
 check(is_full(x));
 
 return at(x,0);
}

void front(buf& x,const char y)
{
 check(is_full(x));
 
 set(x,0,y);
}

mem front(const txt& x)
{
 check(is_full(x));
 
 buf b;
 
 const int l=get_length(x.buffer);
 
 for(int i=0;i<l;i++)
 {
  const char c=at(x.buffer,i);
  
  if(c=='\n')
   break;
   
  push(b,c);
 }
 
 return detach(b);
}

mem front(const seq& x)
{
 check(is_full(x));
 
 const mem m=front(x.text);
 
 return decode(m);
}
