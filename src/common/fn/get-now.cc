double get_now()
{
 static thread_local const double origin=get_time();
 
 const double n=get_time();
 
 return n-origin;
}
