mem os_execute(const mem& x)
{
 print(concat("> ",x));
 
 pfile f;
 
 open(f,x);
 
 const mem r=read(f);
 
 clear(f);
 
 return r;
}
