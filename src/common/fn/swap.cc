void swap(bool& x,bool& y)
{
 const bool b=x;
 
 x=y;
 y=b;
}

void swap(char& x,char& y)
{
 const bool b=x;
 
 x=y;
 y=b;
}

void swap(int& x,int& y)
{
 const int n=x;
 
 x=y;
 y=n;
}

void swap(unsigned& x,unsigned& y)
{
 const unsigned n=x;
 
 x=y;
 y=n;
}

void swap(long& x,long& y)
{
 const long n=x;
 
 x=y;
 y=n;
}

void swap(unsigned long& x,unsigned long& y)
{
 const unsigned long n=x;
 
 x=y;
 y=n;
}

void swap(double& x,double& y)
{
 const double n=x;
 
 x=y;
 y=n;
}

void swap(char*& x,char*& y)
{
 char* p=x;
 
 x=y;
 y=p;
}

void swap(FILE*& x,FILE*& y)
{
 FILE* p=x;
 
 x=y;
 y=p;
}

void swap(ptr& x,ptr& y)
{
 swap(x.pointer,y.pointer);
 swap(x.length,y.length);
}

void swap(mem& x,mem& y)
{
 swap(x.pointer,y.pointer);
}

void swap(buf& x,buf& y)
{
 swap(x.index,y.index);
 swap(x.length,y.length);
 swap(x.memory,y.memory);
}

void swap(txt& x,txt& y)
{
 swap(x.buffer,y.buffer);
}

void swap(seq& x,seq& y)
{
 swap(x.text,y.text);
}

void swap(fd& x,fd& y)
{
 swap(x.handle,y.handle);
}

void swap(pfile& x,pfile& y)
{
 swap(x.pointer,y.pointer);
}

void swap(map& x,map& y)
{
 swap(x.keys,y.keys);
 swap(x.values,y.values);
}

void swap(grid& x,grid& y)
{
 swap(x.width,y.width);
 swap(x.height,y.height);
 swap(x.cells,y.cells);
}

void swap(tbl& x,tbl& y)
{
 swap(x.columns,y.columns);
 swap(x.cells,y.cells);
}

void swap(timer& x,timer& y)
{
 swap(x.start,y.start);
 swap(x.duration,y.duration);
}

