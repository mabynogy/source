seq delimit(const mem& x)
{
 seq r;
 buf b=to_buf(x);
 
 while(is_full(b))
 {
  const char c=shift(b);
  
  if(is_empty(r))
  {
   mem m;
   
   push(m,c);   
   push(r,m);
   
   continue;
  }
  
  const mem left=back(r);
  mem right;
  
  push(right,c);
  
  if(is_alnum(left)&&is_alnum(right))
  {
   const mem m=concat(left,right);
   
   drop_r(r);
   push(r,m);
  }
  else if(is_space(left)&&is_space(right))
  {
   const mem m=concat(left,right);
   
   drop_r(r);
   push(r,m);
  }
  else if(is_nl(left)&&is_nl(right))
  {
   const mem m=concat(left,right);
   
   drop_r(r);
   push(r,m);
  }
  else
   push(r,right);  
 }
 
 return r;
}
