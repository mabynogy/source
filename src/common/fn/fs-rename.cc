void fs_rename(const mem& x,const mem& y)
{
 check(is_fs(x));

 const mem mx=to_c(x); 
 const mem my=to_c(y); 
 const char* px=get_address(mx);
 const char* py=get_address(my);
 const int n=rename(px,py);
 
 check(n==0);
}
