long to_long(const mem& x)
{
 const mem m=to_c(x); 
 const char* p=get_address(m);
 const long r=strtol(p,0,10);
 
 check(r!=LONG_MIN);
 check(r!=LONG_MAX);
 
 return r;
}
