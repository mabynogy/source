bool eq(const mem& x,const mem& y)
bool eq(const buf& x,const buf& y)
{
 const int lx=get_length(x);
 const int ly=get_length(y);
 
 if(lx!=ly)
  return false;
 
 for(int i=0;i<lx;i++)
 {
  const char cx=at(x,i);
  const char cy=at(y,i);
  
  if(cx!=cy)
   return false;
 }
 
 return true;
}
