bool is_fs(const mem& x)
{
 if(is_dir(x))
  return true;

 if(is_file(x))
  return true;
  
 return false;
}
