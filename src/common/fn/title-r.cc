void title_r(const mem& x)
{
 title_r(x,"*");
}

void title_r(const mem& x,const mem& y)
{
 check(is_full(x));
 check(is_full(y));
 
 const int width=term_width(); 
 const mem m1=repeat(y,2);
 const mem m2=repeat(y,width);
 const mem m3=concat(m2," ",x," ",m1);
 const mem m4=tail(m3,width);
 
 print(m4);
}
