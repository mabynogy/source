double get_time()
{
 timespec spec;
 
 const int n=clock_gettime(CLOCK_MONOTONIC,&spec);
 
 check(n==0);
 
 const double s=spec.tv_sec;
 const double t=spec.tv_nsec/10e9;
 
 return s+t;
}
