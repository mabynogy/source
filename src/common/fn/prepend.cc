void prepend(mem& x,const mem& y)
{
 const int lx=get_length(x);
 const int ly=get_length(y);
 const int l=lx+ly;
 
 allocate(x,l);
 copy(x,lx,0,lx);
 copy(x,0,y,0,ly);
}

void prepend(buf& x,const char* y)
{
 const mem m=y;
 
 prepend(x,m);
}

void prepend(buf& x,const mem& y)
void prepend(buf& x,const buf& y)
{
 const int lx=get_length(x);
 const int ly=get_length(y);
 const int l=lx+ly;
 
 reserve(x,l);
 
 for(int i=0;i<ly;i++)
 {
  const char c=back(y,i);
  
  unshift(x,c);
 }
}

void prepend(txt& x,const txt& y)
{
 prepend(x.buffer,y.buffer);
}

void prepend(seq& x,const seq& y)
{
 prepend(x.text,y.text);
}
