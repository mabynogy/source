mem to_lit(const char x)
{
 const mem m=escape(x);
 
 return single_quote(m);
}

mem to_lit(const mem& x)
{
 const mem m=escape(x);
 
 return quote(m);
}

mem to_lit(const buf& x)
{
 const mem m=to_mem(x);
 
 return to_lit(m);
}
