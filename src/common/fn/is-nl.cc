bool is_nl(const char x)
{
 if(x=='\n')
  return true;

 if(x=='\r')
  return true;

 return false;
}

bool is_nl(const mem& x)
{
 if(is_empty(x))
  return true;
  
 const int l=get_length(x);
  
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  
  if(!is_nl(c))
   return false;
 }
 
 return false;
}
