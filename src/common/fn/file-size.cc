long file_size(const mem& x)
{
 check(is_file(x));
 
 const struct stat s=fs_stat(x);
 
 return s.st_size;
}
