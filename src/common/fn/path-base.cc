mem path_base(const mem& x)
{
 mem m=to_c(x);
 char* p=get_address(m);
 
 return basename(p);
}
