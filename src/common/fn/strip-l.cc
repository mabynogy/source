mem strip_l(const mem& x,const char y)
mem strip_l(const buf& x,const char y)
{
 if(match_l(x,y))
  return slice(x,1);
  
 return x;
}

mem strip_l(const mem& x,const mem& y)
mem strip_l(const buf& x,const mem& y)
{
 if(match_l(x,y))
 {
  const int l=get_length(y);
  
  return slice(x,l);
 }
  
 return x;
}
