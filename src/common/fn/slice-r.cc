mem slice_r(const mem& x,const int y)
mem slice_r(const buf& x,const int y)
{
 const int l=get_length(x);
 
 check(y>=0);
 check(y<=l);
 
 const int n=l-y;

 return slice(x,n,y);
}

txt slice_r(const txt& x,const int y)
{
 const int l=count(x);

 check(y>=0);
 check(y<=l);
 
 const int n=l-y;

 return slice(x,n,y);
}

seq slice_r(const seq& x,const int y)
{
 const txt t=slice_r(x.text,y);

 return to_seq(t); 
}
