bool is_circular(const buf& x)
{
 const int l=get_capacity(x);
 const int n=x.index+x.length;

 return n>l;
}
