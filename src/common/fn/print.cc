void print()
{
 echo("\n");
}

void print(const bool x)
void print(const char x)
void print(const int x)
void print(const unsigned x)
void print(const long x)
void print(const unsigned long x)
void print(const double x)
void print(const void* x)
void print(const mem& x)
void print(const buf& x)
{
 echo(x);
 echo("\n");
}

void print(const char* x)
{
 check(x!=0);

 echo(x);
 echo("\n");
}

void print(const txt& x)
{
 dump(x.buffer);
}

void print(const seq& x)
{
 dump(x.text);
}

void print(const map& x)
{
 dump(x);
}
