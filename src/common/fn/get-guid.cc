mem get_guid()
{
 return get_guid(16);
}

mem get_guid(const int x)
{
 check(x>0);
 
 buf b;
 
 reserve(b,x);
 
 for(int i=0;i<x;i++)
 {
  const int n=get_random(36);
  
  if(n<10)
  {
   const char c='0'+n;
   
   push(b,c);
  }
  else
  {
   const int v=n-10;
   const char c='a'+v;
   
   push(b,c);
  }
 }
 
 return detach(b);
}
