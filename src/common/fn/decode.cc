mem decode(const mem& x)
{
 buf output;
 buf input=to_buf(x);

 const mem bn="\\n";
 const mem bs="\\\\";  
 
 const int ln=get_length(bn);
 const int ls=get_length(bs);
 
 while(is_full(input))
 {
  if(match_l(input,bn))
  {
   shift(input,ln);   
   push(output,'\n');
  }
  else if(match_l(input,bs))
  {
   shift(input,ls);   
   push(output,'\\');
  }
  else
  {
   const char c=shift(input);
   
   push(output,c);
  }
 }
 
 return detach(output);
}
