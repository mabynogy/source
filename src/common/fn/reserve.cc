void reserve(buf& x,const int y)
{
 check(y>=0);
 
 const int lx=x.length;
 const int l=get_capacity(x);
 
 if(y<=lx)
  return;
 
 if(y<=l)
  return;
 
 buf b;
 
 swap(b,x);
 allocate(x.memory,y);
 
 x.length=lx;
  
 copy(x,0,b,0,lx);
}
