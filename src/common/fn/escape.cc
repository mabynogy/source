mem escape(const char x)
{
 if(x==' ')
  return " ";

 if(x=='\r')
  return "\\r";

 if(x=='\n')
  return "\\n";

 if(x=='\t')
  return "\\t";

 if(x=='"')
  return "\\\"";

 if(is_punct(x))
 {
  mem r;
  
  push(r,x);
  
  return r;
 }

 if(is_alnum(x))
 {
  mem r;
  
  push(r,x);
  
  return r;
 }
 
 const unsigned char u=x;
 const mem m1=to_hexa(u);
 const mem m2=pad_l(m1,2,'0');
 
 return concat("\\x",m2);
}

mem escape(const mem& x)
{
 const int l=get_length(x);
 
 buf b;
 
 reserve(b,l);
 
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  const mem m=escape(c);
  
  append(b,m);
 }
 
 return detach(b);
}
