void serialize(mem& x,const txt& y)
{
 mem m=to_mem(y);
 
 move(x,m);
}

void serialize(mem& x,const map& y)
{
 map m=y;
 txt t;
 
 while(is_full(m))
 {
  const mem m1=shift(m.keys);
  const mem m2=shift(m.values);
  const mem key=encode(m1);
  const mem value=encode(m2);
  
  push(t,key);
  push(t,value);
 }
 
 serialize(x,t); 
}
