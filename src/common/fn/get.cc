mem get(const map& x,const mem& y)
{
 check(is_full(y));
 
 map m=x;
 
 while(is_full(m))
 {
  const mem k=shift(m.keys);
  const mem v=shift(m.values);
  
  if(eq(k,y))
   return v;
 }
 
 stop();
 
 return {};
}
