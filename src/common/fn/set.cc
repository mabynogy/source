void set(mem& x,const int y,const char z)
{
 const int l=get_length(x);

 check(y>=0);
 check(y<l);
 
 char* p=get_address(x,y);
 
 *p=z;
}

void set(buf& x,const int y,const char z)
{
 const int l=get_length(x);

 check(y>=0);
 check(y<l);

 const int n=get_offset(x,y);
 
 set(x.memory,n,z);
}

void set(map& x,const mem& y,const mem& z)
{
 check(!has(x,y));
 
 push(x.keys,y);
 push(x.values,z);
}
