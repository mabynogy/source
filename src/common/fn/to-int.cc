int to_int(const mem& x)
{
 const long l=to_long(x);
 const int r=l;
 const long n=r;
 
 check(l==n);
 
 return r;  
}
