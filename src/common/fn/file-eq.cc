bool file_eq(const mem& x,const mem& y)
{
 check(is_file(x));
 check(is_file(y));

 const long lx=file_size(x);
 const long ly=file_size(y);
 
 if(lx!=ly)
  return false;
  
 const mem mx=load(x);
 const mem my=load(y);
 
 return eq(mx,my);
}
