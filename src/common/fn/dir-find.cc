txt dir_find(const mem& x)
{
 check(is_dir(x));

 txt r;
 
 const mem qx=quote(x);
 const mem command=concat("find ",qx);
 const mem result=os_execute(command);
 
 txt t=to_txt(result);
 
 while(is_full(t))
 {
  const mem m1=shift(t);
  const mem m2=path_real(m1);
  
  if(is_file(m2))
   push(r,m2);
 }
 
 sort(r);
 
 return r;
}
