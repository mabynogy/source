void set_non_block(const fd& x)
{
 const int n1=fcntl(x.handle,F_GETFL,0);
 
 check(n1!=-1);
 
 const int n2=n1|O_NONBLOCK; 
 const int n3=fcntl(x.handle,F_SETFL,n2);
 
 check(n3!=-1);
}
