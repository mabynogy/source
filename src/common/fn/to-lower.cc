char to_lower(const char x)
{
 return tolower(x);
}

mem to_lower(const mem& x)
{
 mem r;
 
 const int l=get_length(x);
 
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  const char n=to_lower(c);
  
  push(r,n);
 }
 
 return r;
}
