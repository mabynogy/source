bool is_alnum(const char x)
{
 if(x=='_')
  return true;
  
 if(is_alpha(x))
  return true;

 if(is_digit(x))
  return true;
 
 return false;
}

bool is_alnum(const mem& x)
{
 if(is_empty(x))
  return false;
 
 const int l=get_length(x);
 
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  
  if(!is_alnum(c))
   return false;
 }
 
 return true;
}
