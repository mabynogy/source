mem slice(const mem& x,const int y)
mem slice(const buf& x,const int y)
{
 const int l=get_length(x);
 
 check(y>=0);
 check(y<=l);
 
 const int ly=l-y;
 
 return slice(x,y,ly);
}

txt slice(const txt& x,const int y)
{
 const int l=count(x);
 
 check(y>=0);
 check(y<=l);
 
 const int ly=l-y;
 
 return slice(x,y,ly);
}

seq slice(const seq& x,const int y)
{
 const txt t=slice(x.text,y);
 
 return to_seq(t);
}

mem slice(const mem& x,const int y,const int z)
mem slice(const buf& x,const int y,const int z)
{
 const int l=get_length(x);
 
 check(y>=0);
 check(y<=l);
 check(z>=0);
 check(y+z<=l);
 
 mem r;
 
 allocate(r,z);
 copy(r,0,x,y,z);
 
 return r;
}

txt slice(const txt& x,const int y,const int z)
{
 const int l=count(x);

 check(y>=0);
 check(y<=l);
 check(z>=0);
 check(y+z<=l);
 
 const int ly=l-y-z;
 
 txt r=x;
 
 drop_l(r,y);
 drop_r(r,ly); 
 
 return r;
}

seq slice(const seq& x,const int y,const int z)
{
 const txt t=slice(x.text,y,z);
 
 return to_seq(t);
}
