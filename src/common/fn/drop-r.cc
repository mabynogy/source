void drop_r(mem& x)
{
 check(is_full(x));

 drop_r(x,1); 
}

void drop_r(mem& x,const int y)
{
 const int l=get_length(x);
 
 check(y>=0);
 check(y<=l);
 
 const int ly=l-y;
  
 allocate(x,ly);
}

void drop_r(buf& x)
{
 check(is_full(x));
 
 drop_r(x,1);
}

void drop_r(buf& x,const int y)
{
 const int l=get_length(x);
 
 check(y>=0);
 check(y<=l);

 x.length-=y; 
}

void drop_r(txt& x)
{
 check(is_full(x));
 
 drop_r(x.buffer);
  
 while(is_full(x.buffer))
 {
  const char c=back(x.buffer);
  
  if(c=='\n')
   break;

  drop_r(x.buffer);
 }
}

void drop_r(seq& x)
{
 check(is_full(x));
 
 drop_r(x.text);
}

void drop_r(txt& x,const int y)
void drop_r(seq& x,const int y)
{
 check(y>=0);
 
 for(int i=0;i<y;i++)
 {
  drop_r(x);
 }
}
