mem glue(const txt& x)
mem glue(const seq& x)
{
 return glue(x,'\n');
}

mem glue(const txt& x,const char y)
mem glue(const seq& x,const char y)
{
 mem r=join(x,y);
 
 if(is_empty(r))
  return r;
 
 check(match_r(r,y));
  
 drop_r(r);
 
 return r;
}

mem glue(const txt& x,const mem& y)
mem glue(const seq& x,const mem& y)
{
 mem r=join(x,y);
 
 if(is_empty(r))
  return r;
 
 check(match_r(r,y));
 
 const int l=get_length(y);
 
 drop_r(r,l);
 
 return r;
}
