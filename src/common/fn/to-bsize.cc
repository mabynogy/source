mem to_bsize(const long x)
{
 check(x>=0);

 const double v=x;  
 const double kb=1024;
 const double mb=1024*kb;
 const double gb=1024*mb;
 const double tb=1024*gb;
 
 if(v<kb)
 {
  const mem m=to_mem(x);
  
  return concat(m,"b");
 }

 if(v<mb)
 {
  const double n=v/kb;
  const mem m=to_mem(n,2);
  
  return concat(m,"Kb");
 }

 if(v<gb)
 {
  const double n=v/mb;
  const mem m=to_mem(n,2);
  
  return concat(m,"Mb");
 }

 if(v<tb)
 {
  const double n=v/gb;
  const mem m=to_mem(n,2);
  
  return concat(m,"Gb");
 }

 const double n=v/tb;
 const mem m=to_mem(n,2);
 
 return concat(m,"Tb");
}
