bool contain(const mem& x,const char y)
{
 if(is_empty(x))
  return false;
  
 const int l=get_length(x);
 const void* p=get_address(x);
 const void* pp=memchr(p,y,l);
 
 return pp!=0;
}

bool contain(const mem& x,const mem& y)
{
 if(is_empty(x))
  return false;

 if(is_empty(y))
  return false;

 const int lx=get_length(x);
 const int ly=get_length(y);
 
 const char* px=get_address(x);
 const char* py=get_address(y);

 void* p=memmem(px,lx,py,ly);
 
 return p!=0;
}

bool contain(const txt& x,const mem& y)
{
 txt t=x;
 
 while(is_full(t))
 {
  const mem m=shift(t);
  
  if(eq(m,y))
   return true;
 }
 
 return false;
}

bool contain(const seq& x,const mem& y)
{
 const mem m=encode(y);
 
 return contain(x.text,m);
}
