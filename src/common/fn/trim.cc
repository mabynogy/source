mem trim(const mem& x)
{
 const mem s=trim_l(x);
 
 return trim_r(s);
}
