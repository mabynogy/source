bool is_text(const mem& x)
{
 if(contain(x,'\r'))
  return true;

 if(contain(x,'\n'))
  return true;
 
 return false;
}
