mem trim_l(const mem& x)
{
 buf b=to_buf(x);
 
 while(is_full(b))
 {
  const char c=front(b);
  
  if(!is_blank(c))
   break;
   
  shift(b);
 }
 
 return detach(b);
}

txt trim_l(const txt& x)
{
 txt r;
 txt t=x;
 
 while(is_full(t))
 {
  const mem m1=shift(t);
  const mem m2=trim_l(m1);
  
  push(r,m2);
 }
  
 return r;
}
