timer timeout(const double x)
{
 check(x>=0);

 timer r;
 
 r.duration=x;
 
 return r; 
}
