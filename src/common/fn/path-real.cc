mem path_real(const mem& x)
{
 const mem m=to_c(x);

 mem result;
 
 allocate(result,PATH_MAX);
 
 const char* p=get_address(m);
 char* presult=get_address(result);
 char* r=realpath(p,presult);
 
 check(r!=0);
 
 return r;
}
