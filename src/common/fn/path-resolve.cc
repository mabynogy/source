mem path_resolve(const mem& x)
{
 if(is_fs(x))
  return path_real(x);
  
 const mem dir1=path_dir(x);
 
 if(is_dir(dir1))
 {
  const mem dir2=path_real(dir1);
  const mem base=path_base(x);
  
  return path_concat(dir2,base);
 }
  
 return x;
}
