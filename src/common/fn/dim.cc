void dim(buf& x,const int y)
{
 check(y>=0);
 
 reserve(x,y);
 
 x.length=y;
}
