bool is_blank(const char x)
{
 if(x==' ')
  return true;

 if(x=='\t')
  return true;

 if(x=='\n')
  return true;

 if(x=='\r')
  return true;
 
 return false;
}

bool is_blank(const mem& x)
{
 if(is_empty(x))
  return true;
  
 const int l=get_length(x);
  
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  
  if(!is_blank(c))
   return false;
 }
 
 return true;
}
