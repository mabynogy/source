txt dir_list(const mem& x)
{
 check(is_dir(x));

 txt r;
 
 const mem dir=path_real(x);
 const mem qdir=quote(dir); 
 const mem command=concat("ls ",qdir);
 const mem result=os_execute(command);
 
 txt t=to_txt(result);
 
 while(is_full(t))
 {
  const mem m1=shift(t);
  const mem m2=path_concat(x,m1);
  const mem m3=path_real(m2);
  
  if(eq(m3,dir))
   continue;
  
  push(r,m3);
 }
 
 sort(r);
 
 return r;
}
