void normalize(buf& x)
{
 if(is_normal(x))
  return;
  
 mem m=to_mem(x);
 
 reset(x); 
 
 swap(x.memory,m);
 
 const int l=get_length(x.memory);
 
 x.length=l;
}
