mem path_unique(const mem& x)
{
 const mem dir=path_dir(x);
 const mem name1=path_name(x);
 const mem ext=path_ext(x);
 
 txt t=split(name1,'-');
 
 const mem last=back(t);
 
 if(is_digit(last))
  drop_r(t);
  
 const mem name2=glue(t,'-');
 
 buf b;
 
 for(int i=0;;i++)
 {
  const mem n=to_mem(i);
  
  reset(b);
  
  append(b,dir);
  append(b,"/");
  append(b,name2);
  
  if(i>0)
  {
   append(b,"-");
   append(b,n);
  }
  
  if(is_full(ext))
  {
   append(b,".");
   append(b,ext);
  }
  
  if(!is_fs(b))
   break;
 }
 
 const mem path=detach(b);
 
 return path_resolve(path);
}
