mem implode(const txt& x)
{
 mem r;
 txt t=x;
 
 while(is_full(t))
 {
  const mem m=shift(t);
  
  append(r,m);
 }
 
 return r;
}

mem implode(const seq& x)
{
 mem r;
 seq s=x;
 
 while(is_full(s))
 {
  const mem m=shift(s);
  
  append(r,m);
 }
 
 return r;
}
