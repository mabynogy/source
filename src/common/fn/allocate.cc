void allocate(mem& x,const int y)
{
 check(y>=0);

 const int l=get_length(x);
 
 if(y==l)
  return;
 
 if(y==0)
 {
  clear(x);
  
  return;
 }
 
 if(is_empty(x))
 {
  x.pointer.pointer=(char*)malloc(y);
  
  check(x.pointer.pointer!=0);
 }
 else
 {
  char* pointer=get_address(x);
  char* p=(char*)realloc(pointer,y);
  
  check(p!=0);
  
  x.pointer.pointer=p;
 }
 
 x.pointer.length=y;
}
