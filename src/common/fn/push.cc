void push(mem& x,const char y)
{
 const int l=get_length(x);
 const int lx=l+1;
 
 allocate(x,lx);
 back(x,0,y);
}

void push(buf& x,const char y)
{
 if(is_filled(x))
 {
  const int l=get_capacity(x)+1024;

  reserve(x,l);
 }
 
 x.length++;

 back(x,0,y);
}

void push(txt& x,const mem& y)
{
 check(!contain(y,'\n'));
 
 append(x.buffer,y);
 push(x.buffer,'\n');
}

void push(seq& x,const mem& y)
{
 const mem m=encode(y);
 
 push(x.text,m);
}
