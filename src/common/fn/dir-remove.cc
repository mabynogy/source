void dir_remove(const mem& x)
{
 check(is_dir(x));

 const mem qx=quote(x); 
 const mem m=concat("rm -rf ",qx);
 
 os_execute(m);
}
