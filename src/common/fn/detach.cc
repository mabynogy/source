ptr detach(ptr& x)
{
 const ptr r=x;
 
 clear(x);
 
 return r;
}

ptr detach(mem& x)
{
 return detach(x.pointer);  
}

mem detach(buf& x)
{
 if(is_normal(x))
 {
  mem r;
  
  swap(r,x.memory);
  reset(x);
  
  return r;
 }
 
 const mem r=to_mem(x); 
 
 clear(x);
 
 return r;
}
