char at(const mem& x,const int y)
{
 const int l=get_length(x);
 
 check(y>=0);
 check(y<l);
 
 const char* p=get_address(x,y);
 
 return *p;
}

char at(const buf& x,const int y)
{
 const int l=get_length(x);

 check(y>=0);
 check(y<l);

 const int n=get_offset(x,y);
 
 return at(x.memory,n);
}

mem at(const txt& x,const int y)
{
 check(y>=0);
 check(y<count(x));
 
 txt t=x;
 
 drop_l(t,y);
 
 return shift(t);
}

mem at(const seq& x,const int y)
{
 const mem m=at(x.text,y);
 
 return decode(m);
}
