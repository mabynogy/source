txt split(const mem& x,const char y)
{
 txt r;
 buf b1=to_buf(x);
 buf b2;

 while(is_full(b1))
 {
  const char c=shift(b1);   

  if(c==y)
  {
   const mem m=detach(b2);
   
   push(r,m);
  }
  else
   push(b2,c);
 }  

 if(is_full(b2))
 {
  const mem m=detach(b2);
  
  push(r,m);
 }

 return r; 
}

txt split(const mem& x,const mem& y)
{
 check(is_full(y));
 
 txt r;
 buf b1=to_buf(x);
 buf b2;
 
 const int l=get_length(y);

 while(is_full(b1))
 {
  if(match_l(b1,y))
  {
   shift(b1,l);
   
   const mem m=detach(b2);
   
   push(r,m);
  }
  else
  {
   const char c=shift(b1);
   
   push(b2,c);
  }
 }  

 if(is_full(b2))
 {
  const mem m=detach(b2);
  
  push(r,m);
 }

 return r; 
}
