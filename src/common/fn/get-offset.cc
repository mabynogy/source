int get_offset(const buf& x,const int y)
{
 const int l=get_length(x);
 
 check(y>=0);
 check(y<l);

 const int lx=get_capacity(x);
 const int n=x.index+y;
 
 return n%lx;
}
