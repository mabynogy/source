mem to_mem(const bool x)
{
 if(x)
  return "true";
  
 return "false";
}

mem to_mem(const char x)
{
 mem r;
 
 push(r,x);
 
 return r;
}

mem to_mem(const int x)
{
 mem r;
 
 allocate(r,256);
 
 const int l=get_length(r);
 char* p=get_address(r);
 const int n=sprintf(p,"%d",x);
 
 check(n>=0);
 check(n<l);
 
 allocate(r,n);
 
 return r;
}

mem to_mem(const unsigned x)
{
 mem r;
 
 allocate(r,256);

 const int l=get_length(r);
 char* p=get_address(r);
 const int n=sprintf(p,"%u",x);
 
 check(n>=0);
 check(n<l);
 
 allocate(r,n);
 
 return r;
}

mem to_mem(const long x)
{
 mem r;
 
 allocate(r,256);

 const int l=get_length(r);
 char* p=get_address(r);
 const int n=sprintf(p,"%ld",x);
 
 check(n>=0);
 check(n<l);
 
 allocate(r,n);
 
 return r;
}

mem to_mem(const unsigned long x)
{
 mem r;
 
 allocate(r,256);

 const int l=get_length(r);
 char* p=get_address(r);
 const int n=sprintf(p,"%lu",x);
 
 check(n>=0);
 check(n<l);
 
 allocate(r,n);
 
 return r;
}

mem to_mem(const double x)
{
 mem r;
 
 allocate(r,256);

 const int l=get_length(r);
 char* p=get_address(r);
 const int n=sprintf(p,"%f",x);
 
 check(n>=0);
 check(n<l);
 
 allocate(r,n);
 
 while(is_full(r))
 {
  const char c=back(r);
  
  if(c!='0')
   break;
   
  drop_r(r);
 }
 
 const char c=back(r);
  
 if(c=='.')
  drop_r(r);
 
 return r;
}

mem to_mem(const double x,const int y)
{
 check(y>=0);
 
 const mem m=to_mem(x);
 
 if(!contain(m,'.'))
  return m;
  
 txt t=split(m,'.');
 
 const mem integer=shift(t);
 const mem real=shift(t);
 
 check(is_empty(t));
 
 mem round=head(real,y);
 
 if(is_empty(round))
  return integer;
  
 while(is_full(round))
 {
  const char c=back(round);
  
  if(c!='0')
   break;
  
  pop(round);
 }

 if(is_empty(round))
  return integer;
 
 return concat(integer,".",round);
}

mem to_mem(const void* x)
{
 mem r;
 
 allocate(r,256);

 const int l=get_length(r);
 char* p=get_address(r);
 const int n=sprintf(p,"%p",x);
 
 check(n>=0);
 check(n<l);
 
 allocate(r,n);
 
 return r;
}

mem to_mem(const char* x)
{
 check(x!=0);

 return x;
}

mem to_mem(const buf& x)
{
 return x;
}

mem to_mem(const txt& x)
{
 return to_mem(x.buffer);
}

mem to_mem(const seq& x)
{
 return to_mem(x.text);
}

mem to_mem(const map& x)
{
 mem r;
 
 serialize(r,x);
 
 return r;
}
