bool is_empty(const ptr& x)
bool is_empty(const buf& x)
{
 return x.length==0;
}

bool is_empty(const mem& x)
{
 return is_empty(x.pointer);
}

bool is_empty(const txt& x)
{
 if(is_full(x.buffer))
  check(back(x.buffer)=='\n');

 return is_empty(x.buffer);
}

bool is_empty(const seq& x)
{
 return is_empty(x.text);
}

bool is_empty(const fd& x)
{
 return x.handle<0;
}

bool is_empty(const pfile& x)
bool is_empty(const pthread& x)
{
 return x.pointer==0;
}

bool is_empty(const map& x)
{
 return is_empty(x.keys);
}

bool is_empty(const grid& x)
{
 return is_empty(x.cells);
}

bool is_empty(const tbl& x)
{
 return is_empty(x.cells);
}
