mem replace(const mem& x,const char y,const char z)
{
 mem r=x;
 
 const int l=get_length(r);
 
 for(int i=0;i<l;i++)
 {
  const char c=at(r,i);
  
  if(c==y)
   set(r,i,z);
 }
 
 return r;
}

mem replace(const mem& x,const mem& y,const mem& z)
{
 if(is_empty(y))
  return x;
  
 buf bx=to_buf(x);
 buf br;
 
 while(is_full(bx))
 {
  if(match_l(bx,y))
  {
   const int l=get_length(y);
 
   shift(bx,l);
   append(br,z);
  }
  else
  {
   const char c=shift(bx);
   
   push(br,c);
  }
 }
 
 return detach(br);
}
