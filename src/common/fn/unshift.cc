void unshift(mem& x,const char y)
{
 const int lx=get_length(x);
 const int l=lx+1;
 
 allocate(x,l);
 copy(x,1,0,lx);
 front(x,y);
}

void unshift(buf& x,const char y)
{
 if(is_filled(x))
 {
  const int l=get_capacity(x);
  const int lx=l+1024;
  
  reserve(x,lx);
 }
 
 if(x.index==0)   
 {
  const int l=get_capacity(x);
  const int lx=l-1;

  x.index=lx;
 }
 else 
  x.index--;
  
 x.length++;
 
 front(x,y);
}

void unshift(txt& x,const mem& y)
{
 check(!contain(y,'\n'));
 
 unshift(x.buffer,'\n');
 prepend(x.buffer,y);
}

void unshift(seq& x,const mem& y)
{
 const mem m=encode(y);
 
 unshift(x.text,m);
}
