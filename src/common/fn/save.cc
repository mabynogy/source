void save(const mem& x,const mem& y)
{
 if(is_file(x))
 {
  const mem m=load(x);
  
  if(eq(m,y))
   return;  
 }
 
 const mem dir=path_dir(x);
 
 if(!is_dir(dir))
  dir_make(dir);
 
 fd f;
 
 const int flag=O_WRONLY|O_CREAT|O_TRUNC;
 
 open(f,x,flag); 
 write(f,y);
}
