void wait(const double x)
{
 const double t=x*10e5;
 const int n=usleep(t);
 
 check(n==0);
}
