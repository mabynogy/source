void file_copy(const mem& x,const mem& y)
{
 check(is_file(x));
 
 if(is_dir(y))
 {
  const mem base=path_base(x);
  const mem m=path_concat(y,base);
  
  file_copy(x,m);
  
  return;
 }
 
 const mem qx=quote(x);
 const mem qy=quote(y); 
 const mem m=concat("cp -f --preserve ",qx," ",qy);
 
 os_execute(m);
}
