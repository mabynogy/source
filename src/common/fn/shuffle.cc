void shuffle(txt& x)
{
 txt t;
 
 move(t,x);
 
 while(is_full(t))
 {
  const int l=count(t);
  const int n=get_random(l);  
  const mem m=at(t,n);
  
  push(x,m);
  remove(t,n);
 }
}

void shuffle(seq& x)
{
 shuffle(x.text);
}
