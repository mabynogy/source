mem term_read()
{
 mem r;
 fd f;
 
 f.handle=dup(STDIN_FILENO);

 termios t1;

 const int n0=tcgetattr(f.handle,&t1);

 check(n0==0);

 const termios t2=t1;
 
 t1.c_lflag&=~ICANON;
 t1.c_lflag&=~ECHO;

 const int n1=tcsetattr(f.handle,TCSANOW,&t1); 

 check(n1==0);
 
 const int n2=fcntl(f.handle,F_GETFL);
 
 check(n2!=-1);
 
 const int a=n2|O_NONBLOCK; 
 const int n3=fcntl(f.handle,F_SETFL,a);
  
 check(n3!=-1);
 
 char c=0;
 
 const int n4=read(f.handle,&c,1);
 
 if(n4==1)
 {
  push(r,c);
   
  while(true)
  {
   const int n5=read(f.handle,&c,1);

   if(n5<=0)
    break;

   push(r,c);
  }
 }
 
 const int n6=tcsetattr(f.handle,TCSANOW,&t2); 

 check(n6==0); 
 
 return r;
}

