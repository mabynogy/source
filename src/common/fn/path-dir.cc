mem path_dir(const mem& x)
{
 mem m=to_c(x);
 char* p=get_address(m);
 
 return dirname(p);
}
