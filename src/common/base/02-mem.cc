struct mem
{
 ptr pointer;

 mem()
 {
 }
 
 mem(const char* x)
 {
  assign(*this,x);
 }
 
 mem(const mem& x)
 {
  assign(*this,x);
 }

 mem(const buf& x)
 {
  assign(*this,x);
 }

 mem(mem&& x)
 {
  swap(*this,x);
 }
 
 ~mem()
 {
  destruct(*this);
 }
 
 void operator =(const char* x)
 {
  assign(*this,x);
 } 

 void operator =(const mem& x)
 {
  assign(*this,x);
 }

 void operator =(const buf& x)
 {
  assign(*this,x);
 }

 void operator =(mem&& x)
 {
  swap(*this,x);
 } 
};
