struct txt
{ 
 buf buffer;
 
 txt()
 {
 }
 
 txt(const txt& x)
 {
  assign(*this,x);
 }

 txt(txt&& x)
 {
  swap(*this,x);
 }
 
 void operator =(const txt& x)
 {
  assign(*this,x);
 }

 void operator =(txt&& x)
 {
  swap(*this,x);
 }
};
