struct buf
{
 int index=0;
 int length=0;
 mem memory;

 buf()
 {
 }
 
 buf(const char* x)
 {
  assign(*this,x);
 }
 
 buf(const buf& x)
 {
  assign(*this,x);
 }

 buf(buf&& x)
 {
  swap(*this,x);
 }
 
 void operator =(const char* x)
 {
  assign(*this,x);
 } 

 void operator =(const buf& x)
 {
  assign(*this,x);
 }

 void operator =(buf&& x)
 {
  swap(*this,x);
 } 
};
