struct seq
{ 
 txt text;
 
 seq()
 {
 }
 
 seq(const seq& x)
 {
  assign(*this,x);
 }

 seq(seq&& x)
 {
  swap(*this,x);
 }
 
 void operator =(const seq& x)
 {
  assign(*this,x);
 }

 void operator =(seq&& x)
 {
  swap(*this,x);
 }
};
