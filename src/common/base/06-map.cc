struct map
{
 seq keys;
 seq values;

 map()
 {
 }
 
 map(const map& x)
 {
  assign(*this,x);
 }

 map(map&& x)
 {
  assign(*this,x);
 }
 
 void operator =(const map& x)
 {
  assign(*this,x);
 }

 void operator =(map&& x)
 {
  swap(*this,x);
 }
};
