mem to_name(const mem& x,const int y)
{
 check(y>=0);
 
 const mem m1=translit(x);
 const mem m2=to_lower(m1);
 mem m3;
 
 const int l2=get_length(m3);
 
 for(int i=0;i<l2;i++)
 {
  const char c=at(m2,i);
  
  if(c=='_')
   push(m3,' ');
  else if(is_alnum(c))
   push(m3,c);
  else
   push(m3,' ');
 }
 
 txt t1=split(m3,' ');
 txt t2;

 while(is_full(t1))
 {
  const mem m=shift(t1);

  if(is_empty(m))
   continue;
  
  if(is_digit(m))
   continue;
   
  push(t2,m);
 } 
 
 txt t3=t2;
 
 while(is_full(t3))
 {
  const mem m=join(t3);
  const int l=get_length(m);
  
  if(l<=y)
   break;
  
  drop_l(t3);
 }
 
 return glue(t3,'-');
}

mem make_path(const mem& x)
{
 check(is_file(x));
 
 const mem dir=path_dir(x);
 const mem name=path_name(x);
 const mem ext=to_lower(path_ext(x)); 
 const mem prefix=to_name(dir,20);
 const mem suffix=to_name(name,40);
 const int time=file_mtime(x);
 const mem date1=get_date(time);
 const mem date2=replace(date1,'/',' ');
 const txt date3=split(date2,' ');
 const txt date4=slice_l(date3,3);
 const mem date5=glue(date4,'-'); 
 
 if(is_empty(suffix))
  return concat(prefix,"-",date5,"-image.",ext);

 return concat(prefix,"-",date5,"-",suffix,".",ext);
}

void deduplicate()
{
 txt t1=dir_find("/home/mayor/Images/rose-abiven-a");
 txt t2;
 
 while(is_full(t1))
 {
  const mem left=shift(t1);
  const mem ext=to_lower(path_ext(left));
  const mem base=path_base(left);
  const int n=file_size(left);
  
  if(neq(ext,"jpg"))
   continue;
  
  print(concat(to_mem(count(t1))," ",to_mem(count(t2))," ",to_bsize(n)," ",base));
  
  bool found=false;
  txt t3=t2;
  
  while(is_full(t3))
  {
   const mem right=shift(t3);
   
   if(file_eq(left,right))
   {
    found=true;
    
    break;
   }
  }
  
  if(found)  
   print(concat("found ",left));
  else
   push(t2,left);
   
  //if(count(t2)>50)
  // break;
 }
 
 print("filter ok");
 
 txt t3=t2;
 
 while(is_full(t3))
 {
  const mem m1=shift(t3);
  const mem m2=make_path(m1);
  const mem m3=path_concat("/home/mayor/Images/rose-abiven-b",m2);
  const mem m4=path_unique(m3);
  
  print();
  print(m1);
  print(m4);
  file_copy(m1,m4);
 }
}

void compress()
{
 txt t1=dir_list("/home/mayor/Images/rose-abiven-b");
 
 while(is_full(t1))
 {
  const mem m=shift(t1);  
  const long l1=file_size(m);
  
  try
  {
   os_execute(concat("jpegoptim --size=900k ",m));
  }
  catch(...)
  {
   print("failed");
   print(m);
   print();
   
   continue;   
  }

  const long l2=file_size(m);
  const long l=abs(l1-l2);
  
  print(concat(to_mem(count(t1))," ",to_bsize(l)));
 }
}

void number()
{
 txt t1=dir_list("/home/mayor/Images/rose-abiven-b");
 int n=1;

 while(is_full(t1))
 {
  const mem m1=shift(t1);  
  const mem m2=path_name(m1);
  txt t2=split(m2,'-');
  const mem m3=back(t2);
  
  if(is_digit(m3))
   drop_r(t2);
   
  const mem m4=glue(t2,'-');
  const mem m5=pad_l(n,5);
  const mem m6=concat(m5,"-",m4,".jpg");
  const mem m7=path_concat("/home/mayor/Images/rose-abiven-c",m6);
  
  print(m1);
  print(m7);
  print();
  
  file_copy(m1,m7);
  
  n++;
 }
}

void entry(const txt& x)
{
 //deduplicate();
 //compress();
 //number();
 print("ok");
}
