void entry(const txt& x)
{
 const mem html=doc_generate();
 const mem tmp=dir_locate("tmp");
 const mem repo="mabiven.github.io";
 const mem work=path_concat(tmp,repo);
 
 dir_clean(work);
 fs_rename(html,work);
 
 os_system("./build --commit");
 
 os_system(concat("out/commit-gcc-g ",work," github-a")); 
 
 os_system("xdg-open https://mabiven.github.io/"); 
}

//~ void entry(const txt& x)
//~ {
 //~ const mem html=doc_generate();
 //~ const mem tmp=dir_locate("tmp");
 //~ const mem repo="doc";
 //~ const mem work=path_concat(tmp,repo);
 //~ const mem public_=path_concat(work,"public");
 //~ const mem config=path_concat(work,".gitlab-ci.yml");
 
 //~ dir_reset(work);
 //~ os_execute(concat("cp -r ",html," ",public_));
 
 //~ txt t2;
 
 //~ push(t2,"image: alpine:latest");
 //~ push(t2,"");
 //~ push(t2,"pages:");
 //~ push(t2,"  stage: deploy");
 //~ push(t2,"  script:");
 //~ push(t2,"  - echo 'ok'");
 //~ push(t2,"  artifacts:");
 //~ push(t2,"    paths:");
 //~ push(t2,"    - public");
 //~ push(t2,"  only:");
 //~ push(t2,"  - main");
 
 //~ save(config,join(t2));
 
 //~ os_system("./build --commit"); 
 //~ os_system(concat("out/commit-gcc-g ",work)); 
//~ }
