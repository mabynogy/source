mem doc_generate()
{
 const mem tmp=dir_locate("tmp");
 const mem work=path_concat(tmp,"doc-gen");
 const mem code=path_concat(work,"code");
 const mem config=path_concat(work,"Doxyfile");

 os_system("./build --build");
 
 dir_reset(work);
 dir_make(code);
 
 txt t1=dir_list("src/program");

 while(is_full(t1))
 {
  const mem left=shift(t1);
  const mem base=path_base(left);
  const mem out="out/out.cc";
  const mem cc=concat(base,".cc");
  const mem right=path_concat(code,cc);
  
  os_system(concat("./build --",base));
  file_copy(out,right);
 }
 
 txt t2;
 
 push(t2,concat("INPUT=",code));
 push(t2,"RECURSIVE=YES");
 push(t2,"EXTRACT_ALL=YES");
 push(t2,"INLINE_SOURCES=YES");
 push(t2,"SOURCE_BROWSER=YES");
 push(t2,"CALL_GRAPH=YES");
 push(t2,"CALLER_GRAPH=YES");
 push(t2,"REFERENCED_BY_RELATION=YES");
 push(t2,"REFERENCES_RELATION=YES");
 push(t2,"HTML_TIMESTAMP=YES");
 push(t2,"PROJECT_NAME=source");
 push(t2,"GENERATE_TREEVIEW=yes");
 push(t2,"GENERATE_LATEX=no");
 push(t2,"HAVE_DOT=yes");
 push(t2,"DOT_NUM_THREADS=16");
 push(t2,"DOT_GRAPH_MAX_NODES=256");
 push(t2,"COLLABORATION_GRAPH=yes");
  
 save(config,join(t2));
 
 os_system(work,concat("doxygen ",config));
 
 return path_concat(work,"html");
}
