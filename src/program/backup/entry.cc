void entry(const txt& x)
{
 const mem tmp=dir_locate("tmp");
 const mem repo="source";
 const mem work=path_concat(tmp,repo);
 
 os_system("./build --build");
 
 const mem cc=load("out/out.cc");
 
 os_system("./build --commit");
 os_system("./build --explore");
 
 dir_reset(work);
 dir_copy("src",work);
 file_copy("build",work);
 file_copy("out/out.cc",path_concat(work,"build.cc"));
 
 const mem m=concat("//g++ build.cc -o build","\n",cc);

 save(path_concat(work,"build.cc"),m);

 os_execute(concat("out/commit-gcc-g ",work," framagit")); 
 os_execute("out/explore-gcc-g"); 
}
