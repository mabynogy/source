map to_map(const http_req& x)
{
 map r;
 
 set(r,"method",x.method);
 set(r,"url",x.url);
 set(r,"version",x.version);
 set(r,"headers",to_mem(x.headers));
 set(r,"content",x.content);
 
 return r;
}

map to_map(const http_resp& x)
{
 map r;

 set(r,"content_type",x.content_type);
 set(r,"content",x.content);
 set(r,"etag",x.etag);

 return r;
}
