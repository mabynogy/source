void clear(http_req& x)
{
 clear(x.method);
 clear(x.url);
 clear(x.version);
 clear(x.headers);
 clear(x.content);
}

void clear(http_resp& x)
{
 clear(x.content_type);
 clear(x.content);
 clear(x.etag);
}
