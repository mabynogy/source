bool receive(const fd& x,http_req& y)
{
 const timer t=timeout(0.1);
 mem m;
 
 while(!hit(t))
 {
  const mem chunk=receive(x);
  
  append(m,chunk);
  
  if(parse(y,m))
   return true;
  
  wait(0.01);
 }

 return false;
}
