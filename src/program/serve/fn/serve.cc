void serve(const int x)
{
 check(x>0);

 map threads;
 //queue q;
 fd s;
 
 listen(s,x);
 
 while(true)
 {
  if(term_hit())
   break;
   
  fd f;
 
  if(!accept(s,f)) 
  {
   wait(0.1);
   
   continue;
  }
  
  const mem ip=get_ip(f);
  const mem domain=get_domain(ip);
  
  print(concat("accept ",to_lit(domain)));
  
  http_req request;
  
  if(receive(f,request))
  {
   print(concat(request.method," ",to_lit(domain)," ",to_lit(request.url)));
      
   if(eq(request.url,"/"))
   {
    const mem guid=get_guid();
    http_resp response;
   
    response.content_type="text/html; charset=utf-8";
    response.content=remote_bin();
    response.content=replace(response.content,"GUID",guid);
    response.content=compact(response.content);
    
    send(f,response);
   }
   else
    send_404(f);
  }
 }
}
