bool parse(http_req& x,const mem& y)
{
 clear(x);
 
 txt t1=to_txt(y);
 
 if(is_empty(t1))
  return false;
  
 const mem m1=shift(t1);
 txt t2=split(m1,' ');
 
 if(count(t2)!=3)
  return false;
  
 x.method=shift(t2);
 x.method=to_lower(x.method); 
 x.url=shift(t2);
 x.version=shift(t2);
 x.version=to_lower(x.version);
 
 while(true)
 {
  const mem m=shift(t1);
  
  if(is_empty(m))
   break;
   
  txt t3=split(m,':');
  
  const mem k=shift(t3);
  const mem v1=glue(t3,':');
  const mem v2=trim(v1);
  
  set(x.headers,k,v2);    
 }
 
 x.content=join(t1);
 
 return true;
}
