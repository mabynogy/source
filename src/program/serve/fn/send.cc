void send(const fd& x,const http_resp& y)
{
 seq t;
 
 const int l=get_length(y.content);
 
 push(t,"HTTP/1.1 200 OK");
 push(t,concat("Content-Type:",y.content_type));
 push(t,concat("Content-Length:",to_mem(l)));
 
 if(is_full(y.etag))
  push(t,concat("ETag:",y.etag));
 
 push(t,"");
 push(t,y.content);
 
 const mem m=glue(t);
 
 send(x,m);
}
