mem mangle(const mem& x)
{
 const mem m1=translit(x);
 const mem m2=to_lower(m1);
 const mem m3=simplify(m2);
 mem m4;
 
 const int l3=get_length(m3);
 
 for(int i=0;i<l3;i++)
 {
  const char c=at(m3,i);
  
  if(c=='_')
   push(m4,' ');
  else if(is_alnum(c))
   push(m4,c);
  else
   push(m4,' ');
 }
 
 txt t1=split(m4,' ');
 txt t2;
 
 while(is_full(t1))
 {
  const mem m1=shift(t1);
  const mem m2=trim(m1);
  
  if(is_full(m2))
   push(t2,m2);
 }
 
 return glue(t2,'-');
}
