bool is_censored(const mem& x)
{
 const mem path=file_locate("room-media/case-download/config/filter.txt");
 const mem tx=translit(x);
 const mem lx=to_lower(tx);
 const txt wx=split(lx,' ');
 txt t=cfg_load(path);
 
 while(is_full(t))
 {
  const mem word=shift(t);
  const mem tword=translit(word);
  const mem lword=to_lower(tword);
  
  if(contain(wx,lword))
   return true;
 }
 
 return false;
}
