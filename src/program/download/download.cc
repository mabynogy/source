mem download(const mem& x,const int y)
{
 check(y>=0);
 
 const int order=y+1;
 const mem sorder=to_mem(order);
 const mem config=path_real("../config");
 mem archive;
 
 if(debug()) 
  archive=path_concat(config,"yt-history-debug.txt");
 else
  archive=path_concat(config,"yt-history.txt");

 mem url;
  
 if(match_l(x,"https://"))
  url=x;
 else
  url=concat("ytsearch:",x);
   
 txt parts;
 
 push(parts,"yt-dlp");
 push(parts,"--playlist-start");
 push(parts,sorder);
 push(parts,"--playlist-end");
 push(parts,sorder);
 push(parts,"--download-archive");
 push(parts,archive);
 push(parts,"-o");
 push(parts,"\"%(channel)s-%(upload_date>%Y-%m-%d)s-%(title)s.%(ext)s\"");
 push(parts,"--no-mtime");
 push(parts,"--quiet");
 push(parts,"--progress");
 push(parts,quote(url));
 
 const mem command=join(parts,' ');
 
 os_system(command);
 
 txt paths=dir_list("."); 
 
 if(is_empty(paths))
  return {};
  
 mem r=shift(paths);
 
 while(is_full(paths))
 {
  const mem m=shift(paths);  
  
  const int tr=file_mtime(r);
  const int tm=file_mtime(m);
  
  if(tm>tr)
   r=m;
 }
 
 return r;
}
