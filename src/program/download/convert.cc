mem convert(const mem& x)
{
 check(is_file(x));
 
 const mem ext=path_ext(x);
 
 if(eq(ext,"mp4")) 
  return x;
  
 const mem dir=path_dir(x);
 const mem name=path_name(x);
 const mem file=concat(name,".mp4");
 const mem r=path_concat(dir,file);
 
 mem command;
 
 if(debug())
  command=concat("ffmpeg -hide_banner -loglevel warning -stats -i ",x," -preset ultrafast ",r);
 else
  command=concat("ffmpeg -hide_banner -loglevel warning -stats -i ",x," ",r);
 
 os_system(command);
 
 check(is_file(r));
 
 fs_remove(x);
 
 return r;
}
