void clean()
{
 txt t=dir_list(".");
 
 while(is_full(t))
 {
  const mem path=shift(t);
  const mem ext=path_ext(path);
  
  if(!is_file(path))
   continue;
   
  if(eq(ext,"part"))
  {
   fs_remove(path);
      
   continue;
  }
  
  const long n=file_size(path);
  
  if(n==0)
  {
   fs_remove(path);
      
   continue;
  }
 }
}
