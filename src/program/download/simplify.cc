mem simplify(const mem& x)
{
 const mem path=file_locate("room-media/case-download/config/stop.txt");
 const txt stops=cfg_load(path);
 seq s1=delimit(x);
 seq s2;
 
 while(is_full(s1))
 {
  const mem word=shift(s1);
  
  if(!contain(stops,word))
   push(s2,word);
 }
 
 return implode(s2); 
}
