bool scrap(const txt& x,const int y)
{
 check(y>=0);
 
 txt t=x;
 
 while(is_full(t))
 {
  if(bump())
   return false;
   
  const long free=disk_free();
  const int order=y+1;
  const mem bfree=to_bsize(free);
  const mem depth=to_mem(order);
  const mem query=shift(t);
  
  mem channel=query;
  
  if(match_l(channel,"https://www.youtube.com/"))
  {
   channel=strip_l(channel,"https://www.youtube.com/");
   channel=strip_l(channel,"channel/");
   channel=strip_l(channel,"c/");
   channel=strip_l(channel,"user/");
   channel=strip_r(channel,"/videos");
  }
  else if(match_l(channel,"https://"))
   channel=strip_l(channel,"https://");
  
  title_r(concat(channel," / free=",bfree," / ","depth=",depth));
  clean();
  
  mem path=download(query,y);
  
  if(is_empty(path))
   continue;
  
  const mem name=path_name(path);
  
  if(is_censored(name))
  {
   const mem m=concat("censored ",to_lit(name));
   
   title_r(m,"=");
   fs_remove(path);
   
   continue;  
  }
  
  const mem mangled=mangle(name);

  if(neq(mangled,name))
  {
   const mem dir=path_dir(path);
   const mem ext=path_ext(path);  
   const mem base=concat(mangled,".",ext);
   const mem path1=path_concat(dir,base);
   const mem path2=path_unique(path1);
   
   fs_rename(path,path2);   
   path=path2;
  }
  
  path=convert(path);
  
  const long size=file_size(path);
  const mem bsize=to_bsize(size);
  const mem name2=path_name(path);
  
  print(concat(bsize," ",name2));
 }
 
 return true;
}
