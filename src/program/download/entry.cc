void entry(const txt& x)
{ 
 const mem dir=dir_locate("room-media/case-download");
 const mem config=path_concat(dir,"config");
 const mem video=path_concat(dir,"video");
 const txt queries=cfg_load(path_concat(config,"follow.txt"));
 const mem previous=dir_current();

 dir_change(video);
 
 try
 {  
  crawl(queries);
 }
 catch(...)
 {
  dir_change(previous);
  
  throw;
 } 
 
 dir_change(previous);
}
