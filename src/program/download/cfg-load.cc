txt cfg_load(const mem& x)
{
 txt r;
 const mem m=load(x);
 txt t=to_txt(m);
 
 while(is_full(t))
 {
  const mem m1=shift(t);
  const mem m2=trim_r(m1);
  
  if(is_empty(m2))
   continue;
   
  const char c=front(m2);
  
  if(!is_space(c))
   continue;
  
  const mem m3=trim_l(m2);
  
  if(match_l(m3,"//"))
   continue;
  
  push(r,m3);
 }
 
 return r;
}
