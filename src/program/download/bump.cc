bool bump()
{
 if(term_hit())
  return true;

 const long gb=1024*1024*1024;
 const long free=50*gb;
 const long n=disk_free();
 
 if(n<free)
 {
  print(concat("free ",to_bsize(n)));
  
  return true;
 }
  
 return false;
}
