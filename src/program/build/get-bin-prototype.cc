mem get_bin_prototype(const mem& x)
{
 check(is_file(x));
 
 const mem m1=path_base(x);
 const mem m2=replace(m1,'-','_');
 const mem m3=replace(m2,'.','_');
 
 return concat("const char* ",m3,"()");
}
