txt parse_structs(const mem& x)
{
 txt r;
 const mem m=load(x);
 txt lines=to_txt(m);
 
 while(is_full(lines))
 {
  const mem line=shift(lines);
  
  if(match_l(line,"struct "))
  {
   mem s=strip_l(line,"struct");
   mem name=trim(s);
   
   push(r,name);
  }
 }
 
 return r;
}
