txt prg_generate_prototypes(const mem& x)
{
 txt r;
 txt t=prg_prototypes(x);
 
 while(is_full(t))
 {
  const mem m1=shift(t);
  const mem m2=concat(m1,";");
  
  push(r,m2);
 }
 
 return r;
}
