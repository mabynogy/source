txt prg_generate_structs(const mem& x)
{
 txt r;
 txt t=prg_structs(x);
 
 while(is_full(t))
 {
  const mem m1=shift(t);
  const mem m2=concat("struct ",m1);
  const mem m3=concat(m2,";");
  
  push(r,m3);
 }
 
 return r;
}
