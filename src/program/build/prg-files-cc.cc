txt prg_files_cc(const mem& x)
{
 txt r;
 txt t=prg_files(x);
 
 while(is_full(t))
 {
  const mem path=shift(t);
  const mem ext=path_ext(path);
  
  if(eq(ext,"cc"))
   push(r,path);  
 }
 
 return r;
}
