txt prg_prototypes(const mem& x)
{
 txt r;
 txt t=prg_files(x);
 
 while(is_full(t))
 {
  const mem path=shift(t);
  const mem ext=path_ext(path);
  
  if(eq(ext,"cc"))
  {
   const txt prototypes=parse_prototypes(path);
  
   append(r,prototypes);
  }
  else if(eq(ext,"bin"))
  {
   const mem m=get_bin_prototype(path);
   
   push(r,m);
  }
  else
   stop();
 }
 
 return r;
}
