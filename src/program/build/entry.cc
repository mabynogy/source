void entry(const txt& x)
{
 const txt prgs=get_prgs();
 mem prg="build";
 bool release=false;
 txt parameters;
 txt t=x;
 
 while(is_full(t))
 {
  const mem m1=shift(t);
  const mem m2=strip_l(m1,"--");
  
  if(eq(m1,"--release"))
   release=true;
  else if(contain(prgs,m2))
   prg=m2;
  else
   push(parameters,m1);
 }
 
 flower("-");
 build(prg,release);
}
