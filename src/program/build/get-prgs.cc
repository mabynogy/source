txt get_prgs()
{
 txt r;
 txt t=dir_list("src/program");
 
 while(is_full(t))
 {
  const mem m=shift(t);
  const mem name=path_name(m);
  
  push(r,name);
 }
 
 return r;
}
