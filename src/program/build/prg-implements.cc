txt prg_implements(const mem& x)
{
 txt r;
 txt t=prg_files_cc(x);
 
 while(is_full(t))
 {
  const mem path=shift(t);
  const txt implements=parse_implements(path);
  
  append(r,implements);
 }
 
 return r;
}
