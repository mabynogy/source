void build(const mem& x,const bool release)
{
 const double begin=get_now(); 

 txt out;
 const txt macros=prg_generate_macros(x);
 const txt structs=prg_generate_structs(x);
 const txt prototypes=prg_generate_prototypes(x);
 const txt implements=prg_generate_implements(x);
 const txt binaries=prg_generate_binaries(x);

 append(out,macros);
 append(out,structs);
 append(out,prototypes);
 append(out,implements);
 append(out,binaries);

 //dump(out);
  
 const mem m1=to_mem(out);
 const mem m2=trim_r(m1);
 
 const mem cc="out/out.cc";
 
 save(cc,m2); 
 
 const int file_count=count(dir_find("src"));
 const int struct_count=count(structs);
 const int fn_count=count(prototypes);
 const int sloc=count(implements);
 
 print(concat("program ",x));
 print(concat("file ",to_mem(file_count)));
 print(concat("struct ",to_mem(struct_count)));
 print(concat("fn ",to_mem(fn_count)));
 print(concat("sloc ",to_mem(sloc)));

 const double generate=get_now()-begin; 
 
 const mem bin_gcc=concat(x,"-gcc");
 const mem bin_gcc_g=concat(x,"-gcc-g");
 const mem bin_gcc_o3=concat(x,"-gcc-o3");

 const mem bin_clang=concat(x,"-clang");
 const mem bin_clang_g=concat(x,"-clang-g");
 const mem bin_clang_o3=concat(x,"-clang-o3");

 const mem out_gcc=path_concat("out",bin_gcc);
 const mem out_gcc_g=path_concat("out",bin_gcc_g);
 const mem out_gcc_o3=path_concat("out",bin_gcc_o3);

 const mem out_clang=path_concat("out",bin_clang);
 const mem out_clang_g=path_concat("out",bin_clang_g);
 const mem out_clang_o3=path_concat("out",bin_clang_o3);

 os_system(concat("g++ -g -fmax-errors=5 -pthread ",cc," -o ",out_gcc_g));
 os_system(concat("clang++ -g -ferror-limit=5 -pthread ",cc," -o ",out_clang_g)); 
 
 if(release)
 {
  os_system(concat("g++ -fmax-errors=5 -pthread ",cc," -o ",out_gcc));
  os_system(concat("g++ -O3 -fmax-errors=5 -pthread ",cc," -o ",out_gcc_o3));

  os_system(concat("clang++ -ferror-limit=5 -pthread ",cc," -o ",out_clang));
  os_system(concat("clang++ -O3 -ferror-limit=5 -pthread ",cc," -o ",out_clang_o3));
 }

 const double compile=get_now()-generate;
 const double profile=get_now()-begin;
 
 print(concat("generate ",to_mem(generate,2),"s"));
 print(concat("compile ",to_mem(compile,2),"s"));
 print(concat("profile ",to_mem(profile,2),"s"));
}
