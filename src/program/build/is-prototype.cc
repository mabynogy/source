bool is_prototype(const mem& x)
{
 if(is_empty(x))
  return false;

 if(is_text(x))
  return false;
 
 const char c1=front(x);
  
 if(!is_alpha(c1))
  return false;

 const char c2=back(x);

 if(c2!=')')
  return false;
  
 return true;
}
