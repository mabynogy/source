txt prg_macros(const mem& x)
{
 txt r;
 txt t=prg_files_cc(x);
 
 while(is_full(t))
 {
  const mem path=shift(t);
  const txt macros=parse_macros(path);
  
  append(r,macros);
 }
 
 return r;
}
