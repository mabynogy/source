txt parse_prototypes(const mem& x)
{
 txt r;
 const mem m=load(x);
 txt lines=to_txt(m);
 
 while(is_full(lines))
 {
  const mem line=shift(lines);
  
  if(is_prototype(line))
   push(r,line);
 }
 
 return r;
}
