bool is_macro(const mem& x)
{
 if(is_empty(x))
  return false;

 if(is_text(x))
  return false;
  
 if(match_l(x,"#"))
  return true;
 
 return false;
}
