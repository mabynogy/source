txt prg_files(const mem& x)
{
 txt r;
 txt t;
 const mem program=path_concat("src/program",x);
 
 append(t,dir_find("src/common"));
 append(t,dir_find(program));
 
 while(is_full(t))
 {
  const mem path=shift(t);
  const mem ext=path_ext(path);
  
  if(is_dir(path))
   continue;
   
  if(eq(ext,"cc"))
   push(r,path);  
  else if(eq(ext,"bin"))
   push(r,path);  
 }
 
 return r;
}
