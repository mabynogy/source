txt parse_implements(const mem& x)
{
 txt r;
 const mem m=load(x);
 txt lines=to_txt(m);
 
 while(is_full(lines))
 {
  const mem line=shift(lines);
  
  if(is_blank(line))
   continue;

  if(is_macro(line))
   continue;
   
  if(is_prototype(line))
  {
   txt prototypes;
   
   push(prototypes,line);
   
   while(is_full(lines))
   {
    const mem line=front(lines);
    
    if(is_prototype(line))
    {
     shift(lines);
     push(prototypes,line);
    }
    else
     break;
   }
   
   txt body;
   
   const mem m1=shift(lines);
   const mem m2=trim_r(m1);
   
   check(eq(m2,"{"));
   
   push(body,m2);
   
   while(is_full(lines))
   {
    const mem m1=front(lines);
    const mem m2=trim_r(m1);

    if(is_blank(m2))
    {
     shift(lines);
     
     continue;
    }
    
    if(eq(m2,"}"))
     break;
     
    shift(lines);
    push(body,m2);
   }
   
   const mem m3=shift(lines);
   const mem m4=trim_r(m3);

   check(eq(m4,"}"));

   push(body,m4);
   
   while(is_full(prototypes))
   {
    const mem prototype=shift(prototypes);
    
    push(r,prototype);
    append(r,body);
   }
   
   continue;
  }
      
  push(r,line);
 }
 
 return r;
}
