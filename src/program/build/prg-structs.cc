txt prg_structs(const mem& x)
{
 txt r;
 txt t=prg_files_cc(x);
 
 while(is_full(t))
 {
  const mem path=shift(t);
  const txt structs=parse_structs(path);
  
  append(r,structs);
 }
 
 return r;
}
