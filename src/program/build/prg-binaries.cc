txt prg_binaries(const mem& x)
{
 txt r;
 txt t=prg_files(x);
 
 while(is_full(t))
 {
  const mem path=shift(t);
  const mem ext=path_ext(path);
  
  if(eq(ext,"bin"))
  {
   const mem prototype=get_bin_prototype(path);
   const mem m1=load(path);
   const mem m2=to_lit(m1);
   
   push(r,prototype);
   push(r,"{");
   push(r,concat(" static thread_local const char* p=",m2,";"));
   push(r," return p;");
   push(r,"}");
  }
 }
 
 return r;
}
