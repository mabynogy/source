void entry(const txt& x)
{ 
 const mem dir=dir_locate("room-media/case-music");
 const txt paths=dir_list(dir);
 txt t1=paths;
 
 while(is_full(t1))
 {
  const mem path=shift(t1);
  const mem dir=path_dir(path);
  const mem name=path_name(path);
  const mem ext=path_ext(path);
  
  if(!is_file(path))
   continue;
  
  if(eq(ext,"opus"))
   continue;
   
  const mem base=concat(name,".opus");
  const mem opus=path_concat(dir,base);
   
  print(base);
  
  if(!is_file(opus))
   os_system(concat("ffmpeg -hide_banner -loglevel warning -stats -i ",path," ",opus));  
   
  fs_remove(path);  
 }
}
