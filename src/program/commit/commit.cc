void commit(const mem& x,const map& y)
{
 const mem work=path_real(x);
 const mem repo=path_base(work); 
 const mem date=get_date();
 const mem name=get(y,"name");
 const mem user=get(y,"user");
 const mem mail=get(y,"mail");
 const mem website=get(y,"website");
 const mem pat=get(y,"pat");
 const mem git=concat("https://",user,":",pat,"@",website,"/",user,"/",repo,".git");
 const mem url=concat("https://",website,"/",user,"/",repo);
 
 const mem previous=dir_current();

 dir_change(work);
 
 try
 {
  os_system(concat("git config --global user.name \"",name,"\""));
  os_system(concat("git config --global user.email \"",mail,"\""));
  os_system("git init");
  os_system("git switch -c main");
  os_system("git add .");
  os_system(concat("git commit -m \"",date,"\""));
  os_system(concat("git push --force ",git," main"));
 }
 catch(...)
 {
  dir_change(previous);
  
  throw;
 }

 dir_change(previous); 
 
 os_system(concat("xdg-open ",url));
}
