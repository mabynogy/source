void entry(const txt& x)
{
 txt t=x;

 const mem binary=shift(t);
 const mem dir=shift(t);
 const mem name=shift(t);
 
 check(is_dir(dir));
 check(is_empty(t));
 
 const mem base=concat(name,"-pat.txt");
 const mem path=file_locate(base);
 const mem content=load(path);
 
 map config;
 
 deserialize(config,content); 
 commit(dir,config);
}
