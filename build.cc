//g++ build.cc -o build
#include "ctype.h"
#include "errno.h"
#include "fcntl.h"
#include "libgen.h"
#include "limits.h"
#include "pthread.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "termios.h"
#include "time.h"
#include "unistd.h"
#include "arpa/inet.h"
#include "sys/socket.h"
#include "sys/stat.h"
#include "sys/vfs.h"
struct ptr;
struct mem;
struct buf;
struct txt;
struct seq;
struct map;
struct grid;
struct tbl;
struct fd;
struct pfile;
struct pthread;
struct timer;
struct stat fs_stat(const mem& x);
bool accept(fd& x,fd& y);
void allocate(mem& x,const int y);
void append(mem& x,const mem& y);
void append(buf& x,const char* y);
void append(buf& x,const mem& y);
void append(buf& x,const buf& y);
void append(txt& x,const txt& y);
void append(seq& x,const seq& y);
void assign(mem& x,const char* y);
void assign(mem& x,const mem& y);
void assign(mem& x,const buf& y);
void assign(buf& x,const char* y);
void assign(buf& x,const buf& y);
void assign(txt& x,const txt& y);
void assign(seq& x,const seq& y);
void assign(map& x,const map& y);
char at(const mem& x,const int y);
char at(const buf& x,const int y);
mem at(const txt& x,const int y);
mem at(const seq& x,const int y);
char back(const mem& x);
char back(const mem& x,const int y);
void back(mem& x,const int y,const char z);
char back(const buf& x);
char back(const buf& x,const int y);
void back(buf& x,const int y,const char z);
mem back(const txt& x);
mem back(const seq& x);
void check(const bool x);
void clear(ptr& x);
void clear(mem& x);
void clear(buf& x);
void clear(txt& x);
void clear(seq& x);
void clear(fd& x);
void clear(pfile& x);
void clear(pthread& x);
void clear(map& x);
void clear(timer& x);
void clear(grid& x);
void clear(tbl& x);
mem compact(const mem& x);
mem concat(const mem& x,const mem& y);
mem concat(const mem& x,const mem& y,const mem& z);
mem concat(const mem& x,const mem& y,const mem& z,const mem& a);
mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b);
mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b,const mem& c);
mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b,const mem& c,const mem& d);
mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b,const mem& c,const mem& d,const mem& e);
mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b,const mem& c,const mem& d,const mem& e,const mem& f);
mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b,const mem& c,const mem& d,const mem& e,const mem& f,const mem& g);
mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b,const mem& c,const mem& d,const mem& e,const mem& f,const mem& g,const mem& h);
bool contain(const mem& x,const char y);
bool contain(const mem& x,const mem& y);
bool contain(const txt& x,const mem& y);
bool contain(const seq& x,const mem& y);
void copy(ptr& x,const int y,const int z,const int a);
void copy(mem& x,const int y,const int z,const int a);
void copy(ptr& x,const int y,const ptr& z,const int a,const int b);
void copy(mem& x,const int y,const mem& z,const int a,const int b);
void copy(buf& x,const int y,const mem& z,const int a,const int b);
void copy(mem& x,const int y,const buf& z,const int a,const int b);
void copy(buf& x,const int y,const buf& z,const int a,const int b);
int count(const mem& x,const char y);
int count(const buf& x,const char y);
int count(const txt& x);
int count(const seq& x);
int count(const map& x);
void create(pthread& x,void (*y)(const map&),const map& z);
void* on_create(void* x);
mem decode(const mem& x);
seq delimit(const mem& x);
void deserialize(txt& x,const mem& y);
void deserialize(map& x,const mem& y);
void destruct(mem& x);
void destruct(fd& x);
void destruct(pfile& x);
void destruct(pthread& x);
ptr detach(ptr& x);
ptr detach(mem& x);
mem detach(buf& x);
void dim(buf& x,const int y);
void dir_change(const mem& x);
void dir_clean(const mem& x);
void dir_copy(const mem& x,const mem& y);
mem dir_current();
txt dir_find(const mem& x);
txt dir_list(const mem& x);
mem dir_locate(const mem& x);
void dir_make(const mem& x);
void dir_remove(const mem& x);
void dir_reset(const mem& x);
long disk_free();
long disk_size();
long disk_usage();
void drop_l(mem& x);
void drop_l(mem& x,const int y);
void drop_l(buf& x);
void drop_l(buf& x,const int y);
void drop_l(txt& x);
void drop_l(seq& x);
void drop_l(txt& x,const int y);
void drop_l(seq& x,const int y);
void drop_r(mem& x);
void drop_r(mem& x,const int y);
void drop_r(buf& x);
void drop_r(buf& x,const int y);
void drop_r(txt& x);
void drop_r(seq& x);
void drop_r(txt& x,const int y);
void drop_r(seq& x,const int y);
void dump(const bool x);
void dump(const char x);
void dump(const int x);
void dump(const unsigned x);
void dump(const long x);
void dump(const unsigned long x);
void dump(const double x);
void dump(const void* x);
void dump(const char* x);
void dump(const mem& x);
void dump(const buf& x);
void dump(const txt& x);
void dump(const seq& x);
void dump(const map& x);
void echo(const bool x);
void echo(const char x);
void echo(const int x);
void echo(const unsigned x);
void echo(const long x);
void echo(const unsigned long x);
void echo(const double x);
void echo(const void* x);
void echo(const char* x);
void echo(const mem& x);
void echo(const buf& x);
mem encode(const char x);
mem encode(const mem& x);
bool eq(const mem& x,const mem& y);
bool eq(const buf& x,const buf& y);
mem escape(const char x);
mem escape(const mem& x);
void file_copy(const mem& x,const mem& y);
bool file_eq(const mem& x,const mem& y);
mem file_locate(const mem& x);
int file_mtime(const mem& x);
long file_size(const mem& x);
int find(const mem& x,const char y);
void flower();
void flower(const mem& x);
char front(const mem& x);
void front(mem& x,const char y);
char front(const buf& x);
void front(buf& x,const char y);
mem front(const txt& x);
mem front(const seq& x);
void fs_remove(const mem& x);
void fs_rename(const mem& x,const mem& y);
struct stat fs_stat(const mem& x);
char* get_address(ptr& x);
char* get_address(mem& x);
const char* get_address(const mem& x);
char* get_address(ptr& x,const int y);
const char* get_address(const ptr& x,const int y);
char* get_address(mem& x,const int y);
const char* get_address(const mem& x,const int y);
int get_capacity(const buf& x);
mem get(const map& x,const mem& y);
mem get_date();
mem get_date(const int x);
mem get_domain(const mem& x);
mem get_guid();
mem get_guid(const int x);
mem get_ip(const fd& x);
int get_length(const ptr& x);
int get_length(const mem& x);
int get_length(const buf& x);
double get_now();
int get_offset(const buf& x,const int y);
int get_overhead(const buf& x);
int get_random(const int x);
double get_time();
mem glue(const txt& x);
mem glue(const seq& x);
mem glue(const txt& x,const char y);
mem glue(const seq& x,const char y);
mem glue(const txt& x,const mem& y);
mem glue(const seq& x,const mem& y);
bool has(map& x,const mem& y);
mem head(const mem& x,const int y);
mem head(const buf& x,const int y);
bool hit(const timer& x);
mem implode(const txt& x);
mem implode(const seq& x);
bool is_alnum(const char x);
bool is_alnum(const mem& x);
bool is_alpha(const char x);
bool is_alpha(const mem& x);
bool is_blank(const char x);
bool is_blank(const mem& x);
bool is_circular(const buf& x);
bool is_digit(const char x);
bool is_digit(const mem& x);
bool is_dir(const mem& x);
bool is_empty(const ptr& x);
bool is_empty(const buf& x);
bool is_empty(const mem& x);
bool is_empty(const txt& x);
bool is_empty(const seq& x);
bool is_empty(const fd& x);
bool is_empty(const pfile& x);
bool is_empty(const pthread& x);
bool is_empty(const map& x);
bool is_empty(const grid& x);
bool is_empty(const tbl& x);
bool is_file(const mem& x);
bool is_filled(const buf& x);
bool is_fs(const mem& x);
bool is_full(const ptr& x);
bool is_full(const mem& x);
bool is_full(const buf& x);
bool is_full(const fd& x);
bool is_full(const pfile& x);
bool is_full(const pthread& x);
bool is_full(const seq& x);
bool is_full(const txt& x);
bool is_full(const map& x);
bool is_full(const grid& x);
bool is_full(const tbl& x);
bool is_line(const mem& x);
bool is_nl(const char x);
bool is_nl(const mem& x);
bool is_normal(const buf& x);
bool is_punct(const char x);
bool is_space(const char x);
bool is_space(const mem& x);
bool is_text(const mem& x);
mem join(const txt& x);
mem join(const seq& x);
mem join(const txt& x,const char y);
mem join(const seq& x,const char y);
mem join(const txt& x,const mem& y);
mem join(const seq& x,const mem& y);
void* lift(const mem& x);
void* lift(const txt& x);
void* lift(const map& x);
void listen(fd& x,const int y);
mem load(const mem& x);
int main(const int x,const char** y);
bool match_l(const mem& x,const char y);
bool match_l(const buf& x,const char y);
bool match_l(const mem& x,const mem& y);
bool match_l(const buf& x,const mem& y);
bool match_r(const mem& x,const char y);
bool match_r(const buf& x,const char y);
bool match_r(const mem& x,const mem& y);
bool match_r(const buf& x,const mem& y);
void move(ptr& x,ptr& y);
void move(mem& x,mem& y);
void move(buf& x,buf& y);
void move(txt& x,txt& y);
void move(seq& x,seq& y);
void move(map& x,map& y);
void move(timer& x,timer& y);
void move(grid& x,grid& y);
void move(tbl& x,tbl& y);
bool neq(const mem& x,const mem& y);
bool neq(const buf& x,const buf& y);
void normalize(buf& x);
void open(fd& x,const mem& y);
void open(fd& x,const mem& y,const int z);
void open(pfile& x,const mem& y);
mem os_execute(const mem& x);
int os_system(const mem& x);
int os_system(const mem& x,const mem& y);
mem pad_l(const int x,const int y);
mem pad_l(const mem& x,const int y,const char z);
mem pad_r(const int x,const int y);
mem pad_r(const mem& x,const int y,const char z);
mem path_base(const mem& x);
mem path_concat(const mem& x,const mem& y);
mem path_dir(const mem& x);
mem path_ext(const mem& x);
mem path_name(const mem& x);
mem path_real(const mem& x);
mem path_resolve(const mem& x);
mem path_tmp(const mem& x);
mem path_unique(const mem& x);
char pop(mem& x);
mem pop(mem& x,const int y);
char pop(buf& x);
mem pop(buf& x,const int y);
mem pop(txt& x);
mem pop(seq& x);
void prepend(mem& x,const mem& y);
void prepend(buf& x,const char* y);
void prepend(buf& x,const mem& y);
void prepend(buf& x,const buf& y);
void prepend(txt& x,const txt& y);
void prepend(seq& x,const seq& y);
void print();
void print(const bool x);
void print(const char x);
void print(const int x);
void print(const unsigned x);
void print(const long x);
void print(const unsigned long x);
void print(const double x);
void print(const void* x);
void print(const mem& x);
void print(const buf& x);
void print(const char* x);
void print(const txt& x);
void print(const seq& x);
void print(const map& x);
void push(mem& x,const char y);
void push(buf& x,const char y);
void push(txt& x,const mem& y);
void push(seq& x,const mem& y);
void put(map& x,const mem& y,const mem& z);
mem quote(const mem& x);
mem read(const fd& x);
mem read(const pfile& x);
mem read(const fd& x,const int y);
mem read(const pfile& x,const int y);
mem receive(const fd& x);
void remove(txt& x,const int y);
void remove(txt& x,const int y,const int z);
void remove(seq& x,const int y);
void remove(seq& x,const int y,const int z);
mem repeat(const mem& x,const int y);
mem replace(const mem& x,const char y,const char z);
mem replace(const mem& x,const mem& y,const mem& z);
void report();
void reserve(buf& x,const int y);
void reset(buf& x);
void save(const mem& x,const mem& y);
void send(const fd& x,const mem& y);
void serialize(mem& x,const txt& y);
void serialize(mem& x,const map& y);
void set(mem& x,const int y,const char z);
void set(buf& x,const int y,const char z);
void set(map& x,const mem& y,const mem& z);
void set_non_block(const fd& x);
char shift(mem& x);
mem shift(mem& x,const int y);
char shift(buf& x);
mem shift(buf& x,const int y);
mem shift(txt& x);
mem shift(seq& x);
void shuffle(txt& x);
void shuffle(seq& x);
mem single_quote(const mem& x);
mem slice(const mem& x,const int y);
mem slice(const buf& x,const int y);
txt slice(const txt& x,const int y);
seq slice(const seq& x,const int y);
mem slice(const mem& x,const int y,const int z);
mem slice(const buf& x,const int y,const int z);
txt slice(const txt& x,const int y,const int z);
seq slice(const seq& x,const int y,const int z);
mem slice_l(const mem& x,const int y);
mem slice_l(const buf& x,const int y);
txt slice_l(const txt& x,const int y);
seq slice_l(const seq& x,const int y);
mem slice_r(const mem& x,const int y);
mem slice_r(const buf& x,const int y);
txt slice_r(const txt& x,const int y);
seq slice_r(const seq& x,const int y);
void sort(txt& x);
txt split(const mem& x,const char y);
txt split(const mem& x,const mem& y);
void stop();
mem strip_l(const mem& x,const char y);
mem strip_l(const buf& x,const char y);
mem strip_l(const mem& x,const mem& y);
mem strip_l(const buf& x,const mem& y);
mem strip_r(const mem& x,const char y);
mem strip_r(const buf& x,const char y);
mem strip_r(const mem& x,const mem& y);
mem strip_r(const buf& x,const mem& y);
void swap(bool& x,bool& y);
void swap(char& x,char& y);
void swap(int& x,int& y);
void swap(unsigned& x,unsigned& y);
void swap(long& x,long& y);
void swap(unsigned long& x,unsigned long& y);
void swap(double& x,double& y);
void swap(char*& x,char*& y);
void swap(FILE*& x,FILE*& y);
void swap(ptr& x,ptr& y);
void swap(mem& x,mem& y);
void swap(buf& x,buf& y);
void swap(txt& x,txt& y);
void swap(seq& x,seq& y);
void swap(fd& x,fd& y);
void swap(pfile& x,pfile& y);
void swap(map& x,map& y);
void swap(grid& x,grid& y);
void swap(tbl& x,tbl& y);
void swap(timer& x,timer& y);
mem tail(const mem& x,const int y);
mem tail(const buf& x,const int y);
bool term_hit();
mem term_read();
int term_width();
timer timeout(const double x);
void title_l(const mem& x);
void title_l(const mem& x,const mem& y);
void title_r(const mem& x);
void title_r(const mem& x,const mem& y);
mem to_bsize(const long x);
buf to_buf(const mem& x);
mem to_c(const mem& x);
mem to_hexa(const int x);
int to_int(const mem& x);
mem to_lit(const char x);
mem to_lit(const mem& x);
mem to_lit(const buf& x);
long to_long(const mem& x);
char to_lower(const char x);
mem to_lower(const mem& x);
mem to_mem(const bool x);
mem to_mem(const char x);
mem to_mem(const int x);
mem to_mem(const unsigned x);
mem to_mem(const long x);
mem to_mem(const unsigned long x);
mem to_mem(const double x);
mem to_mem(const double x,const int y);
mem to_mem(const void* x);
mem to_mem(const char* x);
mem to_mem(const buf& x);
mem to_mem(const txt& x);
mem to_mem(const seq& x);
mem to_mem(const map& x);
seq to_seq(const txt& x);
txt to_txt(const mem& x);
char to_upper(const char x);
mem to_upper(const mem& x);
mem translit(const mem& x);
mem trim(const mem& x);
mem trim_l(const mem& x);
txt trim_l(const txt& x);
mem trim_r(const mem& x);
txt trim_r(const txt& x);
void unlift(mem& x,void* y);
void unlift(txt& x,void* y);
void unlift(map& x,void* y);
void unshift(mem& x,const char y);
void unshift(buf& x,const char y);
void unshift(txt& x,const mem& y);
void unshift(seq& x,const mem& y);
void unwrap(void* x,const int y,const mem& z);
void unwrap(bool& x,const mem& y);
void unwrap(char& x,const mem& y);
void unwrap(int& x,const mem& y);
void unwrap(unsigned& x,const mem& y);
void unwrap(long& x,const mem& y);
void unwrap(unsigned long& x,const mem& y);
void unwrap(double& x,const mem& y);
void unwrap(void*& x,const mem& y);
void unwrap(txt& x,const mem& y);
void unwrap(map& x,const mem& y);
void wait(const double x);
mem wrap(const void* x,const int y);
mem wrap(const bool y);
mem wrap(const char y);
mem wrap(const int y);
mem wrap(const unsigned y);
mem wrap(const long y);
mem wrap(const unsigned long y);
mem wrap(const double y);
mem wrap(const void* y);
mem wrap(const txt& x);
mem wrap(const map& x);
void write(const fd& x,const mem& y);
void build(const mem& x,const bool release);
void entry(const txt& x);
mem get_bin_prototype(const mem& x);
txt get_prgs();
bool is_macro(const mem& x);
bool is_prototype(const mem& x);
txt parse_implements(const mem& x);
txt parse_macros(const mem& x);
txt parse_prototypes(const mem& x);
txt parse_structs(const mem& x);
txt prg_binaries(const mem& x);
txt prg_files(const mem& x);
txt prg_files_cc(const mem& x);
txt prg_generate_binaries(const mem& x);
txt prg_generate_implements(const mem& x);
txt prg_generate_macros(const mem& x);
txt prg_generate_prototypes(const mem& x);
txt prg_generate_structs(const mem& x);
txt prg_implements(const mem& x);
txt prg_macros(const mem& x);
txt prg_prototypes(const mem& x);
txt prg_structs(const mem& x);
struct ptr
{
 char* pointer=0;
 int length=0;
};
struct mem
{
 ptr pointer;
 mem()
 {
 }
 mem(const char* x)
 {
  assign(*this,x);
 }
 mem(const mem& x)
 {
  assign(*this,x);
 }
 mem(const buf& x)
 {
  assign(*this,x);
 }
 mem(mem&& x)
 {
  swap(*this,x);
 }
 ~mem()
 {
  destruct(*this);
 }
 void operator =(const char* x)
 {
  assign(*this,x);
 } 
 void operator =(const mem& x)
 {
  assign(*this,x);
 }
 void operator =(const buf& x)
 {
  assign(*this,x);
 }
 void operator =(mem&& x)
 {
  swap(*this,x);
 } 
};
struct buf
{
 int index=0;
 int length=0;
 mem memory;
 buf()
 {
 }
 buf(const char* x)
 {
  assign(*this,x);
 }
 buf(const buf& x)
 {
  assign(*this,x);
 }
 buf(buf&& x)
 {
  swap(*this,x);
 }
 void operator =(const char* x)
 {
  assign(*this,x);
 } 
 void operator =(const buf& x)
 {
  assign(*this,x);
 }
 void operator =(buf&& x)
 {
  swap(*this,x);
 } 
};
struct txt
{ 
 buf buffer;
 txt()
 {
 }
 txt(const txt& x)
 {
  assign(*this,x);
 }
 txt(txt&& x)
 {
  swap(*this,x);
 }
 void operator =(const txt& x)
 {
  assign(*this,x);
 }
 void operator =(txt&& x)
 {
  swap(*this,x);
 }
};
struct seq
{ 
 txt text;
 seq()
 {
 }
 seq(const seq& x)
 {
  assign(*this,x);
 }
 seq(seq&& x)
 {
  swap(*this,x);
 }
 void operator =(const seq& x)
 {
  assign(*this,x);
 }
 void operator =(seq&& x)
 {
  swap(*this,x);
 }
};
struct map
{
 seq keys;
 seq values;
 map()
 {
 }
 map(const map& x)
 {
  assign(*this,x);
 }
 map(map&& x)
 {
  assign(*this,x);
 }
 void operator =(const map& x)
 {
  assign(*this,x);
 }
 void operator =(map&& x)
 {
  swap(*this,x);
 }
};
struct grid
{
 int width=0;
 int height=0;
 seq cells;
};
struct tbl
{
 txt columns;
 grid cells;
};
struct fd
{
 int handle=-1;
 fd()
 {
 }
 fd(const fd& x)
 {
  stop();
 }
 fd(fd&& x)
 {
  swap(*this,x);
 }
 ~fd()
 {
  destruct(*this);
 }
 void operator =(const fd& x)
 {
  stop();
 }
 void operator =(fd&& x)
 {
  swap(*this,x);
 }
};
struct pfile
{
 FILE* pointer=0;
 pfile()
 {
 }
 pfile(const pfile& x)
 {
  stop();
 }
 pfile(pfile&& x)
 {
  stop();
 }
 ~pfile()
 {
  destruct(*this);
 }
 void operator =(const pfile& x)
 {
  stop();
 }
 void operator =(pfile&& x)
 {
  stop();
 }
};
struct pthread
{
 pthread_t pointer=0;
 pthread()
 {
 }
 pthread(const pthread& x)
 {
  stop();
 }
 pthread(pthread&& x)
 {
  stop();
 }
 ~pthread()
 {
  destruct(*this);
 }
 void operator =(const pthread& x)
 {
  stop();
 }
 void operator =(pthread&& x)
 {
  stop();
 }
};
struct timer
{
 double start=get_now();
 double duration=1;
};
bool accept(fd& x,fd& y)
{
 clear(y);
 y.handle=accept(x.handle,0,0);
 if(y.handle==-1)
 {
  check(errno==EAGAIN||errno==EWOULDBLOCK);
  return false;
 }
 set_non_block(y);
 return true;
}
void allocate(mem& x,const int y)
{
 check(y>=0);
 const int l=get_length(x);
 if(y==l)
  return;
 if(y==0)
 {
  clear(x);
  return;
 }
 if(is_empty(x))
 {
  x.pointer.pointer=(char*)malloc(y);
  check(x.pointer.pointer!=0);
 }
 else
 {
  char* pointer=get_address(x);
  char* p=(char*)realloc(pointer,y);
  check(p!=0);
  x.pointer.pointer=p;
 }
 x.pointer.length=y;
}
void append(mem& x,const mem& y)
{
 const int lx=get_length(x);
 const int ly=get_length(y);
 const int l=lx+ly;
 allocate(x,l);
 copy(x,lx,y,0,ly);
}
void append(buf& x,const char* y)
{
 check(y!=0);
 const mem m=y;
 append(x,m);
}
void append(buf& x,const mem& y)
{
 const int lx=get_length(x);
 const int ly=get_length(y);
 const int l=lx+ly;
 reserve(x,l);
 for(int i=0;i<ly;i++)
 {
  const char c=at(y,i);
  push(x,c);
 }
}
void append(buf& x,const buf& y)
{
 const int lx=get_length(x);
 const int ly=get_length(y);
 const int l=lx+ly;
 reserve(x,l);
 for(int i=0;i<ly;i++)
 {
  const char c=at(y,i);
  push(x,c);
 }
}
void append(txt& x,const txt& y)
{
 append(x.buffer,y.buffer);
}
void append(seq& x,const seq& y)
{
 append(x.text,y.text);
}
void assign(mem& x,const char* y)
{
 check(y!=0);
 const int l=strlen(y);
 if(l==0)
 {
  clear(x);
  return;
 }
 allocate(x,l);
 char* p=get_address(x);
 memcpy(p,y,l);
}
void assign(mem& x,const mem& y)
{
 const int l=get_length(y);
 allocate(x,l);
 copy(x,0,y,0,l);
}
void assign(mem& x,const buf& y)
{
 const int l=get_length(y);
 allocate(x,l);
 copy(x,0,y,0,l);
}
void assign(buf& x,const char* y)
{
 check(y!=0);
 const int l=strlen(y);
 reset(x);
 reserve(x,l);
 for(int i=0;i<l;i++)
 {
  const char c=y[i];
  push(x,c);
 }
}
void assign(buf& x,const buf& y)
{
 reset(x);
 append(x,y);
}
void assign(txt& x,const txt& y)
{
 assign(x.buffer,y.buffer);
}
void assign(seq& x,const seq& y)
{
 assign(x.text,y.text);
}
void assign(map& x,const map& y)
{
 assign(x.keys,y.keys);
 assign(x.values,y.values);
}
char at(const mem& x,const int y)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<l);
 const char* p=get_address(x,y);
 return *p;
}
char at(const buf& x,const int y)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<l);
 const int n=get_offset(x,y);
 return at(x.memory,n);
}
mem at(const txt& x,const int y)
{
 check(y>=0);
 check(y<count(x));
 txt t=x;
 drop_l(t,y);
 return shift(t);
}
mem at(const seq& x,const int y)
{
 const mem m=at(x.text,y);
 return decode(m);
}
char back(const mem& x)
{
 check(is_full(x));
 return back(x,0);
}
char back(const mem& x,const int y)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<l);
 const int n=l-y-1;
 return at(x,n);
}
void back(mem& x,const int y,const char z)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<l);
 const int n=l-y-1;
 set(x,n,z);
}
char back(const buf& x)
{
 check(is_full(x));
 return back(x,0);
}
char back(const buf& x,const int y)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<l);
 const int n=l-y-1;
 return at(x,n);
}
void back(buf& x,const int y,const char z)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<l);
 const int n=l-y-1;
 set(x,n,z);
}
mem back(const txt& x)
{
 check(is_full(x));
 buf b;
 const int l=get_length(x.buffer);
 const int lx=l-1;
 for(int i=0;i<lx;i++)
 {
  const char n=i+1;
  const char c=back(x.buffer,n);
  if(c=='\n')
   break;
  unshift(b,c);
 }
 return detach(b);
}
mem back(const seq& x)
{
 check(is_full(x));
 const mem m=back(x.text);
 return decode(m);
}
void check(const bool x)
{
 if(x)
  return;
 stop();
}
void clear(ptr& x)
{
 x.pointer=0;
 x.length=0;
}
void clear(mem& x)
{
 if(is_empty(x))
  return;
 free(x.pointer.pointer);
 clear(x.pointer);
}
void clear(buf& x)
{
 x.index=0;
 x.length=0;
 clear(x.memory);
}
void clear(txt& x)
{
 clear(x.buffer);
}
void clear(seq& x)
{
 clear(x.text);
}
void clear(fd& x)
{
 if(is_empty(x))
  return;
 const int n=close(x.handle);
 x.handle=-1;
 check(n==0);
}
void clear(pfile& x)
{
 if(is_empty(x))
  return;
 const int n=pclose(x.pointer);
 x.pointer=0;
 check(n==0);
}
void clear(pthread& x)
{
 if(is_empty(x))
  return;
 void* p=0;
 const int n=pthread_join(x.pointer,&p);
 x.pointer=0;
 check(n==0);
}
void clear(map& x)
{
 clear(x.keys);
 clear(x.values);
}
void clear(timer& x)
{
 x.start=get_now();
 x.duration=1;
}
void clear(grid& x)
{
 x.width=0;
 x.height=0;
 clear(x.cells);
}
void clear(tbl& x)
{
 clear(x.columns);
 clear(x.cells);
}
mem compact(const mem& x)
{
 txt t1=to_txt(x);
 txt t2;
 while(is_full(t1))
 {
  const mem m1=shift(t1);
  const mem m2=trim_r(m1);
  if(is_empty(m2))
   continue;
  push(t2,m2);
 }
 const mem m3=glue(t2);
 return trim_r(m3);
}
mem concat(const mem& x,const mem& y)
{
 mem r;
 append(r,x);
 append(r,y);
 return r;
}
mem concat(const mem& x,const mem& y,const mem& z)
{
 mem r;
 append(r,x);
 append(r,y);
 append(r,z);
 return r;
}
mem concat(const mem& x,const mem& y,const mem& z,const mem& a)
{
 mem r;
 append(r,x);
 append(r,y);
 append(r,z);
 append(r,a);
 return r;
}
mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b)
{
 mem r;
 append(r,x);
 append(r,y);
 append(r,z);
 append(r,a);
 append(r,b);
 return r;
}
mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b,const mem& c)
{
 mem r;
 append(r,x);
 append(r,y);
 append(r,z);
 append(r,a);
 append(r,b);
 append(r,c);
 return r;
}
mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b,const mem& c,const mem& d)
{
 mem r;
 append(r,x);
 append(r,y);
 append(r,z);
 append(r,a);
 append(r,b);
 append(r,c);
 append(r,d);
 return r;
}
mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b,const mem& c,const mem& d,const mem& e)
{
 mem r;
 append(r,x);
 append(r,y);
 append(r,z);
 append(r,a);
 append(r,b);
 append(r,c);
 append(r,d);
 append(r,e);
 return r;
}
mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b,const mem& c,const mem& d,const mem& e,const mem& f)
{
 mem r;
 append(r,x);
 append(r,y);
 append(r,z);
 append(r,a);
 append(r,b);
 append(r,c);
 append(r,d);
 append(r,e);
 append(r,f);
 return r;
}
mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b,const mem& c,const mem& d,const mem& e,const mem& f,const mem& g)
{
 mem r;
 append(r,x);
 append(r,y);
 append(r,z);
 append(r,a);
 append(r,b);
 append(r,c);
 append(r,d);
 append(r,e);
 append(r,f);
 append(r,g);
 return r;
}
mem concat(const mem& x,const mem& y,const mem& z,const mem& a,const mem& b,const mem& c,const mem& d,const mem& e,const mem& f,const mem& g,const mem& h)
{
 mem r;
 append(r,x);
 append(r,y);
 append(r,z);
 append(r,a);
 append(r,b);
 append(r,c);
 append(r,d);
 append(r,e);
 append(r,f);
 append(r,g);
 append(r,h);
 return r;
}
bool contain(const mem& x,const char y)
{
 if(is_empty(x))
  return false;
 const int l=get_length(x);
 const void* p=get_address(x);
 const void* pp=memchr(p,y,l);
 return pp!=0;
}
bool contain(const mem& x,const mem& y)
{
 if(is_empty(x))
  return false;
 if(is_empty(y))
  return false;
 const int lx=get_length(x);
 const int ly=get_length(y);
 const char* px=get_address(x);
 const char* py=get_address(y);
 void* p=memmem(px,lx,py,ly);
 return p!=0;
}
bool contain(const txt& x,const mem& y)
{
 txt t=x;
 while(is_full(t))
 {
  const mem m=shift(t);
  if(eq(m,y))
   return true;
 }
 return false;
}
bool contain(const seq& x,const mem& y)
{
 const mem m=encode(y);
 return contain(x.text,m);
}
void copy(ptr& x,const int y,const int z,const int a)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<=l);
 check(z>=0);
 check(z<=l);
 check(a>=0);
 check(a<=l);
 check(y+a<=l);
 check(z+a<=l);
 if(a==0)
  return;
 const int yz=y+z;
 const bool overlap=a>y&&a<yz;
 char* py=get_address(x,y);
 char* pz=get_address(x,z);
 if(overlap)
  memmove(py,pz,a);
 else
  memcpy(py,pz,a);
}
void copy(mem& x,const int y,const int z,const int a)
{
 copy(x.pointer,y,z,a);
}
void copy(ptr& x,const int y,const ptr& z,const int a,const int b)
{
 const int lx=get_length(x);
 const int lz=get_length(z);
 check(y>=0);
 check(y<=lx);
 check(a>=0);
 check(a<=lz);
 check(b>=0);
 check(b<=lz);
 check(y+b<=lx);
 check(a+b<=lz);
 if(b==0)
  return;
 char* py=get_address(x,y);
 const char* pa=get_address(z,a);
 memcpy(py,pa,b);
}
void copy(mem& x,const int y,const mem& z,const int a,const int b)
{
 copy(x.pointer,y,z.pointer,a,b);
}
void copy(buf& x,const int y,const mem& z,const int a,const int b)
{
 const int lx=get_length(x);
 const int lz=get_length(z);
 check(y>=0);
 check(y<=lx);
 check(a>=0);
 check(a<=lz);
 check(b>=0);
 check(b<=lz);
 check(y+b<=lx);
 check(a+b<=lz);
 if(b==0)
  return;
 if(is_circular(x))
 {
  for(int i=0;i<b;i++)
  {
   const int ny=y+i;
   const int na=a+i;
   const char c=at(z,na);
   set(x,ny,c);
  }
  return;
 }
 const int ny=x.index+y;
 copy(x.memory,ny,z,a,b);
}
void copy(mem& x,const int y,const buf& z,const int a,const int b)
{
 const int lx=get_length(x);
 const int lz=get_length(z);
 check(y>=0);
 check(y<=lx);
 check(a>=0);
 check(a<=lz);
 check(b>=0);
 check(b<=lz);
 check(y+b<=lx);
 check(a+b<=lz);
 if(b==0)
  return;
 if(is_circular(z))
 {
  for(int i=0;i<b;i++)
  {
   const int ny=y+i;
   const int na=a+i;
   const char c=at(z,na);
   set(x,ny,c);
  }
  return;
 }
 const int na=z.index+a;
 copy(x,y,z.memory,na,b);
}
void copy(buf& x,const int y,const buf& z,const int a,const int b)
{
 const int lx=get_length(x);
 const int lz=get_length(z);
 check(y>=0);
 check(y<=lx);
 check(a>=0);
 check(a<=lz);
 check(b>=0);
 check(b<=lz);
 check(y+b<=lx);
 check(a+b<=lz);
 if(b==0)
  return;
 if(is_circular(x)||is_circular(z))
 {
  for(int i=0;i<b;i++)
  {
   const int ny=y+i;
   const int na=a+i;
   const char c=at(z,na);
   set(x,ny,c);
  }
  return;
 }
 const int ny=x.index+y;
 const int na=z.index+a;
 copy(x.memory,ny,z.memory,na,b);
}
int count(const mem& x,const char y)
{
 int r=0;
 const int l=get_length(x);
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  if(c==y)
   r++;
 }
 return r;
}
int count(const buf& x,const char y)
{
 int r=0;
 const int l=get_length(x);
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  if(c==y)
   r++;
 }
 return r;
}
int count(const txt& x)
{
 return count(x.buffer,'\n');
}
int count(const seq& x)
{
 return count(x.text);
}
int count(const map& x)
{
 return count(x.keys);
}
void create(pthread& x,void (*y)(const map&),const map& z)
{
 check(y!=0);
 clear(x);
 map m;
 set(m,"routine",wrap((void*)y));
 set(m,"arguments",wrap(z));
 void* p=lift(m);
 const int n=pthread_create(&x.pointer,0,on_create,p);
 check(n==0);
}
void* on_create(void* x)
{
 check(x!=0);
 map m1;
 unlift(m1,x);
 const mem m2=get(m1,"routine");
 const mem m3=get(m1,"arguments");
 void* p=0;
 unwrap(p,m2);
 void (*routine)(const map&)=(void (*)(const map&))p;
 map arguments;
 unwrap(arguments,m3);
 routine(arguments);
 return 0;
}
mem decode(const mem& x)
{
 buf output;
 buf input=to_buf(x);
 const mem bn="\\n";
 const mem bs="\\\\";
 const int ln=get_length(bn);
 const int ls=get_length(bs);
 while(is_full(input))
 {
  if(match_l(input,bn))
  {
   shift(input,ln);
   push(output,'\n');
  }
  else if(match_l(input,bs))
  {
   shift(input,ls);
   push(output,'\\');
  }
  else
  {
   const char c=shift(input);
   push(output,c);
  }
 }
 return detach(output);
}
seq delimit(const mem& x)
{
 seq r;
 buf b=to_buf(x);
 while(is_full(b))
 {
  const char c=shift(b);
  if(is_empty(r))
  {
   mem m;
   push(m,c);
   push(r,m);
   continue;
  }
  const mem left=back(r);
  mem right;
  push(right,c);
  if(is_alnum(left)&&is_alnum(right))
  {
   const mem m=concat(left,right);
   drop_r(r);
   push(r,m);
  }
  else if(is_space(left)&&is_space(right))
  {
   const mem m=concat(left,right);
   drop_r(r);
   push(r,m);
  }
  else if(is_nl(left)&&is_nl(right))
  {
   const mem m=concat(left,right);
   drop_r(r);
   push(r,m);
  }
  else
   push(r,right);
 }
 return r;
}
void deserialize(txt& x,const mem& y)
{
 txt t=to_txt(y);
 move(x,t);
}
void deserialize(map& x,const mem& y)
{
 clear(x);
 txt t;
 deserialize(t,y);
 while(is_full(t))
 {
  const mem m1=shift(t);
  const mem m2=shift(t);
  const mem m3=decode(m1);
  const mem key=trim(m3);
  const mem value=decode(m2);
  put(x,key,value);
 }
}
void destruct(mem& x)
{
 try
 {
  clear(x);
 }
 catch(...)
 {
  report();
 }
}
void destruct(fd& x)
{
 try
 {
  clear(x);
 }
 catch(...)
 {
  report();
 }
}
void destruct(pfile& x)
{
 try
 {
  clear(x);
 }
 catch(...)
 {
  report();
 }
}
void destruct(pthread& x)
{
 try
 {
  clear(x);
 }
 catch(...)
 {
  report();
 }
}
ptr detach(ptr& x)
{
 const ptr r=x;
 clear(x);
 return r;
}
ptr detach(mem& x)
{
 return detach(x.pointer);
}
mem detach(buf& x)
{
 if(is_normal(x))
 {
  mem r;
  swap(r,x.memory);
  reset(x);
  return r;
 }
 const mem r=to_mem(x);
 clear(x);
 return r;
}
void dim(buf& x,const int y)
{
 check(y>=0);
 reserve(x,y);
 x.length=y;
}
void dir_change(const mem& x)
{
 const mem m=to_c(x);
 const char* p=get_address(m);
 const int n=chdir(p);
 check(n==0);
}
void dir_clean(const mem& x)
{
 if(is_dir(x))
  dir_remove(x);
}
void dir_copy(const mem& x,const mem& y)
{
 check(is_dir(x));
 check(is_dir(y));
 const mem qx=quote(x);
 const mem qy=quote(y);
 const mem m=concat("cp -rf ",qx," ",qy);
 os_execute(m);
}
mem dir_current()
{
 mem m;
 allocate(m,PATH_MAX);
 const int l=get_length(m);
 char* p=get_address(m);
 char* r=getcwd(p,l);
 check(r!=0);
 return r;
}
txt dir_find(const mem& x)
{
 check(is_dir(x));
 txt r;
 const mem qx=quote(x);
 const mem command=concat("find ",qx);
 const mem result=os_execute(command);
 txt t=to_txt(result);
 while(is_full(t))
 {
  const mem m1=shift(t);
  const mem m2=path_real(m1);
  if(is_file(m2))
   push(r,m2);
 }
 sort(r);
 return r;
}
txt dir_list(const mem& x)
{
 check(is_dir(x));
 txt r;
 const mem dir=path_real(x);
 const mem qdir=quote(dir);
 const mem command=concat("ls ",qdir);
 const mem result=os_execute(command);
 txt t=to_txt(result);
 while(is_full(t))
 {
  const mem m1=shift(t);
  const mem m2=path_concat(x,m1);
  const mem m3=path_real(m2);
  if(eq(m3,dir))
   continue;
  push(r,m3);
 }
 sort(r);
 return r;
}
mem dir_locate(const mem& x)
{
 check(is_full(x));
 const mem current=dir_current();
 txt dirs=split(current,'/');
 while(is_full(dirs))
 {
  const mem dir=join(dirs,'/');
  const mem path=path_concat(dir,x);
  if(is_dir(path))
   return path_real(path);
  drop_r(dirs);
 }
 stop();
 return {};
}
void dir_make(const mem& x)
{
 check(!is_fs(x));
 const mem qx=quote(x);
 const mem command=concat("mkdir -p ",qx);
 os_execute(command);
 check(is_dir(x));
}
void dir_remove(const mem& x)
{
 check(is_dir(x));
 const mem qx=quote(x);
 const mem m=concat("rm -rf ",qx);
 os_execute(m);
}
void dir_reset(const mem& x)
{
 dir_clean(x);
 dir_make(x);
}
long disk_free()
{
 const long s=disk_size();
 const long u=disk_usage();
 return s-u;
}
long disk_size()
{
 struct statfs s;
 const int n=statfs("/",&s);
 check(n==0);
 return s.f_bsize*s.f_blocks;
}
long disk_usage()
{
 struct statfs s;
 const int n=statfs("/",&s);
 check(n==0);
 const long l=disk_size();
 return l-s.f_bsize*s.f_bfree;
}
void drop_l(mem& x)
{
 check(is_full(x));
 drop_l(x,1);
}
void drop_l(mem& x,const int y)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<=l);
 const int ly=l-y;
 copy(x,0,y,ly);
 allocate(x,ly);
}
void drop_l(buf& x)
{
 check(is_full(x));
 drop_l(x,1);
}
void drop_l(buf& x,const int y)
{
 check(y>=0);
 check(y<=x.length);
 const int l=get_capacity(x);
 x.index+=y;
 x.index%=l;
 x.length-=y;
}
void drop_l(txt& x)
{
 check(is_full(x));
 while(is_full(x.buffer))
 {
  const char c=shift(x.buffer);
  if(c=='\n')
   break;
 }
}
void drop_l(seq& x)
{
 check(is_full(x));
 drop_l(x.text);
}
void drop_l(txt& x,const int y)
{
 check(y>=0);
 for(int i=0;i<y;i++)
 {
  drop_l(x);
 }
}
void drop_l(seq& x,const int y)
{
 check(y>=0);
 for(int i=0;i<y;i++)
 {
  drop_l(x);
 }
}
void drop_r(mem& x)
{
 check(is_full(x));
 drop_r(x,1);
}
void drop_r(mem& x,const int y)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<=l);
 const int ly=l-y;
 allocate(x,ly);
}
void drop_r(buf& x)
{
 check(is_full(x));
 drop_r(x,1);
}
void drop_r(buf& x,const int y)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<=l);
 x.length-=y;
}
void drop_r(txt& x)
{
 check(is_full(x));
 drop_r(x.buffer);
 while(is_full(x.buffer))
 {
  const char c=back(x.buffer);
  if(c=='\n')
   break;
  drop_r(x.buffer);
 }
}
void drop_r(seq& x)
{
 check(is_full(x));
 drop_r(x.text);
}
void drop_r(txt& x,const int y)
{
 check(y>=0);
 for(int i=0;i<y;i++)
 {
  drop_r(x);
 }
}
void drop_r(seq& x,const int y)
{
 check(y>=0);
 for(int i=0;i<y;i++)
 {
  drop_r(x);
 }
}
void dump(const bool x)
{
 print(x);
}
void dump(const char x)
{
 print(x);
}
void dump(const int x)
{
 print(x);
}
void dump(const unsigned x)
{
 print(x);
}
void dump(const long x)
{
 print(x);
}
void dump(const unsigned long x)
{
 print(x);
}
void dump(const double x)
{
 print(x);
}
void dump(const void* x)
{
 print(x);
}
void dump(const char* x)
{
 check(x!=0);
 const mem m=to_mem(x);
 dump(m);
}
void dump(const mem& x)
{
 const mem m=to_lit(x);
 print(m);
}
void dump(const buf& x)
{
 const mem m=to_lit(x);
 print(m);
}
void dump(const txt& x)
{
 txt t=x;
 int n=0;
 while(is_full(t))
 {
  const mem m=shift(t);
  echo("#");
  echo(n);
  echo(" ");
  dump(m);
  n++;
 }
}
void dump(const seq& x)
{
 seq s=x;
 int n=0;
 while(is_full(s))
 {
  const mem m=shift(s);
  echo("#");
  echo(n);
  echo(" ");
  dump(m);
  n++;
 }
}
void dump(const map& x)
{
 map m=x;
 while(is_full(m))
 {
  const mem k=shift(m.keys);
  const mem v=shift(m.values);
  echo(to_lit(k));
  echo(" ");
  echo(to_lit(v));
  print();
 }
}
void echo(const bool x)
{
 const mem m=to_mem(x);
 echo(m);
}
void echo(const char x)
{
 const mem m=to_mem(x);
 echo(m);
}
void echo(const int x)
{
 const mem m=to_mem(x);
 echo(m);
}
void echo(const unsigned x)
{
 const mem m=to_mem(x);
 echo(m);
}
void echo(const long x)
{
 const mem m=to_mem(x);
 echo(m);
}
void echo(const unsigned long x)
{
 const mem m=to_mem(x);
 echo(m);
}
void echo(const double x)
{
 const mem m=to_mem(x);
 echo(m);
}
void echo(const void* x)
{
 const mem m=to_mem(x);
 echo(m);
}
void echo(const char* x)
{
 check(x!=0);
 const mem m=to_mem(x);
 echo(m);
}
void echo(const mem& x)
{
 if(is_empty(x))
  return;
 const int l=get_length(x);
 const char* p=get_address(x);
 const int n=write(STDOUT_FILENO,p,l);
 check(n==l);
}
void echo(const buf& x)
{
 const mem m=to_mem(x);
 echo(m);
}
mem encode(const char x)
{
 mem r;
 if(x=='\\')
  append(r,"\\\\");
 else if(x=='\n')
  append(r,"\\n");
 else
  push(r,x);
 return r;
}
mem encode(const mem& x)
{
 const int l=get_length(x);
 buf b;
 reserve(b,l);
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  const mem m=encode(c);
  append(b,m);
 }
 return detach(b);
}
bool eq(const mem& x,const mem& y)
{
 const int lx=get_length(x);
 const int ly=get_length(y);
 if(lx!=ly)
  return false;
 for(int i=0;i<lx;i++)
 {
  const char cx=at(x,i);
  const char cy=at(y,i);
  if(cx!=cy)
   return false;
 }
 return true;
}
bool eq(const buf& x,const buf& y)
{
 const int lx=get_length(x);
 const int ly=get_length(y);
 if(lx!=ly)
  return false;
 for(int i=0;i<lx;i++)
 {
  const char cx=at(x,i);
  const char cy=at(y,i);
  if(cx!=cy)
   return false;
 }
 return true;
}
mem escape(const char x)
{
 if(x==' ')
  return " ";
 if(x=='\r')
  return "\\r";
 if(x=='\n')
  return "\\n";
 if(x=='\t')
  return "\\t";
 if(x=='"')
  return "\\\"";
 if(is_punct(x))
 {
  mem r;
  push(r,x);
  return r;
 }
 if(is_alnum(x))
 {
  mem r;
  push(r,x);
  return r;
 }
 const unsigned char u=x;
 const mem m1=to_hexa(u);
 const mem m2=pad_l(m1,2,'0');
 return concat("\\x",m2);
}
mem escape(const mem& x)
{
 const int l=get_length(x);
 buf b;
 reserve(b,l);
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  const mem m=escape(c);
  append(b,m);
 }
 return detach(b);
}
void file_copy(const mem& x,const mem& y)
{
 check(is_file(x));
 if(is_dir(y))
 {
  const mem base=path_base(x);
  const mem m=path_concat(y,base);
  file_copy(x,m);
  return;
 }
 const mem qx=quote(x);
 const mem qy=quote(y);
 const mem m=concat("cp -f --preserve ",qx," ",qy);
 os_execute(m);
}
bool file_eq(const mem& x,const mem& y)
{
 check(is_file(x));
 check(is_file(y));
 const long lx=file_size(x);
 const long ly=file_size(y);
 if(lx!=ly)
  return false;
 const mem mx=load(x);
 const mem my=load(y);
 return eq(mx,my);
}
mem file_locate(const mem& x)
{
 check(is_full(x));
 const mem current=dir_current();
 txt dirs=split(current,'/');
 while(is_full(dirs))
 {
  const mem dir=join(dirs,'/');
  const mem path=path_concat(dir,x);
  if(is_file(path))
   return path_real(path);
  drop_r(dirs);
 }
 stop();
 return {};
}
int file_mtime(const mem& x)
{
 check(is_file(x));
 const struct stat s=fs_stat(x);
 return s.st_mtime;
}
long file_size(const mem& x)
{
 check(is_file(x));
 const struct stat s=fs_stat(x);
 return s.st_size;
}
int find(const mem& x,const char y)
{
 const int l=get_length(x);
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  if(c==y)
   return i;
 }
 return -1;
}
void flower()
{
 flower("*");
}
void flower(const mem& x)
{
 check(is_full(x));
 const int width=term_width();
 const mem m1=repeat(x,width);
 const mem m2=head(m1,width);
 print(m2);
}
char front(const mem& x)
{
 check(is_full(x));
 return at(x,0);
}
void front(mem& x,const char y)
{
 check(is_full(x));
 set(x,0,y);
}
char front(const buf& x)
{
 check(is_full(x));
 return at(x,0);
}
void front(buf& x,const char y)
{
 check(is_full(x));
 set(x,0,y);
}
mem front(const txt& x)
{
 check(is_full(x));
 buf b;
 const int l=get_length(x.buffer);
 for(int i=0;i<l;i++)
 {
  const char c=at(x.buffer,i);
  if(c=='\n')
   break;
  push(b,c);
 }
 return detach(b);
}
mem front(const seq& x)
{
 check(is_full(x));
 const mem m=front(x.text);
 return decode(m);
}
void fs_remove(const mem& x)
{
 check(is_fs(x));
 const mem m=to_c(x);
 const char* p=get_address(m);
 const int n=remove(p);
 check(n==0);
}
void fs_rename(const mem& x,const mem& y)
{
 check(is_fs(x));
 const mem mx=to_c(x);
 const mem my=to_c(y);
 const char* px=get_address(mx);
 const char* py=get_address(my);
 const int n=rename(px,py);
 check(n==0);
}
struct stat fs_stat(const mem& x)
{
 check(is_fs(x));
 const mem m=to_c(x);
 struct stat r;
 const char* p=get_address(m);
 const int n=stat(p,&r);
 check(n==0);
 return r;
}
char* get_address(ptr& x)
{
 return get_address(x,0);
}
char* get_address(mem& x)
{
 return get_address(x,0);
}
const char* get_address(const mem& x)
{
 return get_address(x,0);
}
char* get_address(ptr& x,const int y)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<l);
 return x.pointer+y;
}
const char* get_address(const ptr& x,const int y)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<l);
 return x.pointer+y;
}
char* get_address(mem& x,const int y)
{
 return get_address(x.pointer,y);
}
const char* get_address(const mem& x,const int y)
{
 return get_address(x.pointer,y);
}
int get_capacity(const buf& x)
{
 return get_length(x.memory);
}
mem get(const map& x,const mem& y)
{
 check(is_full(y));
 map m=x;
 while(is_full(m))
 {
  const mem k=shift(m.keys);
  const mem v=shift(m.values);
  if(eq(k,y))
   return v;
 }
 stop();
 return {};
}
mem get_date()
{
 const int n=time(0);
 check(n!=-1);
 return get_date(n);
}
mem get_date(const int x)
{
 const time_t t=x;
 const tm* p=localtime(&t);
 check(p!=0);
 const tm s=*p;
 const int m=s.tm_mon+1;
 const int y=s.tm_year+1900;
 const mem minute=pad_l(s.tm_min,2);
 const mem hour=pad_l(s.tm_hour,2);
 const mem day=pad_l(s.tm_mday,2);
 const mem month=pad_l(m,2);
 const mem year=to_mem(y);
 const mem date=concat(year,"/",month,"/",day);
 const mem time=concat(hour,"h",minute);
 return concat(date," ",time);
}
mem get_domain(const mem& x)
{
 const mem command=concat("nslookup ",x);
 pfile f;
 open(f,command);
 const mem m1=read(f);
 try
 {
  clear(f);
 }
 catch(...)
 {
  return x;
 }
 const txt t1=to_txt(m1);
 const mem m2=front(t1);
 const txt t2=split(m2,'\t');
 const mem m3=back(t2);
 const txt t3=split(m3,' ');
 const mem m4=back(t3);
 return strip_r(m4,'.');
}
mem get_guid()
{
 return get_guid(16);
}
mem get_guid(const int x)
{
 check(x>0);
 buf b;
 reserve(b,x);
 for(int i=0;i<x;i++)
 {
  const int n=get_random(36);
  if(n<10)
  {
   const char c='0'+n;
   push(b,c);
  }
  else
  {
   const int v=n-10;
   const char c='a'+v;
   push(b,c);
  }
 }
 return detach(b);
}
mem get_ip(const fd& x)
{
 sockaddr_in a;
 socklen_t l=sizeof(a);
 const int n=getpeername(x.handle,(sockaddr*)&a,&l);
 check(n==0);
 const char* p=inet_ntoa(a.sin_addr);
 check(p!=0);
 return p;
}
int get_length(const ptr& x)
{
 return x.length;
}
int get_length(const mem& x)
{
 return get_length(x.pointer);
}
int get_length(const buf& x)
{
 return x.length;
}
double get_now()
{
 static thread_local const double origin=get_time();
 const double n=get_time();
 return n-origin;
}
int get_offset(const buf& x,const int y)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<l);
 const int lx=get_capacity(x);
 const int n=x.index+y;
 return n%lx;
}
int get_overhead(const buf& x)
{
 const int l=get_capacity(x);
 return l-x.length;
}
int get_random(const int x)
{
 check(x>=0);
 int v=0;
 int n=0;
 while(n<x)
 {
  const int i=rand();
  v+=i;
  n+=RAND_MAX;
 }
 return v%x;
}
double get_time()
{
 timespec spec;
 const int n=clock_gettime(CLOCK_MONOTONIC,&spec);
 check(n==0);
 const double s=spec.tv_sec;
 const double t=spec.tv_nsec/10e9;
 return s+t;
}
mem glue(const txt& x)
{
 return glue(x,'\n');
}
mem glue(const seq& x)
{
 return glue(x,'\n');
}
mem glue(const txt& x,const char y)
{
 mem r=join(x,y);
 if(is_empty(r))
  return r;
 check(match_r(r,y));
 drop_r(r);
 return r;
}
mem glue(const seq& x,const char y)
{
 mem r=join(x,y);
 if(is_empty(r))
  return r;
 check(match_r(r,y));
 drop_r(r);
 return r;
}
mem glue(const txt& x,const mem& y)
{
 mem r=join(x,y);
 if(is_empty(r))
  return r;
 check(match_r(r,y));
 const int l=get_length(y);
 drop_r(r,l);
 return r;
}
mem glue(const seq& x,const mem& y)
{
 mem r=join(x,y);
 if(is_empty(r))
  return r;
 check(match_r(r,y));
 const int l=get_length(y);
 drop_r(r,l);
 return r;
}
bool has(map& x,const mem& y)
{
 check(is_full(y));
 return contain(x.keys,y);
}
mem head(const mem& x,const int y)
{
 check(y>=0);
 const int l=get_length(x);
 if(l<=y)
  return x;
 return slice_l(x,y);
}
mem head(const buf& x,const int y)
{
 check(y>=0);
 const int l=get_length(x);
 if(l<=y)
  return x;
 return slice_l(x,y);
}
bool hit(const timer& x)
{
 const double end=x.start+x.duration;
 const double now=get_now();
 return now>end;
}
mem implode(const txt& x)
{
 mem r;
 txt t=x;
 while(is_full(t))
 {
  const mem m=shift(t);
  append(r,m);
 }
 return r;
}
mem implode(const seq& x)
{
 mem r;
 seq s=x;
 while(is_full(s))
 {
  const mem m=shift(s);
  append(r,m);
 }
 return r;
}
bool is_alnum(const char x)
{
 if(x=='_')
  return true;
 if(is_alpha(x))
  return true;
 if(is_digit(x))
  return true;
 return false;
}
bool is_alnum(const mem& x)
{
 if(is_empty(x))
  return false;
 const int l=get_length(x);
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  if(!is_alnum(c))
   return false;
 }
 return true;
}
bool is_alpha(const char x)
{
 static thread_local const mem low="abcdefghijklmnopqrstuvwxyz";
 static thread_local const mem up="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
 if(contain(low,x))
  return true;
 if(contain(up,x))
  return true;
 return false;
}
bool is_alpha(const mem& x)
{
 if(is_empty(x))
  return false;
 const int l=get_length(x);
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  if(!is_alpha(c))
   return false;
 }
 return true;
}
bool is_blank(const char x)
{
 if(x==' ')
  return true;
 if(x=='\t')
  return true;
 if(x=='\n')
  return true;
 if(x=='\r')
  return true;
 return false;
}
bool is_blank(const mem& x)
{
 if(is_empty(x))
  return true;
 const int l=get_length(x);
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  if(!is_blank(c))
   return false;
 }
 return true;
}
bool is_circular(const buf& x)
{
 const int l=get_capacity(x);
 const int n=x.index+x.length;
 return n>l;
}
bool is_digit(const char x)
{
 static thread_local const mem m="0123456789";
 return contain(m,x);
}
bool is_digit(const mem& x)
{
 const int l=get_length(x);
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  if(!is_digit(c))
   return false;
 }
 return true;
}
bool is_dir(const mem& x)
{
 const mem m=to_c(x);
 struct stat s;
 const char* p=get_address(m);
 const int n=stat(p,&s);
 if(n!=0)
  return false;
 return S_ISDIR(s.st_mode);
}
bool is_empty(const ptr& x)
{
 return x.length==0;
}
bool is_empty(const buf& x)
{
 return x.length==0;
}
bool is_empty(const mem& x)
{
 return is_empty(x.pointer);
}
bool is_empty(const txt& x)
{
 if(is_full(x.buffer))
  check(back(x.buffer)=='\n');
 return is_empty(x.buffer);
}
bool is_empty(const seq& x)
{
 return is_empty(x.text);
}
bool is_empty(const fd& x)
{
 return x.handle<0;
}
bool is_empty(const pfile& x)
{
 return x.pointer==0;
}
bool is_empty(const pthread& x)
{
 return x.pointer==0;
}
bool is_empty(const map& x)
{
 return is_empty(x.keys);
}
bool is_empty(const grid& x)
{
 return is_empty(x.cells);
}
bool is_empty(const tbl& x)
{
 return is_empty(x.cells);
}
bool is_file(const mem& x)
{
 const mem m=to_c(x);
 struct stat s;
 const char* p=get_address(m);
 const int n=stat(p,&s);
 if(n!=0)
  return false;
 return S_ISREG(s.st_mode);
}
bool is_filled(const buf& x)
{
 const int l=get_capacity(x);
 return x.length==l;
}
bool is_fs(const mem& x)
{
 if(is_dir(x))
  return true;
 if(is_file(x))
  return true;
 return false;
}
bool is_full(const ptr& x)
{
 return !is_empty(x);
}
bool is_full(const mem& x)
{
 return !is_empty(x);
}
bool is_full(const buf& x)
{
 return !is_empty(x);
}
bool is_full(const fd& x)
{
 return !is_empty(x);
}
bool is_full(const pfile& x)
{
 return !is_empty(x);
}
bool is_full(const pthread& x)
{
 return !is_empty(x);
}
bool is_full(const seq& x)
{
 return !is_empty(x);
}
bool is_full(const txt& x)
{
 return !is_empty(x);
}
bool is_full(const map& x)
{
 return !is_empty(x);
}
bool is_full(const grid& x)
{
 return !is_empty(x);
}
bool is_full(const tbl& x)
{
 return !is_empty(x);
}
bool is_line(const mem& x)
{
 return !is_text(x);
}
bool is_nl(const char x)
{
 if(x=='\n')
  return true;
 if(x=='\r')
  return true;
 return false;
}
bool is_nl(const mem& x)
{
 if(is_empty(x))
  return true;
 const int l=get_length(x);
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  if(!is_nl(c))
   return false;
 }
 return false;
}
bool is_normal(const buf& x)
{
 if(x.index>0)
  return false;
 return is_filled(x);
}
bool is_punct(const char x)
{
 static thread_local const mem m="!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
 return contain(m,x);
}
bool is_space(const char x)
{
 if(x==' ')
  return true;
 if(x=='\t')
  return true;
 return false;
}
bool is_space(const mem& x)
{
 if(is_empty(x))
  return true;
 const int l=get_length(x);
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  if(!is_space(c))
   return false;
 }
 return true;
}
bool is_text(const mem& x)
{
 if(contain(x,'\r'))
  return true;
 if(contain(x,'\n'))
  return true;
 return false;
}
mem join(const txt& x)
{
 return to_mem(x);
}
mem join(const seq& x)
{
 return join(x,'\n');
}
mem join(const txt& x,const char y)
{
 mem r=to_mem(x);
 const int l=get_length(r);
 for(int i=0;i<l;i++)
 {
  const char c=at(r,i);
  if(c=='\n')
   set(r,i,y);
 }
 return r;
}
mem join(const seq& x,const char y)
{
 mem r;
 seq t=x;
 while(is_full(t))
 {
  const mem m=shift(t);
  append(r,m);
  push(r,y);
 }
 return r;
}
mem join(const txt& x,const mem& y)
{
 mem r;
 txt t=x;
 while(is_full(t))
 {
  const mem m=shift(t);
  append(r,m);
  append(r,y);
 }
 return r;
}
mem join(const seq& x,const mem& y)
{
 mem r;
 seq t=x;
 while(is_full(t))
 {
  const mem m=shift(t);
  append(r,m);
  append(r,y);
 }
 return r;
}
void* lift(const mem& x)
{
 mem m=encode(x);
 push(m,'\n');
 const ptr p=detach(m);
 return p.pointer;
}
void* lift(const txt& x)
{
 mem m;
 serialize(m,x);
 return lift(m);
}
void* lift(const map& x)
{
 mem m;
 serialize(m,x);
 return lift(m);
}
void listen(fd& x,const int y)
{
 clear(x);
 x.handle=socket(AF_INET,SOCK_STREAM,0);
 check(x.handle>=0);
 const int on=1;
 const int n1=setsockopt(x.handle,SOL_SOCKET,SO_REUSEADDR,&on,sizeof(on));
 check(n1==0);
 struct sockaddr_in a;
 a.sin_family=AF_INET;
 a.sin_addr.s_addr=htonl(INADDR_ANY);
 a.sin_port=htons(y);
 const int n2=bind(x.handle,(sockaddr*)&a,sizeof(a));
 check(n2==0);
 const int n3=listen(x.handle,8);
 check(n3==0);
 set_non_block(x);
}
mem load(const mem& x)
{
 fd f;
 open(f,x);
 return read(f);
}
int main(const int x,const char** y)
{
 const int n=time(0);
 srand(n);
 txt t;
 for(int i=0;i<x;i++)
 {
  const char* p=y[i];
  push(t,p);
 }
 entry(t);
 return 0;
}
bool match_l(const mem& x,const char y)
{
 if(is_empty(x))
  return false;
 const char c=front(x);
 return c==y;
}
bool match_l(const buf& x,const char y)
{
 if(is_empty(x))
  return false;
 const char c=front(x);
 return c==y;
}
bool match_l(const mem& x,const mem& y)
{
 if(is_empty(x))
  return false;
 if(is_empty(y))
  return false;
 const int lx=get_length(x);
 const int ly=get_length(y);
 if(ly>lx)
  return false;
 for(int i=0;i<ly;i++)
 {
  const char cx=at(x,i);
  const char cy=at(y,i);
  if(cx!=cy)
   return false;
 }
 return true;
}
bool match_l(const buf& x,const mem& y)
{
 if(is_empty(x))
  return false;
 if(is_empty(y))
  return false;
 const int lx=get_length(x);
 const int ly=get_length(y);
 if(ly>lx)
  return false;
 for(int i=0;i<ly;i++)
 {
  const char cx=at(x,i);
  const char cy=at(y,i);
  if(cx!=cy)
   return false;
 }
 return true;
}
bool match_r(const mem& x,const char y)
{
 if(is_empty(x))
  return false;
 const char c=back(x);
 return c==y;
}
bool match_r(const buf& x,const char y)
{
 if(is_empty(x))
  return false;
 const char c=back(x);
 return c==y;
}
bool match_r(const mem& x,const mem& y)
{
 if(is_empty(x))
  return false;
 if(is_empty(y))
  return false;
 const int lx=get_length(x);
 const int ly=get_length(y);
 if(ly>lx)
  return false;
 for(int i=0;i<ly;i++)
 {
  const char cx=back(x,i);
  const char cy=back(y,i);
  if(cx!=cy)
   return false;
 }
 return true;
}
bool match_r(const buf& x,const mem& y)
{
 if(is_empty(x))
  return false;
 if(is_empty(y))
  return false;
 const int lx=get_length(x);
 const int ly=get_length(y);
 if(ly>lx)
  return false;
 for(int i=0;i<ly;i++)
 {
  const char cx=back(x,i);
  const char cy=back(y,i);
  if(cx!=cy)
   return false;
 }
 return true;
}
void move(ptr& x,ptr& y)
{
 swap(x,y);
 clear(y);
}
void move(mem& x,mem& y)
{
 swap(x,y);
 clear(y);
}
void move(buf& x,buf& y)
{
 swap(x,y);
 clear(y);
}
void move(txt& x,txt& y)
{
 swap(x,y);
 clear(y);
}
void move(seq& x,seq& y)
{
 swap(x,y);
 clear(y);
}
void move(map& x,map& y)
{
 swap(x,y);
 clear(y);
}
void move(timer& x,timer& y)
{
 swap(x,y);
 clear(y);
}
void move(grid& x,grid& y)
{
 swap(x,y);
 clear(y);
}
void move(tbl& x,tbl& y)
{
 swap(x,y);
 clear(y);
}
bool neq(const mem& x,const mem& y)
{
 return !eq(x,y);
}
bool neq(const buf& x,const buf& y)
{
 return !eq(x,y);
}
void normalize(buf& x)
{
 if(is_normal(x))
  return;
 mem m=to_mem(x);
 reset(x);
 swap(x.memory,m);
 const int l=get_length(x.memory);
 x.length=l;
}
void open(fd& x,const mem& y)
{
 open(x,y,O_RDONLY);
}
void open(fd& x,const mem& y,const int z)
{
 clear(x);
 const mem m=to_c(y);
 const char* p=get_address(m);
 x.handle=open(p,z,0644);
 check(is_full(x));
}
void open(pfile& x,const mem& y)
{
 clear(x);
 const mem m=to_c(y);
 const char* p=get_address(m);
 x.pointer=popen(p,"r");
 check(is_full(x));
}
mem os_execute(const mem& x)
{
 print(concat("> ",x));
 pfile f;
 open(f,x);
 const mem r=read(f);
 clear(f);
 return r;
}
int os_system(const mem& x)
{
 const mem m=to_c(x);
 const char* p=get_address(m);
 return system(p);
}
int os_system(const mem& x,const mem& y)
{
 int r=0;
 const mem previous=dir_current();
 dir_change(x);
 try
 {
  r=os_system(y);
 }
 catch(...)
 {
  dir_change(previous);
  throw;
 }
 dir_change(previous);
 return r;
}
mem pad_l(const int x,const int y)
{
 check(y>=0);
 const mem m=to_mem(x);
 return pad_l(m,y,'0');
}
mem pad_l(const mem& x,const int y,const char z)
{
 check(y>=0);
 mem r=x;
 while(true)
 {
  const int l=get_length(r);
  if(l>=y)
   break;
  unshift(r,z);
 }
 return r;
}
mem pad_r(const int x,const int y)
{
 check(y>=0);
 const mem m=to_mem(x);
 return pad_r(m,y,'0');
}
mem pad_r(const mem& x,const int y,const char z)
{
 check(y>=0);
 mem r=x;
 while(true)
 {
  const int l=get_length(r);
  if(l>=y)
   break;
  push(r,z);
 }
 return r;
}
mem path_base(const mem& x)
{
 mem m=to_c(x);
 char* p=get_address(m);
 return basename(p);
}
mem path_concat(const mem& x,const mem& y)
{
 return concat(x,"/",y);
}
mem path_dir(const mem& x)
{
 mem m=to_c(x);
 char* p=get_address(m);
 return dirname(p);
}
mem path_ext(const mem& x)
{
 const mem base=path_base(x);
 txt t=split(base,'.');
 const int l=count(t);
 if(l<2)
  return "";
 return pop(t);
}
mem path_name(const mem& x)
{
 const mem base=path_base(x);
 txt t=split(base,'.');
 const int l=count(t);
 if(l>1)
  drop_r(t);
 return glue(t,'.');
}
mem path_real(const mem& x)
{
 const mem m=to_c(x);
 mem result;
 allocate(result,PATH_MAX);
 const char* p=get_address(m);
 char* presult=get_address(result);
 char* r=realpath(p,presult);
 check(r!=0);
 return r;
}
mem path_resolve(const mem& x)
{
 if(is_fs(x))
  return path_real(x);
 const mem dir1=path_dir(x);
 if(is_dir(dir1))
 {
  const mem dir2=path_real(dir1);
  const mem base=path_base(x);
  return path_concat(dir2,base);
 }
 return x;
}
mem path_tmp(const mem& x)
{
 const mem dir1=dir_locate("tmp");
 const mem dir2=path_dir(x);
 const mem dir3=path_concat(dir1,dir2);
 const mem base=path_base(x);
 const mem path=path_concat(dir3,base);
 return path_unique(path);
}
mem path_unique(const mem& x)
{
 const mem dir=path_dir(x);
 const mem name1=path_name(x);
 const mem ext=path_ext(x);
 txt t=split(name1,'-');
 const mem last=back(t);
 if(is_digit(last))
  drop_r(t);
 const mem name2=glue(t,'-');
 buf b;
 for(int i=0;;i++)
 {
  const mem n=to_mem(i);
  reset(b);
  append(b,dir);
  append(b,"/");
  append(b,name2);
  if(i>0)
  {
   append(b,"-");
   append(b,n);
  }
  if(is_full(ext))
  {
   append(b,".");
   append(b,ext);
  }
  if(!is_fs(b))
   break;
 }
 const mem path=detach(b);
 return path_resolve(path);
}
char pop(mem& x)
{
 check(is_full(x));
 const char r=back(x);
 drop_r(x);
 return r;
}
mem pop(mem& x,const int y)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<=l);
 const mem r=slice_r(x,y);
 drop_r(x,y);
 return r;
}
char pop(buf& x)
{
 check(is_full(x));
 const char r=back(x);
 drop_r(x);
 return r;
}
mem pop(buf& x,const int y)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<=l);
 const mem r=slice_r(x,y);
 drop_r(x,y);
 return r;
}
mem pop(txt& x)
{
 check(is_full(x));
 const mem r=back(x);
 drop_r(x);
 return r;
}
mem pop(seq& x)
{
 check(is_full(x));
 const mem r=back(x);
 drop_r(x);
 return r;
}
void prepend(mem& x,const mem& y)
{
 const int lx=get_length(x);
 const int ly=get_length(y);
 const int l=lx+ly;
 allocate(x,l);
 copy(x,lx,0,lx);
 copy(x,0,y,0,ly);
}
void prepend(buf& x,const char* y)
{
 const mem m=y;
 prepend(x,m);
}
void prepend(buf& x,const mem& y)
{
 const int lx=get_length(x);
 const int ly=get_length(y);
 const int l=lx+ly;
 reserve(x,l);
 for(int i=0;i<ly;i++)
 {
  const char c=back(y,i);
  unshift(x,c);
 }
}
void prepend(buf& x,const buf& y)
{
 const int lx=get_length(x);
 const int ly=get_length(y);
 const int l=lx+ly;
 reserve(x,l);
 for(int i=0;i<ly;i++)
 {
  const char c=back(y,i);
  unshift(x,c);
 }
}
void prepend(txt& x,const txt& y)
{
 prepend(x.buffer,y.buffer);
}
void prepend(seq& x,const seq& y)
{
 prepend(x.text,y.text);
}
void print()
{
 echo("\n");
}
void print(const bool x)
{
 echo(x);
 echo("\n");
}
void print(const char x)
{
 echo(x);
 echo("\n");
}
void print(const int x)
{
 echo(x);
 echo("\n");
}
void print(const unsigned x)
{
 echo(x);
 echo("\n");
}
void print(const long x)
{
 echo(x);
 echo("\n");
}
void print(const unsigned long x)
{
 echo(x);
 echo("\n");
}
void print(const double x)
{
 echo(x);
 echo("\n");
}
void print(const void* x)
{
 echo(x);
 echo("\n");
}
void print(const mem& x)
{
 echo(x);
 echo("\n");
}
void print(const buf& x)
{
 echo(x);
 echo("\n");
}
void print(const char* x)
{
 check(x!=0);
 echo(x);
 echo("\n");
}
void print(const txt& x)
{
 dump(x.buffer);
}
void print(const seq& x)
{
 dump(x.text);
}
void print(const map& x)
{
 dump(x);
}
void push(mem& x,const char y)
{
 const int l=get_length(x);
 const int lx=l+1;
 allocate(x,lx);
 back(x,0,y);
}
void push(buf& x,const char y)
{
 if(is_filled(x))
 {
  const int l=get_capacity(x)+1024;
  reserve(x,l);
 }
 x.length++;
 back(x,0,y);
}
void push(txt& x,const mem& y)
{
 check(!contain(y,'\n'));
 append(x.buffer,y);
 push(x.buffer,'\n');
}
void push(seq& x,const mem& y)
{
 const mem m=encode(y);
 push(x.text,m);
}
void put(map& x,const mem& y,const mem& z)
{
 check(is_full(y));
 map m;
 swap(m,x);
 bool found=false;
 while(is_full(m))
 {
  const mem k=shift(m.keys);
  const mem v=shift(m.values);
  push(x.keys,k);
  if(eq(k,y))
  {
   push(x.values,z);
   found=true;
  }
  else
   push(x.values,v);
 }
 if(!found)
 {
  push(x.keys,y);
  push(x.values,z);
 }
}
mem quote(const mem& x)
{
 mem r;
 push(r,'"');
 append(r,x);
 push(r,'"');
 return r;
}
mem read(const fd& x)
{
 buf b;
 while(true)
 {
  const mem m=read(x,1024);
  if(is_empty(m))
   break;
  append(b,m);
 }
 return detach(b);
}
mem read(const pfile& x)
{
 buf b;
 while(true)
 {
  const mem m=read(x,1024);
  if(is_empty(m))
   break;
  append(b,m);
 }
 return detach(b);
}
mem read(const fd& x,const int y)
{
 check(y>=0);
 mem r;
 if(y==0)
  return r;
 allocate(r,y);
 char* p=get_address(r);
 const int n=read(x.handle,p,y);
 check(n>=0);
 check(n<=y);
 allocate(r,n);
 return r;
}
mem read(const pfile& x,const int y)
{
 check(y>=0);
 mem r;
 if(y==0)
  return r;
 allocate(r,y);
 char* p=get_address(r);
 const int n=fread(p,1,y,x.pointer);
 check(n>=0);
 check(n<=y);
 allocate(r,n);
 return r;
}
mem receive(const fd& x)
{
 mem r;
 while(true)
 {
  mem m;
  allocate(m,1024);
  const int l=get_length(m);
  char* p=get_address(m);
  const int n=read(x.handle,p,l);
  if(n==0)
   break;
  if(n==-1)
  {
   check(errno==EAGAIN||errno==EWOULDBLOCK);
   break;
  }
  check(n>0);
  check(n<=l);
  allocate(m,n);
  append(r,m);
 }
 return r;
}
void remove(txt& x,const int y)
{
 remove(x,y,1);
}
void remove(txt& x,const int y,const int z)
{
 const int l=count(x);
 check(y>=0);
 check(z>=0);
 check(y<l);
 check(y+z<=l);
 txt t;
 const int n=y+z;
 move(t,x);
 for(int i=0;i<l;i++)
 {
  const mem m=shift(t);
  if(i<y||i>n)
   push(x,m);
 }
}
void remove(seq& x,const int y)
{
 remove(x.text,y);
}
void remove(seq& x,const int y,const int z)
{
 remove(x.text,y,z);
}
mem repeat(const mem& x,const int y)
{
 check(y>=0);
 buf b;
 const int l=get_length(x);
 const int ly=l*y;
 reserve(b,ly);
 for(int i=0;i<y;i++)
 {
  append(b,x);
 }
 return detach(b);
}
mem replace(const mem& x,const char y,const char z)
{
 mem r=x;
 const int l=get_length(r);
 for(int i=0;i<l;i++)
 {
  const char c=at(r,i);
  if(c==y)
   set(r,i,z);
 }
 return r;
}
mem replace(const mem& x,const mem& y,const mem& z)
{
 if(is_empty(y))
  return x;
 buf bx=to_buf(x);
 buf br;
 while(is_full(bx))
 {
  if(match_l(bx,y))
  {
   const int l=get_length(y);
   shift(bx,l);
   append(br,z);
  }
  else
  {
   const char c=shift(bx);
   push(br,c);
  }
 }
 return detach(br);
}
void report()
{
 const int n=errno;
 const mem m=strerror(n);
 print(concat("error ",to_lit(m)));
 print(concat("number ",to_mem(n)));
}
void reserve(buf& x,const int y)
{
 check(y>=0);
 const int lx=x.length;
 const int l=get_capacity(x);
 if(y<=lx)
  return;
 if(y<=l)
  return;
 buf b;
 swap(b,x);
 allocate(x.memory,y);
 x.length=lx;
 copy(x,0,b,0,lx);
}
void reset(buf& x)
{
 x.index=0;
 x.length=0;
}
void save(const mem& x,const mem& y)
{
 if(is_file(x))
 {
  const mem m=load(x);
  if(eq(m,y))
   return;
 }
 const mem dir=path_dir(x);
 if(!is_dir(dir))
  dir_make(dir);
 fd f;
 const int flag=O_WRONLY|O_CREAT|O_TRUNC;
 open(f,x,flag);
 write(f,y);
}
void send(const fd& x,const mem& y)
{
 const timer t=timeout(0.1);
 mem m=y;
 while(is_full(m))
 {
  check(!hit(t));
  const int l=get_length(m);
  const char* p=get_address(m);
  const int n=write(x.handle,p,l);
  if(n==-1)
  {
   check(errno==EAGAIN||errno==EWOULDBLOCK);
   wait(0.01);
  }
  check(n>=0);
  check(n<=l);
  drop_l(m,n);
 }
}
void serialize(mem& x,const txt& y)
{
 mem m=to_mem(y);
 move(x,m);
}
void serialize(mem& x,const map& y)
{
 map m=y;
 txt t;
 while(is_full(m))
 {
  const mem m1=shift(m.keys);
  const mem m2=shift(m.values);
  const mem key=encode(m1);
  const mem value=encode(m2);
  push(t,key);
  push(t,value);
 }
 serialize(x,t);
}
void set(mem& x,const int y,const char z)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<l);
 char* p=get_address(x,y);
 *p=z;
}
void set(buf& x,const int y,const char z)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<l);
 const int n=get_offset(x,y);
 set(x.memory,n,z);
}
void set(map& x,const mem& y,const mem& z)
{
 check(!has(x,y));
 push(x.keys,y);
 push(x.values,z);
}
void set_non_block(const fd& x)
{
 const int n1=fcntl(x.handle,F_GETFL,0);
 check(n1!=-1);
 const int n2=n1|O_NONBLOCK;
 const int n3=fcntl(x.handle,F_SETFL,n2);
 check(n3!=-1);
}
char shift(mem& x)
{
 check(is_full(x));
 const char r=front(x);
 drop_l(x);
 return r;
}
mem shift(mem& x,const int y)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<=l);
 const mem r=slice_l(x,y);
 drop_l(x,y);
 return r;
}
char shift(buf& x)
{
 check(is_full(x));
 const char r=front(x);
 drop_l(x);
 return r;
}
mem shift(buf& x,const int y)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<=l);
 const mem r=slice_l(x,y);
 drop_l(x,y);
 return r;
}
mem shift(txt& x)
{
 check(is_full(x));
 const mem r=front(x);
 drop_l(x);
 return r;
}
mem shift(seq& x)
{
 check(is_full(x));
 const mem r=front(x);
 drop_l(x);
 return r;
}
void shuffle(txt& x)
{
 txt t;
 move(t,x);
 while(is_full(t))
 {
  const int l=count(t);
  const int n=get_random(l);
  const mem m=at(t,n);
  push(x,m);
  remove(t,n);
 }
}
void shuffle(seq& x)
{
 shuffle(x.text);
}
mem single_quote(const mem& x)
{
 mem r;
 push(r,'\'');
 append(r,x);
 push(r,'\'');
 return r;
}
mem slice(const mem& x,const int y)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<=l);
 const int ly=l-y;
 return slice(x,y,ly);
}
mem slice(const buf& x,const int y)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<=l);
 const int ly=l-y;
 return slice(x,y,ly);
}
txt slice(const txt& x,const int y)
{
 const int l=count(x);
 check(y>=0);
 check(y<=l);
 const int ly=l-y;
 return slice(x,y,ly);
}
seq slice(const seq& x,const int y)
{
 const txt t=slice(x.text,y);
 return to_seq(t);
}
mem slice(const mem& x,const int y,const int z)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<=l);
 check(z>=0);
 check(y+z<=l);
 mem r;
 allocate(r,z);
 copy(r,0,x,y,z);
 return r;
}
mem slice(const buf& x,const int y,const int z)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<=l);
 check(z>=0);
 check(y+z<=l);
 mem r;
 allocate(r,z);
 copy(r,0,x,y,z);
 return r;
}
txt slice(const txt& x,const int y,const int z)
{
 const int l=count(x);
 check(y>=0);
 check(y<=l);
 check(z>=0);
 check(y+z<=l);
 const int ly=l-y-z;
 txt r=x;
 drop_l(r,y);
 drop_r(r,ly);
 return r;
}
seq slice(const seq& x,const int y,const int z)
{
 const txt t=slice(x.text,y,z);
 return to_seq(t);
}
mem slice_l(const mem& x,const int y)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<=l);
 return slice(x,0,y);
}
mem slice_l(const buf& x,const int y)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<=l);
 return slice(x,0,y);
}
txt slice_l(const txt& x,const int y)
{
 const int l=count(x);
 check(y>=0);
 check(y<=l);
 return slice(x,0,y);
}
seq slice_l(const seq& x,const int y)
{
 const txt t=slice_l(x.text,y);
 return to_seq(t);
}
mem slice_r(const mem& x,const int y)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<=l);
 const int n=l-y;
 return slice(x,n,y);
}
mem slice_r(const buf& x,const int y)
{
 const int l=get_length(x);
 check(y>=0);
 check(y<=l);
 const int n=l-y;
 return slice(x,n,y);
}
txt slice_r(const txt& x,const int y)
{
 const int l=count(x);
 check(y>=0);
 check(y<=l);
 const int n=l-y;
 return slice(x,n,y);
}
seq slice_r(const seq& x,const int y)
{
 const txt t=slice_r(x.text,y);
 return to_seq(t);
}
void sort(txt& x)
{
 if(is_empty(x))
  return;
 const mem path=path_tmp("sort.txt");
 const mem content=to_mem(x);
 save(path,content);
 const mem command=concat("sort ",path);
 const mem result=os_execute(command);
 fs_remove(path);
 x=to_txt(result);
}
txt split(const mem& x,const char y)
{
 txt r;
 buf b1=to_buf(x);
 buf b2;
 while(is_full(b1))
 {
  const char c=shift(b1);
  if(c==y)
  {
   const mem m=detach(b2);
   push(r,m);
  }
  else
   push(b2,c);
 }
 if(is_full(b2))
 {
  const mem m=detach(b2);
  push(r,m);
 }
 return r;
}
txt split(const mem& x,const mem& y)
{
 check(is_full(y));
 txt r;
 buf b1=to_buf(x);
 buf b2;
 const int l=get_length(y);
 while(is_full(b1))
 {
  if(match_l(b1,y))
  {
   shift(b1,l);
   const mem m=detach(b2);
   push(r,m);
  }
  else
  {
   const char c=shift(b1);
   push(b2,c);
  }
 }
 if(is_full(b2))
 {
  const mem m=detach(b2);
  push(r,m);
 }
 return r;
}
void stop()
{
 throw 42;
}
mem strip_l(const mem& x,const char y)
{
 if(match_l(x,y))
  return slice(x,1);
 return x;
}
mem strip_l(const buf& x,const char y)
{
 if(match_l(x,y))
  return slice(x,1);
 return x;
}
mem strip_l(const mem& x,const mem& y)
{
 if(match_l(x,y))
 {
  const int l=get_length(y);
  return slice(x,l);
 }
 return x;
}
mem strip_l(const buf& x,const mem& y)
{
 if(match_l(x,y))
 {
  const int l=get_length(y);
  return slice(x,l);
 }
 return x;
}
mem strip_r(const mem& x,const char y)
{
 if(match_r(x,y))
 {
  const int l=get_length(x);
  const int lx=l-1;
  return slice_l(x,lx);
 }
 return x;
}
mem strip_r(const buf& x,const char y)
{
 if(match_r(x,y))
 {
  const int l=get_length(x);
  const int lx=l-1;
  return slice_l(x,lx);
 }
 return x;
}
mem strip_r(const mem& x,const mem& y)
{
 if(match_r(x,y))
 {
  const int lx=get_length(x);
  const int ly=get_length(x);
  const int l=lx-ly;
  return slice_l(x,l);
 }
 return x;
}
mem strip_r(const buf& x,const mem& y)
{
 if(match_r(x,y))
 {
  const int lx=get_length(x);
  const int ly=get_length(x);
  const int l=lx-ly;
  return slice_l(x,l);
 }
 return x;
}
void swap(bool& x,bool& y)
{
 const bool b=x;
 x=y;
 y=b;
}
void swap(char& x,char& y)
{
 const bool b=x;
 x=y;
 y=b;
}
void swap(int& x,int& y)
{
 const int n=x;
 x=y;
 y=n;
}
void swap(unsigned& x,unsigned& y)
{
 const unsigned n=x;
 x=y;
 y=n;
}
void swap(long& x,long& y)
{
 const long n=x;
 x=y;
 y=n;
}
void swap(unsigned long& x,unsigned long& y)
{
 const unsigned long n=x;
 x=y;
 y=n;
}
void swap(double& x,double& y)
{
 const double n=x;
 x=y;
 y=n;
}
void swap(char*& x,char*& y)
{
 char* p=x;
 x=y;
 y=p;
}
void swap(FILE*& x,FILE*& y)
{
 FILE* p=x;
 x=y;
 y=p;
}
void swap(ptr& x,ptr& y)
{
 swap(x.pointer,y.pointer);
 swap(x.length,y.length);
}
void swap(mem& x,mem& y)
{
 swap(x.pointer,y.pointer);
}
void swap(buf& x,buf& y)
{
 swap(x.index,y.index);
 swap(x.length,y.length);
 swap(x.memory,y.memory);
}
void swap(txt& x,txt& y)
{
 swap(x.buffer,y.buffer);
}
void swap(seq& x,seq& y)
{
 swap(x.text,y.text);
}
void swap(fd& x,fd& y)
{
 swap(x.handle,y.handle);
}
void swap(pfile& x,pfile& y)
{
 swap(x.pointer,y.pointer);
}
void swap(map& x,map& y)
{
 swap(x.keys,y.keys);
 swap(x.values,y.values);
}
void swap(grid& x,grid& y)
{
 swap(x.width,y.width);
 swap(x.height,y.height);
 swap(x.cells,y.cells);
}
void swap(tbl& x,tbl& y)
{
 swap(x.columns,y.columns);
 swap(x.cells,y.cells);
}
void swap(timer& x,timer& y)
{
 swap(x.start,y.start);
 swap(x.duration,y.duration);
}
mem tail(const mem& x,const int y)
{
 check(y>=0);
 const int l=get_length(x);
 if(l<=y)
  return x;
 return slice_r(x,y);
}
mem tail(const buf& x,const int y)
{
 check(y>=0);
 const int l=get_length(x);
 if(l<=y)
  return x;
 return slice_r(x,y);
}
bool term_hit()
{
 const mem m=term_read();
 return is_full(m);
}
mem term_read()
{
 mem r;
 fd f;
 f.handle=dup(STDIN_FILENO);
 termios t1;
 const int n0=tcgetattr(f.handle,&t1);
 check(n0==0);
 const termios t2=t1;
 t1.c_lflag&=~ICANON;
 t1.c_lflag&=~ECHO;
 const int n1=tcsetattr(f.handle,TCSANOW,&t1);
 check(n1==0);
 const int n2=fcntl(f.handle,F_GETFL);
 check(n2!=-1);
 const int a=n2|O_NONBLOCK;
 const int n3=fcntl(f.handle,F_SETFL,a);
 check(n3!=-1);
 char c=0;
 const int n4=read(f.handle,&c,1);
 if(n4==1)
 {
  push(r,c);
  while(true)
  {
   const int n5=read(f.handle,&c,1);
   if(n5<=0)
    break;
   push(r,c);
  }
 }
 const int n6=tcsetattr(f.handle,TCSANOW,&t2);
 check(n6==0);
 return r;
}
int term_width()
{
 const mem m=os_execute("tput cols");
 return to_int(m);
}
timer timeout(const double x)
{
 check(x>=0);
 timer r;
 r.duration=x;
 return r;
}
void title_l(const mem& x)
{
 title_l(x,"*");
}
void title_l(const mem& x,const mem& y)
{
 check(is_full(x));
 check(is_full(y));
 const int width=term_width();
 const mem m1=repeat(y,2);
 const mem m2=repeat(y,width);
 const mem m3=concat(m1," ",x," ",m2);
 const mem m4=head(m3,width);
 print(m4);
}
void title_r(const mem& x)
{
 title_r(x,"*");
}
void title_r(const mem& x,const mem& y)
{
 check(is_full(x));
 check(is_full(y));
 const int width=term_width();
 const mem m1=repeat(y,2);
 const mem m2=repeat(y,width);
 const mem m3=concat(m2," ",x," ",m1);
 const mem m4=tail(m3,width);
 print(m4);
}
mem to_bsize(const long x)
{
 check(x>=0);
 const double v=x;
 const double kb=1024;
 const double mb=1024*kb;
 const double gb=1024*mb;
 const double tb=1024*gb;
 if(v<kb)
 {
  const mem m=to_mem(x);
  return concat(m,"b");
 }
 if(v<mb)
 {
  const double n=v/kb;
  const mem m=to_mem(n,2);
  return concat(m,"Kb");
 }
 if(v<gb)
 {
  const double n=v/mb;
  const mem m=to_mem(n,2);
  return concat(m,"Mb");
 }
 if(v<tb)
 {
  const double n=v/gb;
  const mem m=to_mem(n,2);
  return concat(m,"Gb");
 }
 const double n=v/tb;
 const mem m=to_mem(n,2);
 return concat(m,"Tb");
}
buf to_buf(const mem& x)
{
 const int l=get_length(x);
 buf r;
 dim(r,l);
 copy(r,0,x,0,l);
 return r;
}
mem to_c(const mem& x)
{
 mem r=x;
 push(r,0);
 return r;
}
mem to_hexa(const int x)
{
 mem r;
 allocate(r,256);
 const int l=get_length(r);
 char* p=get_address(r);
 const int n=sprintf(p,"%X",x);
 check(n>=0);
 check(n<l);
 allocate(r,n);
 return r;
}
int to_int(const mem& x)
{
 const long l=to_long(x);
 const int r=l;
 const long n=r;
 check(l==n);
 return r;
}
mem to_lit(const char x)
{
 const mem m=escape(x);
 return single_quote(m);
}
mem to_lit(const mem& x)
{
 const mem m=escape(x);
 return quote(m);
}
mem to_lit(const buf& x)
{
 const mem m=to_mem(x);
 return to_lit(m);
}
long to_long(const mem& x)
{
 const mem m=to_c(x);
 const char* p=get_address(m);
 const long r=strtol(p,0,10);
 check(r!=LONG_MIN);
 check(r!=LONG_MAX);
 return r;
}
char to_lower(const char x)
{
 return tolower(x);
}
mem to_lower(const mem& x)
{
 mem r;
 const int l=get_length(x);
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  const char n=to_lower(c);
  push(r,n);
 }
 return r;
}
mem to_mem(const bool x)
{
 if(x)
  return "true";
 return "false";
}
mem to_mem(const char x)
{
 mem r;
 push(r,x);
 return r;
}
mem to_mem(const int x)
{
 mem r;
 allocate(r,256);
 const int l=get_length(r);
 char* p=get_address(r);
 const int n=sprintf(p,"%d",x);
 check(n>=0);
 check(n<l);
 allocate(r,n);
 return r;
}
mem to_mem(const unsigned x)
{
 mem r;
 allocate(r,256);
 const int l=get_length(r);
 char* p=get_address(r);
 const int n=sprintf(p,"%u",x);
 check(n>=0);
 check(n<l);
 allocate(r,n);
 return r;
}
mem to_mem(const long x)
{
 mem r;
 allocate(r,256);
 const int l=get_length(r);
 char* p=get_address(r);
 const int n=sprintf(p,"%ld",x);
 check(n>=0);
 check(n<l);
 allocate(r,n);
 return r;
}
mem to_mem(const unsigned long x)
{
 mem r;
 allocate(r,256);
 const int l=get_length(r);
 char* p=get_address(r);
 const int n=sprintf(p,"%lu",x);
 check(n>=0);
 check(n<l);
 allocate(r,n);
 return r;
}
mem to_mem(const double x)
{
 mem r;
 allocate(r,256);
 const int l=get_length(r);
 char* p=get_address(r);
 const int n=sprintf(p,"%f",x);
 check(n>=0);
 check(n<l);
 allocate(r,n);
 while(is_full(r))
 {
  const char c=back(r);
  if(c!='0')
   break;
  drop_r(r);
 }
 const char c=back(r);
 if(c=='.')
  drop_r(r);
 return r;
}
mem to_mem(const double x,const int y)
{
 check(y>=0);
 const mem m=to_mem(x);
 if(!contain(m,'.'))
  return m;
 txt t=split(m,'.');
 const mem integer=shift(t);
 const mem real=shift(t);
 check(is_empty(t));
 mem round=head(real,y);
 if(is_empty(round))
  return integer;
 while(is_full(round))
 {
  const char c=back(round);
  if(c!='0')
   break;
  pop(round);
 }
 if(is_empty(round))
  return integer;
 return concat(integer,".",round);
}
mem to_mem(const void* x)
{
 mem r;
 allocate(r,256);
 const int l=get_length(r);
 char* p=get_address(r);
 const int n=sprintf(p,"%p",x);
 check(n>=0);
 check(n<l);
 allocate(r,n);
 return r;
}
mem to_mem(const char* x)
{
 check(x!=0);
 return x;
}
mem to_mem(const buf& x)
{
 return x;
}
mem to_mem(const txt& x)
{
 return to_mem(x.buffer);
}
mem to_mem(const seq& x)
{
 return to_mem(x.text);
}
mem to_mem(const map& x)
{
 mem r;
 serialize(r,x);
 return r;
}
seq to_seq(const txt& x)
{
 seq r;
 txt t=x;
 while(is_full(t))
 {
  const mem m=shift(t);
  push(r,m);
 }
 return r;
}
txt to_txt(const mem& x)
{
 txt r;
 buf b1=to_buf(x);
 buf b2;
 while(is_full(b1))
 {
  if(match_l(b1,"\r\n"))
  {
   shift(b1,2);
   push(b2,'\n');
  }
  else if(match_l(b1,'\r'))
  {
   shift(b1);
   push(b2,'\n');
  }
  else
  {
   const char c=shift(b1);
   push(b2,c);
  }
 }
 if(!match_r(b2,'\n'))
  push(b2,'\n');
 swap(r.buffer,b2);
 return r;
}
char to_upper(const char x)
{
 return toupper(x);
}
mem to_upper(const mem& x)
{
 mem r;
 const int l=get_length(x);
 for(int i=0;i<l;i++)
 {
  const char c=at(x,i);
  const char n=to_upper(c);
  push(r,n);
 }
 return r;
}
mem translit(const mem& x)
{
 mem r;
 const mem tmp=path_tmp("translit.txt");
 save(tmp,x);
 try
 {
  r=os_execute(concat("iconv -f UTF-8 -t ASCII//TRANSLIT ",tmp));
 }
 catch(...)
 {
  fs_remove(tmp);
  throw;
 }
 fs_remove(tmp);
 return r;
}
mem trim(const mem& x)
{
 const mem s=trim_l(x);
 return trim_r(s);
}
mem trim_l(const mem& x)
{
 buf b=to_buf(x);
 while(is_full(b))
 {
  const char c=front(b);
  if(!is_blank(c))
   break;
  shift(b);
 }
 return detach(b);
}
txt trim_l(const txt& x)
{
 txt r;
 txt t=x;
 while(is_full(t))
 {
  const mem m1=shift(t);
  const mem m2=trim_l(m1);
  push(r,m2);
 }
 return r;
}
mem trim_r(const mem& x)
{
 buf b=to_buf(x);
 while(is_full(b))
 {
  const char c=back(b);
  if(!is_blank(c))
   break;
  drop_r(b);
 }
 return detach(b);
}
txt trim_r(const txt& x)
{
 txt r;
 txt t=x;
 while(is_full(t))
 {
  const mem m1=shift(t);
  const mem m2=trim_r(m1);
  push(r,m2);
 }
 while(is_full(r))
 {
  const mem m=back(r);
  if(is_empty(m))
   drop_r(r);
  else
   break;
 }
 return r;
}
void unlift(mem& x,void* y)
{
 check(y!=0);
 buf b;
 while(true)
 {
  const int l=get_length(b);
  char* p=(char*)y;
  const char c=p[l];
  if(c=='\n')
   break;
  push(b,c);
 }
 const mem m=detach(b);
 free(y);
 x=decode(m);
}
void unlift(txt& x,void* y)
{
 check(y!=0);
 mem m;
 unlift(m,y);
 deserialize(x,m);
}
void unlift(map& x,void* y)
{
 check(y!=0);
 mem m;
 unlift(m,y);
 deserialize(x,m);
}
void unshift(mem& x,const char y)
{
 const int lx=get_length(x);
 const int l=lx+1;
 allocate(x,l);
 copy(x,1,0,lx);
 front(x,y);
}
void unshift(buf& x,const char y)
{
 if(is_filled(x))
 {
  const int l=get_capacity(x);
  const int lx=l+1024;
  reserve(x,lx);
 }
 if(x.index==0)
 {
  const int l=get_capacity(x);
  const int lx=l-1;
  x.index=lx;
 }
 else
  x.index--;
 x.length++;
 front(x,y);
}
void unshift(txt& x,const mem& y)
{
 check(!contain(y,'\n'));
 unshift(x.buffer,'\n');
 prepend(x.buffer,y);
}
void unshift(seq& x,const mem& y)
{
 const mem m=encode(y);
 unshift(x.text,m);
}
void unwrap(void* x,const int y,const mem& z)
{
 const int l=get_length(z);
 check(x!=0);
 check(y>0);
 check(y==l);
 const char* p=get_address(z);
 memcpy(x,p,y);
}
void unwrap(bool& x,const mem& y)
{
 unwrap(&x,sizeof(x),y);
}
void unwrap(char& x,const mem& y)
{
 unwrap(&x,sizeof(x),y);
}
void unwrap(int& x,const mem& y)
{
 unwrap(&x,sizeof(x),y);
}
void unwrap(unsigned& x,const mem& y)
{
 unwrap(&x,sizeof(x),y);
}
void unwrap(long& x,const mem& y)
{
 unwrap(&x,sizeof(x),y);
}
void unwrap(unsigned long& x,const mem& y)
{
 unwrap(&x,sizeof(x),y);
}
void unwrap(double& x,const mem& y)
{
 unwrap(&x,sizeof(x),y);
}
void unwrap(void*& x,const mem& y)
{
 unwrap(&x,sizeof(x),y);
}
void unwrap(txt& x,const mem& y)
{
 deserialize(x,y);
}
void unwrap(map& x,const mem& y)
{
 deserialize(x,y);
}
void wait(const double x)
{
 const double t=x*10e5;
 const int n=usleep(t);
 check(n==0);
}
mem wrap(const void* x,const int y)
{
 check(x!=0);
 check(y>0);
 mem r;
 allocate(r,y);
 char* p=get_address(r);
 memcpy(p,x,y);
 return r;
}
mem wrap(const bool y)
{
 return wrap(&y,sizeof(y));
}
mem wrap(const char y)
{
 return wrap(&y,sizeof(y));
}
mem wrap(const int y)
{
 return wrap(&y,sizeof(y));
}
mem wrap(const unsigned y)
{
 return wrap(&y,sizeof(y));
}
mem wrap(const long y)
{
 return wrap(&y,sizeof(y));
}
mem wrap(const unsigned long y)
{
 return wrap(&y,sizeof(y));
}
mem wrap(const double y)
{
 return wrap(&y,sizeof(y));
}
mem wrap(const void* y)
{
 return wrap(&y,sizeof(y));
}
mem wrap(const txt& x)
{
 mem m;
 serialize(m,x);
 return m;
}
mem wrap(const map& x)
{
 mem m;
 serialize(m,x);
 return m;
}
void write(const fd& x,const mem& y)
{
 check(is_full(x));
 if(is_empty(y))
  return;
 const char* p=get_address(y);
 const int l=get_length(y);
 const int n=write(x.handle,p,l);
 check(n==l);
}
void build(const mem& x,const bool release)
{
 const double begin=get_now();
 txt out;
 const txt macros=prg_generate_macros(x);
 const txt structs=prg_generate_structs(x);
 const txt prototypes=prg_generate_prototypes(x);
 const txt implements=prg_generate_implements(x);
 const txt binaries=prg_generate_binaries(x);
 append(out,macros);
 append(out,structs);
 append(out,prototypes);
 append(out,implements);
 append(out,binaries);
 //dump(out);
 const mem m1=to_mem(out);
 const mem m2=trim_r(m1);
 const mem cc="out/out.cc";
 save(cc,m2);
 const int file_count=count(dir_find("src"));
 const int struct_count=count(structs);
 const int fn_count=count(prototypes);
 const int sloc=count(implements);
 print(concat("program ",x));
 print(concat("file ",to_mem(file_count)));
 print(concat("struct ",to_mem(struct_count)));
 print(concat("fn ",to_mem(fn_count)));
 print(concat("sloc ",to_mem(sloc)));
 const double generate=get_now()-begin;
 const mem bin_gcc=concat(x,"-gcc");
 const mem bin_gcc_g=concat(x,"-gcc-g");
 const mem bin_gcc_o3=concat(x,"-gcc-o3");
 const mem bin_clang=concat(x,"-clang");
 const mem bin_clang_g=concat(x,"-clang-g");
 const mem bin_clang_o3=concat(x,"-clang-o3");
 const mem out_gcc=path_concat("out",bin_gcc);
 const mem out_gcc_g=path_concat("out",bin_gcc_g);
 const mem out_gcc_o3=path_concat("out",bin_gcc_o3);
 const mem out_clang=path_concat("out",bin_clang);
 const mem out_clang_g=path_concat("out",bin_clang_g);
 const mem out_clang_o3=path_concat("out",bin_clang_o3);
 os_system(concat("g++ -g -fmax-errors=5 -pthread ",cc," -o ",out_gcc_g));
 os_system(concat("clang++ -g -ferror-limit=5 -pthread ",cc," -o ",out_clang_g));
 if(release)
 {
  os_system(concat("g++ -fmax-errors=5 -pthread ",cc," -o ",out_gcc));
  os_system(concat("g++ -O3 -fmax-errors=5 -pthread ",cc," -o ",out_gcc_o3));
  os_system(concat("clang++ -ferror-limit=5 -pthread ",cc," -o ",out_clang));
  os_system(concat("clang++ -O3 -ferror-limit=5 -pthread ",cc," -o ",out_clang_o3));
 }
 const double compile=get_now()-generate;
 const double profile=get_now()-begin;
 print(concat("generate ",to_mem(generate,2),"s"));
 print(concat("compile ",to_mem(compile,2),"s"));
 print(concat("profile ",to_mem(profile,2),"s"));
}
void entry(const txt& x)
{
 const txt prgs=get_prgs();
 mem prg="build";
 bool release=false;
 txt parameters;
 txt t=x;
 while(is_full(t))
 {
  const mem m1=shift(t);
  const mem m2=strip_l(m1,"--");
  if(eq(m1,"--release"))
   release=true;
  else if(contain(prgs,m2))
   prg=m2;
  else
   push(parameters,m1);
 }
 flower("-");
 build(prg,release);
}
mem get_bin_prototype(const mem& x)
{
 check(is_file(x));
 const mem m1=path_base(x);
 const mem m2=replace(m1,'-','_');
 const mem m3=replace(m2,'.','_');
 return concat("const char* ",m3,"()");
}
txt get_prgs()
{
 txt r;
 txt t=dir_list("src/program");
 while(is_full(t))
 {
  const mem m=shift(t);
  const mem name=path_name(m);
  push(r,name);
 }
 return r;
}
bool is_macro(const mem& x)
{
 if(is_empty(x))
  return false;
 if(is_text(x))
  return false;
 if(match_l(x,"#"))
  return true;
 return false;
}
bool is_prototype(const mem& x)
{
 if(is_empty(x))
  return false;
 if(is_text(x))
  return false;
 const char c1=front(x);
 if(!is_alpha(c1))
  return false;
 const char c2=back(x);
 if(c2!=')')
  return false;
 return true;
}
txt parse_implements(const mem& x)
{
 txt r;
 const mem m=load(x);
 txt lines=to_txt(m);
 while(is_full(lines))
 {
  const mem line=shift(lines);
  if(is_blank(line))
   continue;
  if(is_macro(line))
   continue;
  if(is_prototype(line))
  {
   txt prototypes;
   push(prototypes,line);
   while(is_full(lines))
   {
    const mem line=front(lines);
    if(is_prototype(line))
    {
     shift(lines);
     push(prototypes,line);
    }
    else
     break;
   }
   txt body;
   const mem m1=shift(lines);
   const mem m2=trim_r(m1);
   check(eq(m2,"{"));
   push(body,m2);
   while(is_full(lines))
   {
    const mem m1=front(lines);
    const mem m2=trim_r(m1);
    if(is_blank(m2))
    {
     shift(lines);
     continue;
    }
    if(eq(m2,"}"))
     break;
    shift(lines);
    push(body,m2);
   }
   const mem m3=shift(lines);
   const mem m4=trim_r(m3);
   check(eq(m4,"}"));
   push(body,m4);
   while(is_full(prototypes))
   {
    const mem prototype=shift(prototypes);
    push(r,prototype);
    append(r,body);
   }
   continue;
  }
  push(r,line);
 }
 return r;
}
txt parse_macros(const mem& x)
{
 txt r;
 const mem m=load(x);
 txt lines=to_txt(m);
 while(is_full(lines))
 {
  const mem line=shift(lines);
  if(is_macro(line))
   push(r,line);
 }
 return r;
}
txt parse_prototypes(const mem& x)
{
 txt r;
 const mem m=load(x);
 txt lines=to_txt(m);
 while(is_full(lines))
 {
  const mem line=shift(lines);
  if(is_prototype(line))
   push(r,line);
 }
 return r;
}
txt parse_structs(const mem& x)
{
 txt r;
 const mem m=load(x);
 txt lines=to_txt(m);
 while(is_full(lines))
 {
  const mem line=shift(lines);
  if(match_l(line,"struct "))
  {
   mem s=strip_l(line,"struct");
   mem name=trim(s);
   push(r,name);
  }
 }
 return r;
}
txt prg_binaries(const mem& x)
{
 txt r;
 txt t=prg_files(x);
 while(is_full(t))
 {
  const mem path=shift(t);
  const mem ext=path_ext(path);
  if(eq(ext,"bin"))
  {
   const mem prototype=get_bin_prototype(path);
   const mem m1=load(path);
   const mem m2=to_lit(m1);
   push(r,prototype);
   push(r,"{");
   push(r,concat(" static thread_local const char* p=",m2,";"));
   push(r," return p;");
   push(r,"}");
  }
 }
 return r;
}
txt prg_files(const mem& x)
{
 txt r;
 txt t;
 const mem program=path_concat("src/program",x);
 append(t,dir_find("src/common"));
 append(t,dir_find(program));
 while(is_full(t))
 {
  const mem path=shift(t);
  const mem ext=path_ext(path);
  if(is_dir(path))
   continue;
  if(eq(ext,"cc"))
   push(r,path);
  else if(eq(ext,"bin"))
   push(r,path);
 }
 return r;
}
txt prg_files_cc(const mem& x)
{
 txt r;
 txt t=prg_files(x);
 while(is_full(t))
 {
  const mem path=shift(t);
  const mem ext=path_ext(path);
  if(eq(ext,"cc"))
   push(r,path);
 }
 return r;
}
txt prg_generate_binaries(const mem& x)
{
 return prg_binaries(x);
}
txt prg_generate_implements(const mem& x)
{
 return prg_implements(x);
}
txt prg_generate_macros(const mem& x)
{
 return prg_macros(x);
}
txt prg_generate_prototypes(const mem& x)
{
 txt r;
 txt t=prg_prototypes(x);
 while(is_full(t))
 {
  const mem m1=shift(t);
  const mem m2=concat(m1,";");
  push(r,m2);
 }
 return r;
}
txt prg_generate_structs(const mem& x)
{
 txt r;
 txt t=prg_structs(x);
 while(is_full(t))
 {
  const mem m1=shift(t);
  const mem m2=concat("struct ",m1);
  const mem m3=concat(m2,";");
  push(r,m3);
 }
 return r;
}
txt prg_implements(const mem& x)
{
 txt r;
 txt t=prg_files_cc(x);
 while(is_full(t))
 {
  const mem path=shift(t);
  const txt implements=parse_implements(path);
  append(r,implements);
 }
 return r;
}
txt prg_macros(const mem& x)
{
 txt r;
 txt t=prg_files_cc(x);
 while(is_full(t))
 {
  const mem path=shift(t);
  const txt macros=parse_macros(path);
  append(r,macros);
 }
 return r;
}
txt prg_prototypes(const mem& x)
{
 txt r;
 txt t=prg_files(x);
 while(is_full(t))
 {
  const mem path=shift(t);
  const mem ext=path_ext(path);
  if(eq(ext,"cc"))
  {
   const txt prototypes=parse_prototypes(path);
   append(r,prototypes);
  }
  else if(eq(ext,"bin"))
  {
   const mem m=get_bin_prototype(path);
   push(r,m);
  }
  else
   stop();
 }
 return r;
}
txt prg_structs(const mem& x)
{
 txt r;
 txt t=prg_files_cc(x);
 while(is_full(t))
 {
  const mem path=shift(t);
  const txt structs=parse_structs(path);
  append(r,structs);
 }
 return r;
}